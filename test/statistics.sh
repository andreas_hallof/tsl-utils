#! /bin/bash

r=tmp-$RANDOM-$RANDOM-$RANDOM-$RANDOM-$RANDOM-$RANDOM
e=../Extract-TSP.py

if [ ! -f "$1" ]; then echo ERROR: TSL-File "$1" not found; exit 1; fi
f=$(readlink -f "$1")

if [ ! -x "$e" ]; then echo ERROR: Extractor-Script "$e" found; exit 2; fi

mkdir $r || exit 3
cd $r || exit 4

for i in oid_ak_aut oid_egk_aut oid_hba_aut oid_smc_b_aut oid_vpnk_vpn
do
    ../$e $f $i >/dev/null
    echo $i: $(find . |grep .der | wc -l)
    rm -f *der
done

cd ..
rmdir $r

