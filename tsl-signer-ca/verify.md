# Signaturprüfung

    [a@h tsl-signer-ca]$ sha256sum *
    c2eef6cb3a7829db5c595fef25470a5b5e142fe14827259cc201136f1c032bee  GEM.RCA1.der
    c8f3247b9c4e9f48442ac6e180b7217551a4869b9fa812c54bd2a324282393fe  GEM.RCA1.pem
    848fda162c607b492c62f625840e6451285c40c7334ec8dd659d093236ebc9ec  GEM.RCA2.der
    0114e0086dadec023658d6990d95bb4ad7a6e8d39c2e99fa05d10274f0d24d14  GEM.RCA2.pem
    9a7e2f1ea6449cb39c241f063b5845d3e0789dc28e0286d3c6e64505f95c8457  GEM.RCA3.der
    29325e8032d35ca68ae911123ac83a8dd3b4f9fa8cc2bada133f8ac2a215d2fc  GEM.RCA3.pem
    a9a6d014b5adffaede619afd7c94f2ad4eaa403ddfc63d04907b80a51a4f8b39  GEM.RCA4.der
    ce409e24b6583354e773bad35e7e1111ffa96227f648a493d81b6464b2830d5b  GEM.RCA4.pem
    a94a6c99481caccbe6236e0c1718a9a28bb82ddb41898af300e1de7b631db42f  GEM.TSL-CA1.der
    868dfad2953efc74b3d265fe6aed6d3227b5643c582dd01000b06e1612d9d540  GEM.TSL-CA1.pem
    e2992dc292f6aa8d9e462a7723f54c162853b4056c277260b82ce58921408448  GEM.TSL-CA3.der
    fd12c1de144605d7a9e2755e978fea223466374d395fed1bd3605ea9f781e51a  GEM.TSL-CA3.pem

    [a@h tsl-signer-ca]$ cat verify.sh 
    #! /bin/bash

    openssl verify -verbose -CAfile GEM.RCA1.pem GEM.TSL-CA1.pem
    openssl verify -verbose -CAfile GEM.RCA4.pem GEM.TSL-CA3.pem

    [a@h tsl-signer-ca]$ ./verify.sh 
    GEM.TSL-CA1.pem: OK
    GEM.TSL-CA3.pem: OK

