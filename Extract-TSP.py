#! /usr/bin/env python3

import sys, base64, re, os, glob
from datetime import datetime
from xml.etree import ElementTree

# Ich gehen davon aus, dass zuvor die Signatur der TSL mit Chk-Signature-TSL.py
# erfolreich und mit positiven Ergebnis geprüft worden ist.
# Die hier XML-Daten verarbeiteten Daten sind also nicht durch einen Angreifer
# manipuliert worden (unter der Annahme der TSL-Dienst ist nicht kompromittiert). 
#

if len(sys.argv)<3:
    sys.exit("Syntax:\n", sys.argv[0], "tsl-xml-file oid_or_value1 oid_or_value2 ...",
          "\nExample:\n", sys.argv[0], "TSL.xml oid_zd_tls_s")

Argumente=sys.argv[1:]

if Argumente[0] in ['-v', '--verbose']:
    verbose=True; Argumente=Argumente[1:]
else:
    verbose=False

TSL_Datei=Argumente.pop(0);

if not (os.path.exists(TSL_Datei) and os.path.isfile(TSL_Datei)):
    sys.exit('Datei "{}" nicht gefunden'.format(TSL_Datei))

DER_Dateien=glob.glob("*.der")
if len(DER_Dateien)>0:
    sys.exit( 'Bitte erst alle *.der-Dateien im aktuellen Verzeichnis löschen')

Suchziele={x : 1 for x in Argumente};

ns={ 'tsl': 'http://uri.etsi.org/02231/v2#' } # ElementTree.register_namespace('tsl', ns['tsl'])

e = ElementTree.parse(TSL_Datei).getroot()
if e.tag != '{http://uri.etsi.org/02231/v2#}TrustServiceStatusList':
    sys.exit("keine gültige TI-TSL")

if 'Id' in e.attrib:
    print('TSL id {}'.format(e.attrib['Id']))
else:
    sys.exit("keine gültige TI-TSL")

c1=e.findall('./tsl:SchemeInformation/tsl:ListIssueDateTime', ns);
print('TSL erzeugt am', c1[0].text)
if datetime.now()<datetime.strptime(c1[0].text, '%Y-%m-%dT%H:%M:%SZ'):
    print("WARNUNG: lokale Zeit ist falsch.")
c1=e.findall('./*/tsl:NextUpdate/tsl:dateTime', ns);
print("nächstes Update", c1[0].text)
if datetime.strptime(c1[0].text, '%Y-%m-%dT%H:%M:%SZ')<datetime.now():
    print("WARNUNG: TSL-Gültigkeit abgelaufen oder lokale Zeit ist falsch.")

TSP_Counter=0; TSPService_Counter=0; Zertifikate_Gefunden_Counter=0

TSPList=e.findall('./tsl:TrustServiceProviderList', ns)
for TSP in TSPList[0]:
    TSP_Counter+=1
    if verbose:
        tn=TSP.findall('./*/tsl:TSPName/tsl:Name', ns)
        print("----------", tn[0].text)
    TSPServices=TSP.findall('./tsl:TSPServices', ns)[0]
    for TSPService in TSPServices.findall('./tsl:TSPService/', ns):
        TSPService_Counter+=1
        if verbose:
            name=TSPService.findall('./tsl:ServiceName/tsl:Name', ns)[0]
            name=name.text.strip(); name=re.sub("\n"," ", name); name=re.sub("  +"," ", name)
            print(name)
            if name=='CN=DNSSEC-Trustanchor, DC=telematik':
                continue
        Extensions=TSPService.findall('./tsl:ServiceInformationExtensions', ns)[0]
        Gefunden=False
        for ex in Extensions:
            for ee in ex:
                if not ee.text:
                    continue
                wert=ee.text.strip()
                if wert in Suchziele:
                    Gefunden=True
                    break
            if Gefunden:
                break

        if Gefunden:
            Zertifikate_Gefunden_Counter+=1
            Erfolgreich_Extrahiert=False
            for i in TSPService.findall('./tsl:ServiceDigitalIdentity/tsl:DigitalId', ns):
                for a in i:
                    if a.tag=='{http://uri.etsi.org/02231/v2#}Other':
                        x=a[0]
                        data=x.text.strip()
                    else:
                        data=a.text.strip()

                    assert len(data)>0
                    data=base64.b64decode(data)
                    with open(str(Zertifikate_Gefunden_Counter)+'.der', "wb") as f:
                        f.write(data)
                        Erfolgreich_Extrahiert=True
                    
            if not Erfolgreich_Extrahiert:
                sys.exit("Fehler bei der Extraktion.")

print("In der TSL sind {} TSPs mit zusammen {} TSPServices enthalten. Es wurde{} {} Suchziel{} gefunden{}.".format(
      TSP_Counter, TSPService_Counter, 
      '' if Zertifikate_Gefunden_Counter==1 else 'n',
      Zertifikate_Gefunden_Counter,
      '' if Zertifikate_Gefunden_Counter==1 else 'e',
      ' und extrahiert' if Zertifikate_Gefunden_Counter>0 else ''
      ))

