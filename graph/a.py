#! /usr/bin/env python3

import sys, base64, os, argparse, glob, re, logging
from binascii import hexlify
from datetime import datetime
from xml.etree import ElementTree
import networkx as nx

from cryptography import x509
from cryptography.hazmat.backends import default_backend

# Ich gehen davon aus, dass zuvor die Signatur der TSL mit Chk-Signature-TSL.py
# erfolgreich und mit positiven Ergebnis geprüft worden ist.
# Die hier XML-Daten verarbeiteten Daten sind also nicht durch einen Angreifer
# manipuliert worden (unter der Annahme der TSL-Dienst ist nicht kompromittiert).
#

a_p = argparse.ArgumentParser(
    description=\
"""Aus einer angegeben TSL-Datei suche ich die (TI-internen) URLs und den
Signaturschlüssel (Svctype/Certstatus/OCSP) des OCSP-Responders für jedes
CA-Zertifikat (Svctype/CA/PKC). Zertifikate aus dem eIDAS-Vertrauensraums
werden ignoriert."""
)
a_p.add_argument('-v', '--verbose', dest='verbose', action='store_true', help='verbose mode')
a_p.add_argument(dest='TSL_Datei', metavar='TSL-Datei')
args = a_p.parse_args()

logging.basicConfig(level=logging.DEBUG,
                    format='[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
                    datefmt="%Y-%m-%dT%H:%M:%S%z")

if not (os.path.exists(args.TSL_Datei) and os.path.isfile(args.TSL_Datei)):
    sys.exit('Datei "{}" nicht gefunden'.format(args.TSL_Datei))

root_certs = glob.glob("GEM.RCA*.der")
mygraph = nx.DiGraph()
root_ids = []

def add_cert_to_graph(cert: x509.Certificate, G: nx.Graph, isRoot=False, isOCSP=False):
    """Fügt ein Zertifikat in den PKI-Graphen ein"""

    assert not(isRoot and isOCSP)

    cn = ', '.join(x for x in [ y.value for y in cert.subject ]) #print(cn)

    for e in cert.extensions:
        if e.oid == x509.oid.ExtensionOID.SUBJECT_KEY_IDENTIFIER:
            #print(e.oid)
            subj_k_i = hexlify(e.value.digest).decode()
            #print(subj_k_i)
            test_1 = x509.SubjectKeyIdentifier.from_public_key(cert.public_key())
            if e.value.digest != test_1.digest:
                logging.warning("Keyidentifyer nicht RFC konform?")

    assert subj_k_i

    if isRoot: root_ids.append(subj_k_i)

    a_k_i = ""
    for e in cert.extensions:
        if e.oid == x509.oid.ExtensionOID.AUTHORITY_KEY_IDENTIFIER:
            a_k_i = hexlify(e.value.key_identifier).decode()

    G.add_node(subj_k_i, 
               isroot=isRoot,
               isocsp=isOCSP,
               cn=cn,
               a_k_i=a_k_i,
               NotValidAfter=cert.not_valid_after.isoformat(timespec='minutes'))

    if a_k_i:
        if a_k_i in G.nodes:
            G.add_edge(a_k_i, subj_k_i)
        else:
            logging.warning(a_k_i + " not in graph, cn=" + cn)

    return True
    
def draw_graph(mygraph: nx.Graph):
    """Zeigt den Graphen als ascii-art"""

    labels = dict()
    for i in mygraph:
        labels[i] = 'todo'

    d = { '806170191c38ede26be4a12eab223e75c994fe7d' : "RCA4",
          '848420775b658337384d005ca78ea0a0f2f8d235' : "RCA1",
          'dfc3be75be25eb1332302ea75085089f3710d0e8' : "RCA3",
          'ec5c18e013b4436c098cdffa3c3c5b7e4b708446' : "RCA2",
          '2af62411667b55f3f49cf351432836fe0e93837f' : "tsys-smcb1",
          '0f7f978fc09e1af67833420ae56f9f5305cfd552' : "tsys-hba-o2",
          '657cbd744e7b3525bbb85f88884e4a3279bf5aa8' : "tsys-egk3",
          '5408e9d7946253df1f1282d6a7800b71ba2e9e36' : "tsys-o1",
          '171553637d2c627f0af276ab54d65445fff29c51' : "tsys-smcb2",
          'fa20d1b79c26ec0b77a7170678bb6e98e4fb59b8' : "tsys-o3",
          '02421e7ccf3d6132d13838285fe227913b8fc0f5' : "tsys-o2",
          '1aed9befc190da65223f7431a0f3ebb7685e38ba' : "tsys-hba1",
          'f4e23b61186783c002aae87c64272a9276daedc3' : "tsys-egk4",
          '4b1a00655abd865e048d69dd2f7ee6c2705a93ea' : "tsys-egk5",
          'e159185581e551c3a4e9e5e692d2949ee32e11a7' : "tsys-egk6",
          'e295e85c6c6feccffa6462c8dccfa5c39fa4b288' : "tsys-hba2",
          '2641ecf8115a95d37577daabb0aad6d0dba5fd61' : "tsys-egk7",
          'd33988c7ef1f58c679e576bcfb69c2691a9447cd' : "tsys-o7",
          'c64e9848a35e2f733fc898a1e6f547a835c8853a' : "tsys-smcb3",
          '12ce5a5ae6a66edeeceed031d08e07d6638a1a92' : "dt-hca1-o",
          'f2243b3b22776658e6d431c37f1bc4c1ae758aab' : "dt-hca1",
          'e8f92eb727a8f7cddece3f591aad2e4535152612' : "dt-sca1-o",
          'ec120e565a88256fbb0741f2e614ba409bbee07e' : "dt-sca1",
          'a56c1fbafae3d86ce62254f9e666eb4f20a7ffeb' : "dt-sca3-o",
          '14ded5fd38944b38938335ec668d7172747e5a7d' : "dt-hca3",
          '8653ead8820ad0d0be4a313a4b2184ce4da1d723' : "dt-hca3-o",
          '71beccac2f46f7af7503ce983b86e2f2bc56732e' : "k-eca1",
          'c3185d9b936eff442a48043d6f65c9a61cb06220' : "k-eca4",
          '2fb52fa065e6f34d19302ea8cb5f2ca02ba755e8' : "k-eca4-o1",
          '8593bf5d25a4c2745d4287464c7fe8f1497832e8' : "k-eca2-o2",
          '069c8eab46fb51c4f024027b450b0eaf3efcab37' : "k-eca1-o",
          '097dd2ecafc610289cf8a79daa403f1ac41fb547' : "k-eca1a",
          '38c2f6310fb3d659f30414fbcb8f876e671a5d81' : "b-ca13",
          '35c9ea6220bfa8f3dfd05a401a3c4818cae1f91b' : "b-ca7",
          'f017016c50364d9d21e58c99e2a41f1f1ba94d78' : "b-ca14",
          '0b6574e92ca2e20c0a1efe87297acb7cc31beaba' : "bit-o2"
        }

    for i in d:
        labels[i] = d[i]

    #for i in sorted(mygraph):
    #    print("L=", labels[i].rjust(8), f"ID={i}", "VALUE=", mygraph.nodes[i])
    #
    #print()

    tmp_graph = mygraph.copy()

    #for x in mygraph:
    #    if mygraph.nodes[x]['isroot']:
    #        print("ROOT L=", labels[x].rjust(8), f"ID={x}", "VALUE=", mygraph.nodes[x])
    #        tmp_graph.remove_node(x)
    #        for y in mygraph:
    #            if mygraph.nodes[y]['isroot']:
    #                continue
    #            if mygraph.nodes[y]['a_k_i'] == x:
    #                print(" "*5 + "CA   L=", labels[y].rjust(8), f"ID={y}", "VALUE=", mygraph.nodes[y])
    #                tmp_graph.remove_node(y)
    #                for z in mygraph:
    #                    if z == y: continue
    #                    if mygraph.nodes[z]['a_k_i'] == y:
    #                        print(" "*10 + "OCSP L=", labels[z].rjust(8), f"ID={z}", "VALUE=", mygraph.nodes[z])
    #                        tmp_graph.remove_node(z)
    #                        

    for x in root_ids:
        print("ROOT L=", labels[x].rjust(8), f"ID={x}", "VALUE=", mygraph.nodes[x])
        tmp_graph.remove_node(x)
        for y in mygraph.neighbors(x):
            if mygraph.nodes[y]['isocsp']:
                print(" "*5 + "OCSP L=", labels[y].rjust(8), f"ID={y}", "VALUE=", mygraph.nodes[y])
                tmp_graph.remove_node(y)
            else:
                print(" "*5 + "CA   L=", labels[y].rjust(8), f"ID={y}", "VALUE=", mygraph.nodes[y])
                tmp_graph.remove_node(y)
                for z in mygraph.neighbors(y):
                    assert mygraph.nodes[z]['isocsp']
                    print(" "*10 + "OCSP L=", labels[z].rjust(8), f"ID={z}", "VALUE=", mygraph.nodes[z])
                    tmp_graph.remove_node(z)

    print()

    blacklist=dict()

    for x in tmp_graph:
        print("---- L=", labels[x].rjust(8), f"ID={x}", "VALUE=", tmp_graph.nodes[x])
        for y in mygraph:
            if y == x: continue
            #if y in blacklist: continue
            if mygraph.nodes[y]['a_k_i'] == x:
                print("    OCSP L=", labels[y].rjust(8), f"ID={y}", "VALUE=", mygraph.nodes[y])
                blacklist[y]=1
 


anzahl_roots = 0
for i in root_certs:
    with open(i, "rb") as f:
        cert = x509.load_der_x509_certificate(f.read(), default_backend())
        add_cert_to_graph(cert, mygraph, isRoot=True)
        anzahl_roots += 1

ns = { 'tsl': 'http://uri.etsi.org/02231/v2#' }
#ElementTree.register_namespace('tsl', ns['tsl'])
#ElementTree.register_namespace('tsl1', 'http://uri.etsi.org/02231/v2')

root = ElementTree.parse(args.TSL_Datei).getroot()
if not root.tag.endswith('TrustServiceStatusList'):
    sys.exit("keine gültige TI-TSL")

if 'Id' in root.attrib:
    print('TSL-id: {}'.format(root.attrib['Id']))
else:
    sys.exit("keine gültige TI-TSL")

c1 = root.findall('./tsl:SchemeInformation/tsl:ListIssueDateTime', ns)
print('TSL erzeugt am', c1[0].text)
if datetime.now() < datetime.strptime(c1[0].text, '%Y-%m-%dT%H:%M:%SZ'):
    print("WARNUNG: lokale Zeit ist falsch.")
c1 = root.findall('./*/tsl:NextUpdate/tsl:dateTime', ns)
print("nächstes Update", c1[0].text)
if datetime.strptime(c1[0].text, '%Y-%m-%dT%H:%M:%SZ') < datetime.now():
    print("WARNUNG: TSL-Gültigkeit abgelaufen oder lokale Zeit ist falsch.")
print()

# Zuerst suche ist CA-Zertifikate

for a in root:
    if not a.tag.endswith('TrustServiceProviderList'):
        # das Signatur-Element in der TSL interessiert mich hier nicht
        continue
    for b in a:
        assert b.tag == '{http://uri.etsi.org/02231/v2#}TrustServiceProvider'
        x = b.find('./tsl:TSPInformation/tsl:TSPName/tsl:Name', ns)
        for c in b:
            if c.tag != '{http://uri.etsi.org/02231/v2#}TSPServices':
                # TSPInformation-Felder interessieren mich hier nicht
                continue
            for d in c:
                sti = d.find('.//tsl:ServiceTypeIdentifier', ns)
                if not re.search("/PKC$", sti.text):
                    continue
                sn = d.find('.//tsl:ServiceName/tsl:Name', ns)
                cert_b64 = d.find('.//tsl:X509Certificate', ns)
                ssp = d.find('.//tsl:ServiceSupplyPoint', ns)
                cert = x509.load_der_x509_certificate(base64.b64decode(cert_b64.text), default_backend())

                add_cert_to_graph(cert, mygraph)

anz = 0
for i in mygraph:
    #print(f"ID={i}", "VALUE=", mygraph.nodes[i])
    anz += 1

print(f"{anz} Schlüssel davon {anzahl_roots} Root-Schlüssel")


# Jetzt suche ich nach OCSP-Zertifikaten

for a in root:
    if a.tag != '{http://uri.etsi.org/02231/v2#}TrustServiceProviderList':
        # das Signatur-Element in der TSL interessiert mich hier nicht
        continue
    for b in a:
        assert b.tag == '{http://uri.etsi.org/02231/v2#}TrustServiceProvider'
        x = b.find('./tsl:TSPInformation/tsl:TSPName/tsl:Name', ns)
        for c in b:
            if c.tag != '{http://uri.etsi.org/02231/v2#}TSPServices':
                # TSPInformation-Felder interessieren mich hier nicht
                continue
            for d in c:
                sti = d.find('.//tsl:ServiceTypeIdentifier', ns)
                if not re.search("/OCSP$", sti.text):
                    continue
                sn = d.find('.//tsl:ServiceName/tsl:Name', ns);
                cert_b64 = d.find('.//tsl:X509Certificate', ns);
                ssp = d.find('.//tsl:ServiceSupplyPoint', ns); 
                cert = x509.load_der_x509_certificate(base64.b64decode(cert_b64.text), default_backend())

                add_cert_to_graph(cert, mygraph, isOCSP=True)

anz = 0
for i in mygraph:
    #print(f"ID={i}", "VALUE=", mygraph.nodes[i])
    anz += 1

print(anz, "Schlüssel davon", anzahl_roots, "Root-Schlüssel")


draw_graph(mygraph)
