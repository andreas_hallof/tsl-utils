# BDE

Einmal pro Woche die TSL herunterladen. Die Detached-Signatur prüfen. Und die
Komponenten-CA-Zertifikate extrahieren.

    $ ./weekly-sync.sh
    2023-02-14 13:05:46 URL:http://download.crl.ti-dienste.de/TSL-ECC/ECC-RSA_TSL.xml [510832/510832] -> "ECC-RSA_TSL.xml" [1]
    2023-02-14 13:05:46 URL:http://download.crl.ti-dienste.de/TSL-ECC/ECC-RSA_TSL.sig [711/711] -> "ECC-RSA_TSL.sig" [1]
    Signierendes Zertifikat ("DE, gematik GmbH, TSL Signing Unit 4", SN: 2, gültig bis 2025-05-26T08:08) ist Teil der TI-PKI.
    kryptographische Verifikation der TSL-Daten: OK
    TSL id ID31022320230128000004Z
    TSL erzeugt am 2023-01-28T00:00:04Z
    nächstes Update 2023-02-27T00:00:04Z
    TSL id ID31022320230128000004Z
    TSL erzeugt am 2023-01-28T00:00:04Z
    nächstes Update 2023-02-27T00:00:04Z
    In der TSL sind 10 TSPs mit zusammen 202 TSPServices enthalten. Es wurden 6 Suchziele gefunden und extrahiert.


So jetzt habe ich alle Komponenten-CA-Zertifikat integritäts- und authentitätsgeschützt erhalten.
Wir gucken uns diese mal an:

    a@t:~/git/tsl-utils/bde$ ll *.der
    -rw-rw-r-- 1 a a 1054 Feb 14 13:08 1.der
    -rw-rw-r-- 1 a a 1149 Feb 14 13:08 2.der
    -rw-rw-r-- 1 a a 1054 Feb 14 13:08 3.der
    -rw-rw-r-- 1 a a  658 Feb 14 13:08 4.der
    -rw-rw-r-- 1 a a  709 Feb 14 13:08 5.der
    -rw-rw-r-- 1 a a 1103 Feb 14 13:08 6.der
    a@t:~/git/tsl-utils/bde$ sha256sum *der
    3ea298a64a3d83f1148e0c5607529131c4232e65f9ed75c9bcbdaab208916b96  1.der
    6a3edd46d9d8a9dd7fce3b24666caa7a681a630c9d71cf288f027209c76b1755  2.der
    5c8dd5961be51b156a20189e00a3bf94468126f527b3e115c2d22b67596141cf  3.der
    b4aa253e71e57be5bf919115903b73fd4c779e6dbb66cf778d5d16e736bd90a8  4.der
    4e5a47cb6dec603d3dbbec4302ac4bc29a755ff7645eca9110266673ffdb9c99  5.der
    b42fbc96985a6bf29904b931e5ea0eea365e100c0169b7f6d6bae77089b8babf  6.der
    a@t:~/git/tsl-utils/bde$ ../roots/cert-info.py *.der
    GEM.KOMP-CA3 FDB5B91779BDB86B1E2957223DC2B9B38981CF4D GEM.KOMP-CA3 EC5C18E013B4436C098CDFFA3C3C5B7E4B708446 UNKNOWN
    GEM.KOMP-CA1 42B899C13A588208599C18CF3947669DE133FAE6 GEM.KOMP-CA1 848420775B658337384D005CA78EA0A0F2F8D235 UNKNOWN
    GEM.KOMP-CA5 95549B3FBDC43CB478C8918CFD68578A64A43097 GEM.KOMP-CA5 EC5C18E013B4436C098CDFFA3C3C5B7E4B708446 UNKNOWN
    GEM.KOMP-CA4 0255E2DB21CFB42B1DADADD81EFC4461FEE3B5B0 GEM.KOMP-CA4 DFC3BE75BE25EB1332302EA75085089F3710D0E8 UNKNOWN
    GEM.KOMP-CA6 30D2E136AF6C57F2B18395DAB638A67BBA3174CA GEM.KOMP-CA6 806170191C38EDE26BE4A12EAB223E75C994FE7D UNKNOWN
    GEM.KOMP-CA7 04B3F41630453B7B914AF37CF089E5094E925194 GEM.KOMP-CA7 1844C4DA66933AEF4F3B2A0965E4FE28901E5511 UNKNOWN


