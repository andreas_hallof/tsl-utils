#! /bin/bash

mylog() {
    # hier integriert man dann sein zentrales Logging/Monitoring
    echo $*
    exit 1
}

../Download-TSL.sh || mylog Fehler beim TLS-Download
../Chk-Detached-Signature-TSL.py ECC-RSA_TSL.xml || mylog Fehler bei der Signaturpruefung
../Extract-TSP.py ECC-RSA_TSL.xml oid_zd_tls_s || mylog Fehler beim CA-Zertifikate extrahieren

