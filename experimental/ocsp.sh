#! /usr/bin/env bash

for i in $(seq 1 100); do
    time openssl ocsp -issuer GEM.RCA2.pem -sha256 \
      -url http://ocsp.root-ca.ti-dienste.de/ocsp -serial $i \
      -no_cert_checks -noverify -reqout req.der -no_nonce 
done

