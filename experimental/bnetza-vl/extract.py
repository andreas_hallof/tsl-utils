#! /usr/bin/env python3

import re
from base64 import b64decode

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec, rsa, padding
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.kdf.hkdf import HKDF
from cryptography.exceptions import InvalidSignature, InvalidTag

with open("TSL-XML.xml", "r") as f:
    t=f.read()

print(type(t), len(t))

pattern=re.compile('<X509Certificate>([^>]+)</X509Certificate>')

counter=0; ecc=0
for (cert_base64) in re.findall(pattern, t):
    counter+=1
    der=b64decode(cert_base64)
    cert=x509.load_der_x509_certificate(der, default_backend())
    try:
        public_key=cert.public_key()
        pn=public_key.public_numbers()
    except:
        print(counter, cert_base64)
        next;

    if isinstance(public_key, ec.EllipticCurvePublicKey):
        print("ECC", public_key.curve)
        ecc+=1
    else:
        print("RSA")

print("ECC {}, SUM {}, Rate {}".format(ecc, counter, ecc/counter))
                
