#! /usr/bin/python

import re

from base64 import b64decode
from cryptography import x509
from cryptography.hazmat.primitives.asymmetric import rsa
from icecream import ic

pattern = re.compile(r'<X509Certificate>([^<]+)</X509Certificate>')

a="../../ECC-RSA_TSL.xml"

with open(a, "rt") as f:
    data = f.read()
    ic(len(data))
    i=0
    for (certdata) in re.findall(pattern, data):
        i+=1
        cert_der = b64decode(certdata.encode())
        cert = x509.load_der_x509_certificate(cert_der)
        print("Nr.", i)
        print("", ", ".join(x for x in [y.value for y in cert.subject]))
        print(" nva:", cert.not_valid_after)
        public_key = cert.public_key()
        print(" algo:", "RSA" if isinstance(public_key, rsa.RSAPublicKey) else "ECC")
        print(" Issuer:", ", ".join(x for x in [y.value for y in cert.issuer]))

