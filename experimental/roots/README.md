# README.md

    Nr. 1
     DE, medisign GmbH, DEMDS CA für Zahnärzte 5:PN
     nva: 2023-10-29 14:33:35
     algo: RSA
     Issuer: DE, Bundeszahnärztekammer, Wurzelzertifizierungsstelle BZÄK 3:PN
    Nr. 2
     DE, medisign GmbH, MED-ZOD2-CA-7:PN
     nva: 2023-10-29 15:42:50
     algo: RSA
     Issuer: DE, ZOD, ZOD2-Root-CA-KZBV-5:PN
    Nr. 3
     DE, medisign GmbH, MED-ZOD2-CA-5:PN
     nva: 2023-01-06 13:46:33
     algo: RSA
     Issuer: DE, ZOD, ZOD2-Root-CA-KZBV-5:PN
    Nr. 4
     DE, medisign GmbH, DEMDS CA für Ärzte 9:PN
     nva: 2023-10-29 14:59:20
     algo: RSA
     Issuer: DE, Bundesärztekammer, 7R BÄK CA 1:PN
    Nr. 5
     DE, medisign GmbH, MESIG.SMCB-OCSP2
     nva: 2026-06-10 05:32:24
     algo: RSA
     Issuer: DE, Medisign GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, MESIG.SMCB-CA1
    Nr. 6
     DE, medisign GmbH, MESIG.HBA-OCSP1
     nva: 2025-06-03 13:19:44
     algo: RSA
     Issuer: DE, Medisign GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, MESIG.HBA-CA1
    Nr. 7
     DE, Medisign GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, MESIG.HBA-CA1
     nva: 2026-12-06 10:38:09
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA2
    Nr. 8
     DE, Medisign GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, MESIG.SMCB-CA1
     nva: 2026-06-10 07:32:24
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA2
    Nr. 9
     DE, Medisign GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, MESIG.HBA-CA10
     nva: 2029-10-10 08:14:16
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA5
    Nr. 10
     DE, Medisign GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, MESIG.SMCB-CA10
     nva: 2029-10-10 08:14:32
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA5
    Nr. 11
     DE, Medisign GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, MESIG.HBA-CA4
     nva: 2029-11-30 08:33:14
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA6
    Nr. 12
     DE, Medisign GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, MESIG.SMCB-CA4
     nva: 2029-11-30 08:33:31
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA6
    Nr. 13
     DE, medisign GmbH, MESIG.HBA-OCSP4
     nva: 2026-12-13 13:03:20
     algo: RSA
     Issuer: DE, Medisign GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, MESIG.HBA-CA4
    Nr. 14
     DE, medisign GmbH, MESIG.HBA-OCSP10
     nva: 2026-12-13 15:24:52
     algo: ECC
     Issuer: DE, Medisign GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, MESIG.HBA-CA10
    Nr. 15
     DE, medisign GmbH, MESIG.SMCB-OCSP10
     nva: 2026-12-13 15:22:41
     algo: ECC
     Issuer: DE, Medisign GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, MESIG.SMCB-CA10
    Nr. 16
     DE, medisign GmbH, MESIG.SMCB-OCSP4
     nva: 2026-12-13 13:10:08
     algo: RSA
     Issuer: DE, Medisign GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, MESIG.SMCB-CA4
    Nr. 17
     DE, arvato Systems GmbH, VPNK CRL-Signer4
     nva: 2025-03-26 06:46:24
     algo: RSA
     Issuer: DE, gematik GmbH, VPN-Zugangsdienst-CA der Telematikinfrastruktur, GEM.VPNK-CA3
    Nr. 18
     DE, arvato Systems GmbH, Komp-PKI OCSP-Signer5
     nva: 2025-03-26 08:29:20
     algo: RSA
     Issuer: DE, gematik GmbH, Komponenten-CA der Telematikinfrastruktur, GEM.KOMP-CA3
    Nr. 19
     DE, arvato Systems GmbH, Komp-CA4 OCSP-Signer4
     nva: 2026-09-11 09:26:31
     algo: ECC
     Issuer: DE, gematik GmbH, Komponenten-CA der Telematikinfrastruktur, GEM.KOMP-CA4
    Nr. 20
     DE, gematik GmbH, Komponenten-CA der Telematikinfrastruktur, GEM.KOMP-CA3
     nva: 2026-03-14 12:23:06
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA2
    Nr. 21
     DE, gematik GmbH, Komponenten-CA der Telematikinfrastruktur, GEM.KOMP-CA5
     nva: 2026-12-07 08:41:56
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA2
    Nr. 22
     DE, arvato Systems GmbH, VPNK CRL-Signer5
     nva: 2028-08-01 17:22:49
     algo: ECC
     Issuer: DE, gematik GmbH, VPN-Zugangsdienst-CA der Telematikinfrastruktur, GEM.VPNK-CA6
    Nr. 23
     DE, gematik GmbH, TSL-Signer-CA der Telematikinfrastruktur, GEM.TSL-CA3
     nva: 2028-05-25 06:50:47
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA4
    Nr. 24
     DE, arvato Systems GmbH, TSL-CA OCSP-Signer5
     nva: 2025-05-26 08:10:45
     algo: ECC
     Issuer: DE, gematik GmbH, TSL-Signer-CA der Telematikinfrastruktur, GEM.TSL-CA3
    Nr. 25
     DE, gematik GmbH, Komponenten-CA der Telematikinfrastruktur, GEM.KOMP-CA4
     nva: 2026-09-11 09:26:31
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA3
    Nr. 26
     DE, gematik GmbH, Komponenten-CA der Telematikinfrastruktur, GEM.KOMP-CA6
     nva: 2029-07-20 11:02:53
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA4
    Nr. 27
     DE, gematik GmbH, Komponenten-CA der Telematikinfrastruktur, GEM.KOMP-CA7
     nva: 2029-11-30 08:35:24
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA6
    Nr. 28
     DE, gematik GmbH, VPN-Zugangsdienst-CA der Telematikinfrastruktur, GEM.VPNK-CA4
     nva: 2026-09-11 09:26:03
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA3
    Nr. 29
     DE, gematik GmbH, VPN-Zugangsdienst-CA der Telematikinfrastruktur, GEM.VPNK-CA6
     nva: 2029-07-20 11:03:40
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA4
    Nr. 30
     DE, gematik GmbH, VPN-Zugangsdienst-CA der Telematikinfrastruktur, GEM.VPNK-CA7
     nva: 2029-11-30 08:36:21
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA6
    Nr. 31
     DE, gematik GmbH, VPN-Zugangsdienst-CA der Telematikinfrastruktur, GEM.VPNK-CA5
     nva: 2026-12-07 08:41:56
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA2
    Nr. 32
     DE, gematik GmbH, VPN-Zugangsdienst-CA der Telematikinfrastruktur, GEM.VPNK-CA3
     nva: 2026-03-14 12:24:14
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA2
    Nr. 33
     DE, arvato Systems GmbH, Komp-CA8 OCSP-Signer1
     nva: 2028-08-07 08:18:03
     algo: ECC
     Issuer: DE, gematik GmbH, Komponenten-CA der Telematikinfrastruktur, GEM.KOMP-CA8
    Nr. 34
     DE, gematik GmbH, Komponenten-CA der Telematikinfrastruktur, GEM.KOMP-CA8
     nva: 2031-07-16 11:37:28
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA7
    Nr. 35
     DE, arvato Systems GmbH, Komp-CA6 OCSP-Signer1
     nva: 2028-11-12 13:55:51
     algo: ECC
     Issuer: DE, gematik GmbH, Komponenten-CA der Telematikinfrastruktur, GEM.KOMP-CA6
    Nr. 36
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA201
     nva: 2025-02-09 13:58:37
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA201
    Nr. 37
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA9
     nva: 2026-10-02 12:17:22
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA9
    Nr. 38
     DE, Atos Information Technology GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, ATOS.HBA-CA1
     nva: 2026-12-06 08:57:37
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA2
    Nr. 39
     DE, Atos Information Technology GmbH, ATOS.HBA-OCSP1
     nva: 2025-01-13 08:06:43
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, ATOS.HBA-CA1
    Nr. 40
     DE, Atos Information Technology GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, ATOS.SMCB-CA1
     nva: 2026-12-06 08:59:28
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA2
    Nr. 41
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP201
     nva: 2025-02-09 13:58:36
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA201
    Nr. 42
     DE, Atos Information Technology GmbH, ATOS.SMCB-OCSP1
     nva: 2025-01-13 09:04:52
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, ATOS.SMCB-CA1
    Nr. 43
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP6-230601
     nva: 2026-06-19 09:17:48
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA6
    Nr. 44
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP5-230601
     nva: 2026-06-19 09:41:01
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA5
    Nr. 45
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP7-230628
     nva: 2026-08-29 08:57:54
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA7
    Nr. 46
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP8-230628
     nva: 2026-08-29 09:09:06
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA8
    Nr. 47
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA204
     nva: 2026-12-07 08:41:56
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA2
    Nr. 48
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA5
     nva: 2026-06-19 12:55:47
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA5
    Nr. 49
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA7
     nva: 2026-08-30 12:24:39
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA7
    Nr. 50
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA202
     nva: 2026-11-14 15:06:09
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA202
    Nr. 51
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA12
     nva: 2027-01-29 16:40:09
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA12
    Nr. 52
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA11
     nva: 2027-01-29 16:38:36
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA11
    Nr. 53
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP18
     nva: 2026-07-16 11:50:42
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA18
    Nr. 54
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP13
     nva: 2024-05-06 13:08:16
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA13
    Nr. 55
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA13
     nva: 2027-05-08 12:53:35
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA13
    Nr. 56
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA20
     nva: 2029-08-16 08:07:47
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA4
    Nr. 57
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA6
     nva: 2026-06-19 14:11:01
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA6
    Nr. 58
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA14
     nva: 2027-05-08 12:55:17
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA14
    Nr. 59
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP14
     nva: 2024-05-06 13:08:38
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA14
    Nr. 60
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA203
     nva: 2026-11-14 15:30:34
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA203
    Nr. 61
     DE, Atos Information Technology GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, ATOS.HBA-CA2
     nva: 2028-01-07 09:01:51
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA4
    Nr. 62
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA8
     nva: 2026-08-30 12:40:05
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA8
    Nr. 63
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA10
     nva: 2026-10-02 12:29:43
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA10
    Nr. 64
     DE, Atos Information Technology GmbH, ATOS.HBA-OCSP2
     nva: 2025-01-13 08:43:39
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, ATOS.HBA-CA2
    Nr. 65
     DE, Atos Information Technology GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, ATOS.SMCB-CA2
     nva: 2028-01-07 09:02:02
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA4
    Nr. 66
     DE, Atos Information Technology GmbH, ATOS.SMCB-OCSP2
     nva: 2025-01-13 09:09:05
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, ATOS.SMCB-CA2
    Nr. 67
     DE, Atos Information Technology GmbH, eGK alternative Vers-Ident-CA der Telematikinfrastruktur, ATOS.EGK-ALVI-CA1
     nva: 2028-10-12 09:57:15
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA4
    Nr. 68
     DE, Atos Information Technology GmbH, eGK alternative Vers-Ident-CA der Telematikinfrastruktur, ATOS.EGK-ALVI-CA2
     nva: 2028-10-12 09:57:27
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA4
    Nr. 69
     DE, Atos Information Technology GmbH, eGK alternative Vers-Ident-CA der Telematikinfrastruktur, ATOS.EGK-ALVI-CA4
     nva: 2028-10-12 09:57:42
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA4
    Nr. 70
     DE, Atos Information Technology GmbH, ATOS.EGK-ALVI-OCSP1
     nva: 2025-11-08 12:44:16
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, eGK alternative Vers-Ident-CA der Telematikinfrastruktur, ATOS.EGK-ALVI-CA1
    Nr. 71
     DE, Atos Information Technology GmbH, ATOS.EGK-ALVI-OCSP2
     nva: 2025-11-08 12:45:47
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, eGK alternative Vers-Ident-CA der Telematikinfrastruktur, ATOS.EGK-ALVI-CA2
    Nr. 72
     DE, Atos Information Technology GmbH, ATOS.EGK-ALVI-OCSP4
     nva: 2025-11-08 12:47:21
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, eGK alternative Vers-Ident-CA der Telematikinfrastruktur, ATOS.EGK-ALVI-CA4
    Nr. 73
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA15
     nva: 2026-12-06 10:59:56
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA2
    Nr. 74
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA16
     nva: 2029-05-19 11:00:28
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA4
    Nr. 75
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP15
     nva: 2026-05-25 14:37:52
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA15
    Nr. 76
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP16
     nva: 2026-05-25 14:48:06
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA16
    Nr. 77
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA17
     nva: 2026-12-06 06:14:11
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA2
    Nr. 78
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA18
     nva: 2029-07-13 06:09:32
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA4
    Nr. 79
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP17
     nva: 2026-07-16 11:51:56
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA17
    Nr. 80
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP20
     nva: 2026-08-19 15:27:23
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA20
    Nr. 81
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP19
     nva: 2026-08-19 15:26:52
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA19
    Nr. 82
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA19
     nva: 2026-12-07 08:41:56
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA2
    Nr. 83
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA205
     nva: 2029-10-02 11:49:47
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA5
    Nr. 84
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP204
     nva: 2026-10-11 09:24:08
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA204
    Nr. 85
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP205
     nva: 2026-10-11 09:25:10
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA205
    Nr. 86
     DE, Atos Information Technology GmbH, ATOS.HBA-OCSP3
     nva: 2026-11-17 17:27:15
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, ATOS.HBA-CA3
    Nr. 87
     DE, Atos Information Technology GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, ATOS.SMCB-CA3
     nva: 2029-11-13 14:04:04
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA6
    Nr. 88
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA22
     nva: 2029-11-13 14:03:00
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA6
    Nr. 89
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP21
     nva: 2026-11-17 17:10:48
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA21
    Nr. 90
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA21
     nva: 2029-11-13 14:02:49
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA6
    Nr. 91
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP22
     nva: 2026-11-17 17:11:02
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA22
    Nr. 92
     DE, Atos Information Technology GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, ATOS.HBA-CA3
     nva: 2029-11-13 14:03:43
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA6
    Nr. 93
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP23
     nva: 2026-11-17 17:13:11
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA23
    Nr. 94
     DE, Atos Information Technology GmbH, ATOS.SMCB-OCSP3
     nva: 2026-11-16 17:27:35
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, ATOS.SMCB-CA3
    Nr. 95
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA206
     nva: 2029-11-13 14:03:18
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA6
    Nr. 96
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA23
     nva: 2029-11-13 14:03:07
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA6
    Nr. 97
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP206
     nva: 2026-11-18 13:13:23
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA206
    Nr. 98
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA24
     nva: 2030-03-27 08:19:06
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA5
    Nr. 99
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP25
     nva: 2027-03-31 11:57:35
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA25
    Nr. 100
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP24
     nva: 2027-03-31 11:57:14
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA24
    Nr. 101
     DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA25
     nva: 2030-03-27 08:19:20
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA6
    Nr. 102
     DE, Atos Information Technology GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, ATOS.SMCB-CA4
     nva: 2030-11-20 08:47:04
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA5
    Nr. 103
     DE, Atos Information Technology GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, ATOS.HBA-CA4
     nva: 2030-11-20 08:46:44
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA5
    Nr. 104
     DE, Atos Information Technology GmbH, ATOS.HBA-OCSP4
     nva: 2027-11-29 15:22:45
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, ATOS.HBA-CA4
    Nr. 105
     DE, Atos Information Technology GmbH, ATOS.SMCB-OCSP4
     nva: 2027-11-28 15:23:13
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, ATOS.SMCB-CA4
    Nr. 106
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP10-230809
     nva: 2026-10-01 11:56:15
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA10
    Nr. 107
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP9-230809
     nva: 2026-10-01 11:40:10
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA9
    Nr. 108
     DE, Atos Information Technology GmbH, ATOS.EGK-ALVI-OCSP10
     nva: 2028-10-07 14:43:19
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, eGK alternative Vers-Ident-CA der Telematikinfrastruktur, ATOS.EGK-ALVI-CA10
    Nr. 109
     DE, Atos Information Technology GmbH, ATOS.EGK-ALVI-OCSP9
     nva: 2028-10-07 14:41:33
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, eGK alternative Vers-Ident-CA der Telematikinfrastruktur, ATOS.EGK-ALVI-CA9
    Nr. 110
     DE, Atos Information Technology GmbH, ATOS.EGK-ALVI-OCSP8
     nva: 2028-10-07 14:36:29
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, eGK alternative Vers-Ident-CA der Telematikinfrastruktur, ATOS.EGK-ALVI-CA8
    Nr. 111
     DE, Atos Information Technology GmbH, eGK alternative Vers-Ident-CA der Telematikinfrastruktur, ATOS.EGK-ALVI-CA10
     nva: 2031-09-07 06:33:18
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA5
    Nr. 112
     DE, Atos Information Technology GmbH, eGK alternative Vers-Ident-CA der Telematikinfrastruktur, ATOS.EGK-ALVI-CA9
     nva: 2031-09-07 06:33:18
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA5
    Nr. 113
     DE, Atos Information Technology GmbH, eGK alternative Vers-Ident-CA der Telematikinfrastruktur, ATOS.EGK-ALVI-CA8
     nva: 2031-09-07 06:33:18
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA5
    Nr. 114
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP202-231011
     nva: 2026-11-13 11:30:31
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA202
    Nr. 115
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP203-231011
     nva: 2026-11-13 11:27:59
     algo: ECC
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA203
    Nr. 116
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP12-231207
     nva: 2027-01-29 13:21:45
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA12
    Nr. 117
     DE, Atos Information Technology GmbH, ATOS.EGK-OCSP11-231207
     nva: 2027-01-29 13:10:47
     algo: RSA
     Issuer: DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA11
    Nr. 118
     DE, IBM, SGD1-IBM C.SGD-HSM.AUT 14.09.2020
     nva: 2025-09-13 10:33:03
     algo: ECC
     Issuer: DE, gematik GmbH, Komponenten-CA der Telematikinfrastruktur, GEM.KOMP-CA4
    Nr. 119
     DE, T-Systems International GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, TSYSI.SMCB-CA1
     nva: 2023-12-07 09:45:04
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA1
    Nr. 120
     DE, Deutsche Telekom Security GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, TSYSI.EGK-CA10
     nva: 2030-03-14 10:25:03
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA5
    Nr. 121
     DE, T-Systems International GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, TSYSI.SMCB-CA2
     nva: 2026-12-01 11:03:38
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA2
    Nr. 122
     DE, T-Systems International GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, TSYSI.HBA-CA1
     nva: 2024-03-15 12:47:35
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA1
    Nr. 123
     DE, T-Systems International GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, TSYSI.EGK-CA5
     nva: 2024-05-29 23:59:59
     algo: RSA
     Issuer: DE, T-Systems International GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, TSYSI.EGK-CA5
    Nr. 124
     DE, T-Systems International GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, TSYSI.EGK-CA6
     nva: 2024-05-29 23:59:59
     algo: RSA
     Issuer: DE, T-Systems International GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, TSYSI.EGK-CA6
    Nr. 125
     DE, T-Systems International GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, TSYSI.HBA-CA2
     nva: 2026-12-01 11:04:57
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA2
    Nr. 126
     DE, T-Systems International GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, TSYSI.EGK-CA7
     nva: 2027-05-01 23:59:59
     algo: RSA
     Issuer: DE, T-Systems International GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, TSYSI.EGK-CA7
    Nr. 127
     DE, T-Systems International GmbH, TSYSI.EGK-OCSP-Signer7
     nva: 2024-07-23 23:59:59
     algo: RSA
     Issuer: DE, T-Systems International GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, TSYSI.EGK-CA7
    Nr. 128
     DE, T-Systems International GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, TSYSI.EGK-CA8
     nva: 2027-07-21 23:59:59
     algo: ECC
     Issuer: DE, T-Systems International GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, TSYSI.EGK-CA8
    Nr. 129
     DE, T-Systems International GmbH, TSYSI.EGK-OCSP-Signer8
     nva: 2024-07-23 23:59:59
     algo: ECC
     Issuer: DE, T-Systems International GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, TSYSI.EGK-CA8
    Nr. 130
     DE, Deutsche Telekom Security GmbH - G2 Los 3, TSYSI.HBA-OCSP-Signer3
     nva: 2026-02-12 23:59:59
     algo: ECC
     Issuer: DE, T-Systems International GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, TSYSI.HBA-CA3
    Nr. 131
     DE, T-Systems International GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, TSYSI.HBA-CA3
     nva: 2026-12-01 11:06:33
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA3
    Nr. 132
     DE, Deutsche Telekom Security GmbH - G2 Los 3, TSYSI.SMCB-OCSP-Signer3
     nva: 2026-02-12 23:59:59
     algo: ECC
     Issuer: DE, T-Systems International GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, TSYSI.SMCB-CA3
    Nr. 133
     DE, T-Systems International GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, TSYSI.SMCB-CA3
     nva: 2026-12-01 11:06:49
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA3
    Nr. 134
     DE, Deutsche Telekom Security GmbH, TSYSI.SMCB-OCSP-Signer5
     nva: 2026-10-20 23:59:59
     algo: ECC
     Issuer: DE, Deutsche Telekom Security GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, TSYSI.SMCB-CA5
    Nr. 135
     DE, Deutsche Telekom Security GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, TSYSI.SMCB-CA5
     nva: 2029-10-10 08:15:26
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA5
    Nr. 136
     DE, Deutsche Telekom Security GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, TSYSI.HBA-CA5
     nva: 2029-10-10 08:15:16
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA5
    Nr. 137
     DE, Deutsche Telekom Security GmbH, TSYSI.HBA-OCSP-Signer5
     nva: 2026-10-20 23:59:59
     algo: ECC
     Issuer: DE, Deutsche Telekom Security GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, TSYSI.HBA-CA5
    Nr. 138
     DE, Deutsche Telekom Security GmbH - G2 Los 3, TSYS.HBA-OCSP-Signer4
     nva: 2026-12-08 23:59:59
     algo: RSA
     Issuer: DE, Deutsche Telekom Security GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, TSYSI.HBA-CA4
    Nr. 139
     DE, Deutsche Telekom Security GmbH - G2 Los 3, TSYS.SMCB-OCSP-Signer4
     nva: 2026-12-08 23:59:59
     algo: RSA
     Issuer: DE, Deutsche Telekom Security GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, TSYSI.SMCB-CA4
    Nr. 140
     DE, Deutsche Telekom Security GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, TSYSI.HBA-CA4
     nva: 2029-11-30 08:34:21
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA6
    Nr. 141
     DE, Deutsche Telekom Security GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, TSYSI.SMCB-CA4
     nva: 2029-11-30 08:34:42
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA6
    Nr. 142
     DE, Deutsche Telekom Security GmbH, TSYSI.HBA-OCSP-Signer1-2
     nva: 2024-03-15 12:47:35
     algo: RSA
     Issuer: DE, T-Systems International GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, TSYSI.HBA-CA1
    Nr. 143
     DE, Deutsche Telekom Security GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, TSYSI.EGK-CA9
     nva: 2030-03-14 10:24:21
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA6
    Nr. 144
     DE, Deutsche Telekom Security GmbH, TSYSI.SMCB-OCSP-Signer1-2
     nva: 2023-12-07 09:45:04
     algo: RSA
     Issuer: DE, T-Systems International GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, TSYSI.SMCB-CA1
    Nr. 145
     DE, Deutsche Telekom Security GmbH, TSYSI.SMCB-OCSP-Signer2
     nva: 2026-12-01 11:03:38
     algo: RSA
     Issuer: DE, T-Systems International GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, TSYSI.SMCB-CA2
    Nr. 146
     DE, Deutsche Telekom Security GmbH - G2 Los 3, TSYSI.HBA-OCSP-Signer2-1
     nva: 2026-02-12 23:59:59
     algo: RSA
     Issuer: DE, T-Systems International GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, TSYSI.HBA-CA2
    Nr. 147
     DE, D-TRUST GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, D-Trust.SMCB-CA6
     nva: 2032-01-29 12:23:14
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA8
    Nr. 148
     DE, D-TRUST GmbH, D-Trust.SMCB-CA3-OCSP-Signer-2
     nva: 2026-08-15 07:29:31
     algo: RSA
     Issuer: DE, D-TRUST GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, D-Trust.SMCB-CA3
    Nr. 149
     DE, D-TRUST GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, D-Trust.SMCB-CA3
     nva: 2026-08-15 07:29:31
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA2
    Nr. 150
     DE, D-TRUST GmbH, D-Trust.HBA-CA3-OCSP-Signer-2
     nva: 2026-08-15 07:29:02
     algo: RSA
     Issuer: DE, D-TRUST GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, D-Trust.HBA-CA3
    Nr. 151
     DE, DTRUS, Heilberufsausweis-CA der Telematikinfrastruktur, D-Trust.HBA-CA1
     nva: 2023-12-06 11:56:50
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA1
    Nr. 152
     DE, DTRUS, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, D-Trust.SMCB-CA1
     nva: 2023-12-06 11:57:27
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA1
    Nr. 153
     DE, D-TRUST GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, D-Trust.HBA-CA6
     nva: 2032-01-29 12:22:57
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA8
    Nr. 154
     DE, D-Trust GmbH, D-Trust.SMCB-CA6-OCSP-Signer
     nva: 2029-02-02 12:34:03
     algo: ECC
     Issuer: DE, D-TRUST GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, D-Trust.SMCB-CA6
    Nr. 155
     DE, D-Trust GmbH, D-Trust.HBA-CA6-OCSP-Signer
     nva: 2029-02-02 12:33:24
     algo: ECC
     Issuer: DE, D-TRUST GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, D-Trust.HBA-CA6
    Nr. 156
     DE, D-TRUST GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, D-Trust.HBA-CA3
     nva: 2026-08-15 07:29:02
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA2
    Nr. 157
     DE, D-TRUST GmbH, SGD2-PU
     nva: 2025-09-02 09:21:23
     algo: ECC
     Issuer: DE, gematik GmbH, Komponenten-CA der Telematikinfrastruktur, GEM.KOMP-CA4
    Nr. 158
     DE, D-Trust GmbH, D-Trust.SMCB-CA32-OCSP-Signer
     nva: 2027-09-07 05:42:00
     algo: RSA
     Issuer: DE, D-TRUST GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, D-Trust.SMCB-CA32
    Nr. 159
     DE, D-Trust GmbH, D-Trust.HBA-CA5-OCSP-Signer
     nva: 2026-05-20 11:37:11
     algo: ECC
     Issuer: DE, D-TRUST GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, D-Trust.HBA-CA5
    Nr. 160
     DE, D-Trust GmbH, D-Trust.SMCB-CA5-OCSP-Signer
     nva: 2026-05-20 12:09:38
     algo: ECC
     Issuer: DE, D-TRUST GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, D-Trust.SMCB-CA5
    Nr. 161
     DE, D-TRUST GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, D-Trust.HBA-CA5
     nva: 2029-03-08 11:54:03
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA4
    Nr. 162
     DE, D-TRUST GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, D-Trust.SMCB-CA5
     nva: 2029-03-08 11:54:26
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA4
    Nr. 163
     DE, D-Trust GmbH, D-Trust.SMCB-CA31-OCSP-Signer
     nva: 2027-09-01 13:11:41
     algo: ECC
     Issuer: DE, D-TRUST GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, D-Trust.SMCB-CA31
    Nr. 164
     DE, D-TRUST GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, D-Trust.SMCB-CA32
     nva: 2030-08-30 08:46:54
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA6
    Nr. 165
     DE, D-TRUST GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, D-Trust.SMCB-CA31
     nva: 2030-08-23 09:46:03
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA5
    Nr. 166
     DE, kubus IT GbR, kubus IT OCSP Signer 3 CA1-RSA
     nva: 2024-04-09 14:20:12
     algo: RSA
     Issuer: DE, kubus IT GbR, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, KUBUS.EGK-CA1
    Nr. 167
     DE, kubus IT GbR, kubus IT OCSP Signer 2 CA3-ECC
     nva: 2026-11-24 13:18:51
     algo: ECC
     Issuer: DE, kubus IT GbR, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, KUBUS.EGK-CA3
    Nr. 168
     DE, kubus IT GbR, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, KUBUS.EGK-CA4
     nva: 2026-11-24 13:22:20
     algo: RSA
     Issuer: DE, kubus IT GbR, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, KUBUS.EGK-CA4
    Nr. 169
     DE, kubus IT GbR, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, KUBUS.EGK-CA1
     nva: 2024-04-09 14:20:12
     algo: RSA
     Issuer: DE, kubus IT GbR, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, KUBUS.EGK-CA1
    Nr. 170
     DE, kubus IT GbR, kubus IT OCSP Signer 2 CA4-RSA
     nva: 2026-11-24 13:22:20
     algo: RSA
     Issuer: DE, kubus IT GbR, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, KUBUS.EGK-CA4
    Nr. 171
     DE, kubus IT GbR, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, KUBUS.EGK-CA3
     nva: 2026-11-24 13:18:51
     algo: ECC
     Issuer: DE, kubus IT GbR, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, KUBUS.EGK-CA3
    Nr. 172
     DE, BITMARCK, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, BITMARCK.EGK-CA13
     nva: 2026-07-25 12:57:33
     algo: RSA
     Issuer: DE, BITMARCK, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, BITMARCK.EGK-CA13
    Nr. 173
     DE, BITMARCK, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, BITMARCK.EGK-CA14
     nva: 2026-12-03 09:56:17
     algo: RSA
     Issuer: DE, BITMARCK, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, BITMARCK.EGK-CA14
    Nr. 174
     DE, BITMARCK, BITMARCK OCSP SIGNER 3
     nva: 2026-07-13 13:45:28
     algo: RSA
     Issuer: DE, BITMARCK, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, BITMARCK.EGK-CA14
    Nr. 175
     DE, BITMARCK, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, BITMARCK.EGK-CA12
     nva: 2026-10-23 09:35:16
     algo: ECC
     Issuer: DE, BITMARCK, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, BITMARCK.EGK-CA12
    Nr. 176
     DE, BITMARCK, SGD1-EPA-PU-BITMARCK-2020-11-18
     nva: 2025-11-17 14:23:39
     algo: ECC
     Issuer: DE, gematik GmbH, Komponenten-CA der Telematikinfrastruktur, GEM.KOMP-CA4
    Nr. 177
     DE, BITMARCK, eGK alternative Vers-Ident-CA der Telematikinfrastruktur, BITMARCK.EGK-ALVI-CA27
     nva: 2028-11-16 10:05:07
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA4
    Nr. 178
     DE, BITMARCK Technik GmbH, BITMARCK.EGK-OCSP_SIGNER_27
     nva: 2025-11-18 17:16:08
     algo: ECC
     Issuer: DE, BITMARCK, eGK alternative Vers-Ident-CA der Telematikinfrastruktur, BITMARCK.EGK-ALVI-CA27
    Nr. 179
     DE, BITMARCK, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, BITMARCK.EGK-CA28
     nva: 2029-10-25 12:06:14
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA5
    Nr. 180
     DE, BITMARCK Technik GmbH, BITMARCK.EGK-OCSP_SIGNER_28
     nva: 2026-11-02 12:31:53
     algo: ECC
     Issuer: DE, BITMARCK, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, BITMARCK.EGK-CA28
    Nr. 181
     DE, BITMARCK Technik GmbH, BITMARCK.EGK-OCSP_SIGNER_13
     nva: 2026-07-25 12:57:33
     algo: RSA
     Issuer: DE, BITMARCK, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, BITMARCK.EGK-CA13
    Nr. 182
     DE, BITMARCK Technik GmbH, BITMARCK.EGK-OCSP_SIGNER_26
     nva: 2026-11-28 12:34:21
     algo: RSA
     Issuer: DE, BITMARCK, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, BITMARCK.EGK-CA26
    Nr. 183
     DE, BITMARCK, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, BITMARCK.EGK-CA26
     nva: 2029-11-21 10:14:29
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA6
    Nr. 184
     DE, BITMARCK, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, BITMARCK.EGK-CA30
     nva: 2031-02-07 08:31:05
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA5
    Nr. 185
     DE, BITMARCK, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, BITMARCK.EGK-CA29
     nva: 2031-02-07 08:32:20
     algo: RSA
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA6
    Nr. 186
     DE, BITMARCK Technik GmbH, BITMARCK.EGK-OCSP_SIGNER_30
     nva: 2028-02-21 16:09:12
     algo: ECC
     Issuer: DE, BITMARCK, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, BITMARCK.EGK-CA30
    Nr. 187
     DE, BITMARCK Technik GmbH, BITMARCK.EGK-OCSP_SIGNER_29
     nva: 2028-02-21 16:10:31
     algo: RSA
     Issuer: DE, BITMARCK, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, BITMARCK.EGK-CA29
    Nr. 188
     DE, BITMARCK Technik GmbH, BITMARCK.EGK-OCSP_SIGNER_12_001
     nva: 2026-10-23 09:35:16
     algo: ECC
     Issuer: DE, BITMARCK, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, BITMARCK.EGK-CA12
    Nr. 189
     DE, BITMARCK Technik GmbH, BITMARCK.EGK-OCSP_SIGNER_34
     nva: 2028-10-30 13:11:51
     algo: ECC
     Issuer: DE, BITMARCK, eGK alternative Vers-Ident-CA der Telematikinfrastruktur, BITMARCK.EGK-ALVI-CA34
    Nr. 190
     DE, BITMARCK, eGK alternative Vers-Ident-CA der Telematikinfrastruktur, BITMARCK.EGK-ALVI-CA34
     nva: 2031-09-07 06:33:18
     algo: ECC
     Issuer: DE, gematik GmbH, Zentrale Root-CA der Telematikinfrastruktur, GEM.RCA5
    Nr. 191
     German Trusted List Signer 9, Federal Network Agency, DE
     nva: 2025-03-23 09:00:00
     algo: RSA
     Issuer: German Trusted List Signer 9, Federal Network Agency, DE
    Nr. 192
     German Trusted List Signer 10, Federal Network Agency, DE
     nva: 2025-06-23 08:00:00
     algo: RSA
     Issuer: German Trusted List Signer 10, Federal Network Agency, DE
    Nr. 193
     German Trusted List Signer 11, Federal Network Agency, DE
     nva: 2025-07-04 11:03:02
     algo: RSA
     Issuer: German Trusted List Signer 11, Federal Network Agency, DE
    Nr. 194
     German Trusted List Signer 12, Federal Network Agency, DE
     nva: 2025-10-05 11:07:19
     algo: RSA
     Issuer: German Trusted List Signer 12, Federal Network Agency, DE
