#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import datetime, glob, json, sys

from base64 import b64encode
from binascii import hexlify
from collections import defaultdict
from cryptography import x509
from cryptography.hazmat.backends import default_backend


if __name__ == '__main__':

    if len(sys.argv)<2:
        sys.exit("FEHLER: benötige cert-der files als argv")

    h2n = defaultdict(lambda: "UNKNOWN")
    h2n["None"] = "selfsigned"
    ausgabe = []

    for cert_filename in sys.argv[1:]:
        with open(cert_filename, "rb") as cert_file:
            cert_data = cert_file.read()
            cert = x509.load_der_x509_certificate(cert_data, default_backend())
            ski = None
            aki = None
            for ext in cert.extensions:
                if ext.oid._name == 'subjectKeyIdentifier':
                    ski = ext.value.digest
                    # erst ab cryptography 35.0 kann man anstatt digest
                    # auch key_identifier nehmen
                elif ext.oid._name == 'authorityKeyIdentifier':
                    aki = ext.value.key_identifier

            assert ski
            ski = hexlify(ski).decode()
            if aki:
                aki = hexlify(aki).decode()

            cn = None
            for subject_element in cert.subject:
                if subject_element.oid.dotted_string == '2.5.4.3':
                    cn = subject_element.value
            assert cn

            if ski in h2n:
                if h2n[ski] != cn:
                    print("WARNING: Neudefinition")
            else:
                h2n[ski] = cn

            ausgabe.append([cn, ski, aki])

    for [cn, ski, aki] in ausgabe:
         print(cn, ski, h2n[ski], aki, h2n[aki])

