#! /bin/bash

for a in \
    GEM.RCA1-CROSS-GEM.RCA2.der \
    GEM.RCA1.der                \
    GEM.RCA2-CROSS-GEM.RCA1.der \
    GEM.RCA2-CROSS-GEM.RCA6.der \
    GEM.RCA2.der                \
    GEM.RCA3-CROSS-GEM.RCA2.der \
    GEM.RCA3-CROSS-GEM.RCA4.der \
    GEM.RCA3.der                \
    GEM.RCA4-CROSS-GEM.RCA3.der \
    GEM.RCA4-CROSS-GEM.RCA5.der \
    GEM.RCA4.der                \
    GEM.RCA5-CROSS-GEM.RCA4.der \
    GEM.RCA5-CROSS-GEM.RCA6.der \
    GEM.RCA5.der                \
    GEM.RCA6-CROSS-GEM.RCA2.der \
    GEM.RCA6-CROSS-GEM.RCA5.der \
    GEM.RCA6-CROSS-GEM.RCA7.der \
    GEM.RCA6.der                \
    GEM.RCA7-CROSS-GEM.RCA6.der \
    GEM.RCA7-CROSS-GEM.RCA8.der \
    GEM.RCA7.der                \
    GEM.RCA8-CROSS-GEM.RCA7.der \
    GEM.RCA8.der                
do
    echo $a
    if [ -f $a ]; then
        echo $a existiert schon lokal
    else
        wget https://download.tsl.ti-dienste.de/ECC/ROOT-CA/$a
        sleep 5
    fi
done

