#! /bin/bash

for a in \
     GEM.RCA1_TEST-ONLY-CROSS-GEM.RCA2_TEST-ONLY.der         \
     GEM.RCA1_TEST-ONLY.der                                  \
     GEM.RCA2_TEST-ONLY-CROSS-GEM.RCA1_TEST-ONLY.der         \
     GEM.RCA2_TEST-ONLY-CROSS-GEM.RCA3_TEST-ONLY.der         \
     GEM.RCA2_TEST-ONLY-CROSS-GEM.RCA6_TEST-ONLY.der         \
     GEM.RCA2_TEST-ONLY.der                                  \
     GEM.RCA3_TEST-ONLY-CROSS-GEM.RCA2_TEST-ONLY.der         \
     GEM.RCA3_TEST-ONLY-CROSS-GEM.RCA4_TEST-ONLY.der         \
     GEM.RCA3_TEST-ONLY.der                                  \
     GEM.RCA4_TEST-ONLY-CROSS-GEM.RCA3_TEST-ONLY.der         \
     GEM.RCA4_TEST-ONLY-CROSS-GEM.RCA5_TEST-ONLY.der         \
     GEM.RCA4_TEST-ONLY.der                                  \
     GEM.RCA5_TEST-ONLY-CROSS-GEM.RCA4_TEST-ONLY.der         \
     GEM.RCA5_TEST-ONLY-CROSS-GEM.RCA6_TEST-ONLY.der         \
     GEM.RCA5_TEST-ONLY.der                                  \
     GEM.RCA6_TEST-ONLY-CROSS-GEM.RCA2_TEST-ONLY.der         \
     GEM.RCA6_TEST-ONLY-CROSS-GEM.RCA5_TEST-ONLY.der         \
     GEM.RCA6_TEST-ONLY.der                                  \
     GEM.RCA7_TEST-ONLY-CROSS-GEM.RCA6_TEST-ONLY.der         \
     GEM.RCA7_TEST-ONLY-CROSS-GEM.RCA8_TEST-ONLY.der         \
     GEM.RCA7_TEST-ONLY.der                                  \
     GEM.RCA8_TEST-ONLY-CROSS-GEM.RCA7_TEST-ONLY.der         \
     GEM.RCA8_TEST-ONLY.der                                   
do 
    echo $a
    if [ -f $a ]; then
        echo $a existiert schon lokal
    else
        wget https://download-test.tsl.ti-dienste.de/ECC/ROOT-CA/$a
        sleep 5
    fi
done

