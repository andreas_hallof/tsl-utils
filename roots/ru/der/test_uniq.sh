#! /usr/bin/bash


counter=$(ls *.der | wc -l)

testcounter=$(ls *.der | while read a; do sha256sum $a; done | cut -f1 -d\  | sort -u | wc -l)

if [ "$counter" != "$testcounter" ]; then
    echo PROBLEM
    echo $counter $testcounter
    ls *.der | while read a; do sha256sum $a; done | sort
else
    echo OK
fi

