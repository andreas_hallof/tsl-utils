#! /bin/bash

# https://raymii.org/s/articles/OpenSSL_Manually_Verify_a_certificate_against_an_OCSP.html

url=$(openssl x509 -noout -ocsp_uri -in GEM.RCA4.pem )

openssl ocsp -issuer GEM.RCA4.pem -cert GEM.RCA4.pem -text -url $url

