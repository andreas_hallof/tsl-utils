# verify

    $ cat verify.sh
    #! /usr/bin/bash

    openssl verify -verbose -CAfile GEM.RCA5.pem GEM.RCA6-CROSS-GEM.RCA5.pem
    openssl verify -verbose -CAfile GEM.RCA6.pem GEM.RCA7-CROSS-GEM.RCA6.pem
    openssl verify -verbose -CAfile GEM.RCA7.pem GEM.RCA8-CROSS-GEM.RCA7.pem

    $ ./verify.sh
    GEM.RCA6-CROSS-GEM.RCA5.pem: OK
    GEM.RCA7-CROSS-GEM.RCA6.pem: OK
    GEM.RCA8-CROSS-GEM.RCA7.pem: OK

