#! /usr/bin/bash

if [ ! -f "$1" ]; then
        echo $1 not found
else
        pem_name=$(echo $1 | sed 's/\.der$/\.pem/;')
        if [ "$1" = "$pem_name" ]; then
                echo Fehler
                exit 1
        fi
        openssl x509 -inform der -in "$1" -outform pem -out $pem_name
fi

