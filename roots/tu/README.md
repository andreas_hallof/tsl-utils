# Zertifikate ansehen

## wer bestätigt wenn

     ../cert-info.py der/*.der| grep -v None| sed s'/TEST-ONLY//g' | awk '{ print $5, "->", $3 }' |sort

    GEM.RCA1 -> GEM.RCA2

    GEM.RCA2 -> GEM.RCA1
    GEM.RCA2 -> GEM.RCA3

    GEM.RCA3 -> GEM.RCA4

    GEM.RCA4 -> GEM.RCA3
    GEM.RCA4 -> GEM.RCA5

    GEM.RCA5 -> GEM.RCA4
    GEM.RCA5 -> GEM.RCA6

    GEM.RCA6 -> GEM.RCA5
    GEM.RCA6 -> GEM.RCA7

    GEM.RCA7 -> GEM.RCA6
    GEM.RCA7 -> GEM.RCA8

    GEM.RCA8 -> GEM.RCA7


# wer wird von wem bestätigt

    ../cert-info.py der/*.der| grep -v None| sed s'/TEST-ONLY//g' | awk '{ print $3, "<-", $5 }' | sort

    GEM.RCA1 <- GEM.RCA2

    GEM.RCA2 <- GEM.RCA1

    GEM.RCA3 <- GEM.RCA2
    GEM.RCA3 <- GEM.RCA4

    GEM.RCA4 <- GEM.RCA3
    GEM.RCA4 <- GEM.RCA5

    GEM.RCA5 <- GEM.RCA4
    GEM.RCA5 <- GEM.RCA6

    GEM.RCA6 <- GEM.RCA5
    GEM.RCA6 <- GEM.RCA7

    GEM.RCA7 <- GEM.RCA6
    GEM.RCA7 <- GEM.RCA8

    GEM.RCA8 <- GEM.RCA7
