#! /usr/bin/bash

if [ ! -f "$1" ]; then 
        echo cert-file: "$1" not found
else
        openssl x509 -inform der -in $1 -text -noout
fi

