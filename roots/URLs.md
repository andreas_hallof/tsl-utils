# PU

https://download.tsl.ti-dienste.de/ECC/ROOT-CA/

# TU

RSA-only: https://download-test.tsl.ti-dienste.de/ROOT-CA/
ECC+RSA: https://download-test.tsl.ti-dienste.de/ECC/ROOT-CA/

# RU

RSA-only: https://download-ref.tsl.ti-dienste.de/ROOT-CA/
ECC+RSA: https://download-ref.tsl.ti-dienste.de/ECC/ROOT-CA/

