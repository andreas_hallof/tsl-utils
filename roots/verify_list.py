#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import os, sys, hashlib

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.exceptions import InvalidSignature

for cert_der in list_data["certs"]:
    cert =  x509.load_der_x509_certificate(cert_der, default_backend())
    print("Zertifikat", hashlib.sha256(cert_der).hexdigest(), 
            ', '.join(x for x in [y.value for y in cert.subject]))

