#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import datetime, glob, json

from base64 import b64encode
from binascii import hexlify
from cryptography import x509
from cryptography.hazmat.backends import default_backend


if __name__ == '__main__':

    pre_roots = dict(); cross = dict(); error_counter = 0
    ski_to_name = dict();

    for cert_filename in glob.glob("der/*.der"):
        with open(cert_filename, "rb") as cert_file:
            cert_data = cert_file.read()
            cert = x509.load_der_x509_certificate(cert_data, default_backend())
            ski = None
            aki = None
            for ext in cert.extensions:
                if ext.oid._name == 'subjectKeyIdentifier':
                    ski = ext.value.digest
                    # erst ab cryptography 35.0 kann man anstatt digest
                    # auch key_identifier nehmen
                elif ext.oid._name == 'authorityKeyIdentifier':
                    aki = ext.value.key_identifier

            assert ski

            if not aki:
                pre_roots[cert.not_valid_before.isoformat()] = [cert, cert_data, ski]
            else:
                my_key = hexlify(aki).decode() + hexlify(ski).decode()
                cross[my_key] = b64encode(cert_data).decode()
                print("DEBUG: add cross", my_key)
                
    roots = []
    for entry in sorted(pre_roots):
       (cert, cert_data, ski) = pre_roots[entry]
       element = {"cert": b64encode(cert_data).decode(),
                  "nvb" : cert.not_valid_before.isoformat(),
                  "nva" : cert.not_valid_after.isoformat(),
                  "ski" : hexlify(ski).decode()
                 }
       cn = None
       for subject_element in cert.subject:
            if subject_element.oid.dotted_string == '2.5.4.3':
                cn = subject_element.value
       assert cn
       element['cn'] = cn
       name = cn.replace('GEM.','')
       element['name'] = name
       ski_to_name[element['ski']] = name

       roots.append(element)

    max_index = len(roots)-1
    assert max_index > 0
    for my_index in range(0,max_index+1):
        if my_index < max_index:
            my_key = roots[my_index]["ski"] + roots[my_index+1]["ski"]
            #assert my_key in cross
            if not my_key in cross:
                my_from = roots[my_index]["ski"]
                my_to   = roots[my_index+1]["ski"]
                print("WARNING NOT FOUND (next)", my_from, ski_to_name[my_from],
                        "->", my_to, ski_to_name[my_to])
                cross[my_key] = ""
                error_counter += 1
            roots[my_index]["next"] = cross[my_key]
        else:
            roots[my_index]["next"] = ""

        if my_index > 0:
            my_key = roots[my_index]["ski"] + roots[my_index-1]["ski"]
            #assert my_key in cross
            if not my_key in cross:
                my_from = roots[my_index]["ski"]
                my_to   = roots[my_index-1]["ski"]
                print("WARNING NOT FOUND (prev)", my_from, ski_to_name[my_from],
                        "->", my_to, ski_to_name[my_to])
                cross[my_key] = ""
                error_counter += 1
            roots[my_index]["prev"] = cross[my_key]
        else:
            roots[my_index]["prev"] = ""
        
   
    #print(json.dumps(roots, sort_keys=True, indent=4))

    with open("roots.json", "wt") as roots_file:
        roots_file.write(json.dumps(roots, sort_keys=True, indent=4))

    if error_counter > 0:
        print("ACHTUNG: Anzahl der Fehler bei der Verarbeitung", error_counter)
