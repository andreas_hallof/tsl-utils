#! /usr/bin/env bash

hash wget 2>/dev/null || { echo >&2 "Ich benötige wget und kann es aber nicht finden."; exit 1; }

if [ "$1" = "--overwrite" ];
then
        rm -f TSL.xml ECC-RSA_TSL.xml ECC-RSA_TSL.sig
fi

# if [ -f TSL.xml ]
# then
# 	echo >&2 "Es exitiert bereits eine TSL.xml, bitte vorher diese löschen oder umbenennen."
# 	exit 
# fi
# 
# wget -nv https://download.tsl.ti-dienste.de/TSL.xml

if [ -f ECC-RSA_TSL.xml ]
then
	echo >&2 "Es exitiert bereits eine ECC+RSA TSL (ECC-RSA_TSL.xml), bitte vorher diese löschen oder umbenennen."
	exit 
fi

#wget -nv https://download.tsl.ti-dienste.de/ECC/ECC-RSA_TSL.xml
wget -nv http://download.crl.ti-dienste.de/TSL-ECC/ECC-RSA_TSL.xml 
wget -nv http://download.crl.ti-dienste.de/TSL-ECC/ECC-RSA_TSL.sig


# Hinweis: die TSL für TU und RU gibt es unter
# http://download-testref.crl.ti-dienste.de/
# http://download-testref.crl.ti-dienste.de/TSL-ECC-test/ECC-RSA_TSL-test.xml
#

