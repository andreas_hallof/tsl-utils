#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import argparse, logging, sys, os, hashlib, base64, re, datetime

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec, rsa, padding
from cryptography.hazmat.primitives import hashes
from cryptography.exceptions import InvalidSignature

from xml.etree import ElementTree

# Vertrauensanker aus GEM.TLS-CA1.der
RSA_Public_Key_TSL_Signer_CA = rsa.RSAPublicNumbers(
            0x10001,
            0xE6D3E676388C37619C0495403F071625854F67AC726233518FD8E465689BEA2A27C36A476D696A02DDDD4AE505ED30EFD41608EE498064E162DB1D98C6E41AB0E79058F46E400A85EE7D1FD41A1FBD7F8DDE6AC98F7F83997C1D8B3BA64446F5B3792EFCAB1135D431A55334A0CB1577729A33A5AC11BA8F1A80BB6A21125C1DEB676C3E4D48E0652B1BBC127A3FB620B445919C556C6006AC785250FC1EE2D84159F82E4F93117D9375E8CD893A096DB7D5FAF6EECCF240F5951C62713A75EE8713E7E061EF4DA3BB625A379DD7957C31E67F5CA68743ADF9D1F519ED75AC1C8AAE7BF5CA5092CB6F2E0068F7ED06907EB13E7C5A55D5FBD2C22DFB9A816C85
)

ECC_Public_Key_TSL_Signer_CA = ec.EllipticCurvePublicNumbers(
            x=0x33f30e5392e9f79a01fe9053d17c87d9a08d37ec6244917403319f557d66c7cf,
            y=0x081b37ceba680bdd4f074c96a0521228c093ddff32befe180338d920d8c82c65,
            curve=ec.BrainpoolP256R1()
)

ScriptAge = datetime.datetime.fromtimestamp(os.path.getmtime(sys.argv[0]))
if ScriptAge + datetime.timedelta(days=365) < datetime.datetime.now():
    print("FAIL: bitte tsl-utils aktualisieren (erscheint älter als ein Jahr zu sein).")
    sys.exit(1)

a_p = argparse.ArgumentParser(description="Signaturprüfung einer TSL-XML-Datei / TI-PU")
a_p.add_argument('-v', '--verbose', dest='verbose', action='store_true', help='verbose mode')
a_p.add_argument(dest='TSL_Datei', metavar='TSL-XML-Datei')
args = a_p.parse_args()

logging.basicConfig(level=logging.DEBUG,
                    format='[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
                    datefmt="%Y-%m-%dT%H:%M:%S%z")

if not (os.path.exists(args.TSL_Datei) and os.path.isfile(args.TSL_Datei)):
    sys.exit('Datei "{}" nicht gefunden'.format(args.TSL_Datei))

if not sys.platform in ['cygwin', 'linux']:
    logging.warning("Skript auf Plattform '{}' noch nicht getestet." % sys.platform)

# gesamte TSL einlesen
## Das newline='\n' habe ich eingefügt um auf nicht-cygwin-windows-installationen von 
## python zu funktionieren.
with open(args.TSL_Datei, 'r', newline='\n') as f:
    data = f.read()

# <xml .. encoding=""> ist nicht Teil einer XML-Signatur -> entfernen.
data = re.sub(r"^<\?xml[^>]+>\n", "", data, re.M)

# XML-Signatur suchen
m = re.findall(r"<ds:Signature[^VM]", data, flags=re.M+re.I)
if len(m) > 1:
    sys.exit("FAIL: mehr als eine XML-Signatur in den Daten gefunden -> keine valide TSL.")

signature = re.search("(<ds:Signature.+</ds:Signature>)", data,
                      flags=re.M+re.S)

if signature == None:
    sys.exit("FAIL: keine XML-Signatur in den Daten gefunden.") 

Signature_String = signature.group()

#
#  Das "signierende" X.509-Zertifikat extrahieren und prüfen.
#
#

m = re.findall(r"<ds:X509Certificate>.+</ds:X509Certificate>", Signature_String,
               flags=re.M+re.S)
if len(m) != 1:
    sys.exit("FAIL: entweder kein oder zu viele Zertifikate in der XML-Signatur enthalten")

c = m[0]; c = re.sub("<[^>]+>", "", c); c = re.sub("&#xD;", "", c)
cert_der = base64.b64decode(c)
cert = x509.load_der_x509_certificate(cert_der, default_backend())

try:
    checked = False
    if isinstance(cert.public_key(), rsa.RSAPublicKey):
        RSA_Public_Key_TSL_Signer_CA.public_key(default_backend()).verify( 
                cert.signature,
                cert.tbs_certificate_bytes, 
                padding.PKCS1v15(), 
                hashes.SHA256() 
        )
        checked = True
    else:
        ECC_Public_Key_TSL_Signer_CA.public_key(default_backend()).verify( 
                cert.signature,
                cert.tbs_certificate_bytes, 
                ec.ECDSA(hashes.SHA256())
        )
        checked = True

except InvalidSignature:
    sys.exit("FAIL: das signierende Zertifikat ist nicht Teil der TI-PKI.")
else:
    if not checked:
        sys.exit("FAIL: das signierende Zertifikat ist nicht Teil der TI-PKI.")

TSL_Signer = x509.load_der_x509_certificate(cert_der, default_backend())
print('Signierendes Zertifikat ("{}", SN: {}, gültig bis {}) ist Teil der TI-PKI.'.format(
          ', '.join(x for x in [ y.value for y in TSL_Signer.subject ]),
          TSL_Signer.serial_number,
          TSL_Signer.not_valid_after.isoformat(timespec='minutes')
       )
     )

if TSL_Signer.not_valid_after < datetime.datetime.now():
    sys.exit("FAIL: das signierende Zertifikat ist zeitlich nicht mehr gültig.")

#
# Die Signatur der Daten des SignedInfo-Blocks verifizieren.
#
#

m = re.findall(r"<ds:SignedInfo", Signature_String, flags=re.M+re.I)
if len(m) != 1:
    sys.exit("FAIL: nicht genau ein SignedInfo-Block -> keine valide TSL.")

m = re.findall(r"(<ds:SignedInfo>.+</ds:SignedInfo>)", Signature_String, flags=re.M+re.S)
if len(m) != 1:
    sys.exit("FAIL: Fehler beim Finden des SignedInfo-Blocks.")

SignedInfo = m[0]
SignedInfo = SignedInfo.replace('<ds:SignedInfo>', '<ds:SignedInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#">')

m = re.findall(r"<ds:SignatureValue", Signature_String, flags=re.M+re.I)
if len(m) != 1:
    sys.exit("FAIL: nicht genau ein SignatureValue-Block -> keine valide TSL.")

m = re.findall(r"<ds:SignatureValue>(.+)</ds:SignatureValue", Signature_String, flags=re.M+re.S)
if len(m) != 1:
    sys.exit("FAIL: Fehler beim Finden des SignatureValue-Blocks.")

Signature_Value_b64 = m[0]
Signature_Value_b64 = re.sub("&#xD;", "", Signature_Value_b64)
Signature_Value = base64.b64decode(Signature_Value_b64)
hash_SignedInfo = hashlib.sha256(SignedInfo.encode("utf-8")).digest()


#
# Aus Spass an der Freude habe ich die RSASSA-PSS-Überprüfung mal selbst
# implementiert. Funktioniert, aber letztendlich immer lieber openssl
# das machen lassen -> Selbst_PSS_Verifizieren=False
#

Selbst_PSS_Verifizieren = False

if Selbst_PSS_Verifizieren:

    # RSASSA-PSS selbst prüfen
    #
    # http://www.w3.org/2007/05/xmldsig-more#sha256-rsa-MGF1
    # salt_length same as output hash length => 32
    # ( https://tools.ietf.org/html/rfc6931#section-2.3.10 )
    #
    try:
        x = TSL_Signer.public_key().public_numbers()
        s = int.from_bytes(Signature_Value, byteorder='big')
        m = pow(s, x.e, x.n)
        mb = m.to_bytes((m.bit_length() + 7) // 8, 'big')

        #4.   If the rightmost octet of EM does not have hexadecimal value
        #     0xbc, output "inconsistent" and stop.

        if mb[-1] != 0xbc:
            raise InvalidSignature

        #5.   Let maskedDB be the leftmost emLen - hLen - 1 octets of EM,
        #     and let H be the next hLen octets.

        maskedDB = mb[:-(32 + 1)]
        H = mb[-33:-1]


        #7.   Let dbMask = MGF(H, emLen - hLen - 1).

        dbMask = b''; counter = 0
        while len(dbMask) < len(maskedDB):
            tmp_hash = hashlib.sha256(H + b'\0\0\0' + bytes([counter])).digest()
            dbMask = dbMask + tmp_hash
            counter += 1
        
        #8.   Let DB = maskedDB \xor dbMask.

        DB = [a ^ b for (a, b) in zip(maskedDB, dbMask)]

        print("DB", len(DB), type(DB))
        for i in DB:
            print("{:4x}".format(i), end='')
        print()

        #11.  Let salt be the last sLen octets of DB.

        salt = DB[-32:]
        print("salt", len(salt), type(salt))

        for i in salt:
            print("{:4x}".format(i), end='')
        print()

        #12.  Let
        #
        #     M' = (0x)00 00 00 00 00 00 00 00 || mHash || salt ;
        #
        #  M' is an octet string of length 8 + hLen + sLen with eight
        #  initial zero octets.
        
        m_prime = b'\0\0\0\0\0\0\0\0' + hash_SignedInfo + bytes(salt)

        #13.  Let H' = Hash(M'), an octet string of length hLen.

        H_prime = hashlib.sha256(m_prime).digest()

        print("H", len(H))
        for x in H:
            print("{:2x}".format(x), end="")
        print()

        print("H_prime", len(H_prime))
        for x in H_prime:
            print("{:2x}".format(x), end="")
        print()

        # 14.  If H = H', output "consistent".  Otherwise, output
        #      "inconsistent".

        if H != H_prime:
            print("FAIL: Die Signatur über den SignedInfo-Block stimmt nicht.")
            sys.exit(1)

    except InvalidSignature:
        sys.exit("FAIL: Die Signatur über den SignedInfo-Block stimmt nicht.")
else:

    try:
        # http://www.w3.org/2007/05/xmldsig-more#sha256-rsa-MGF1
        # salt_length same as output hash length => 32
        # ( https://tools.ietf.org/html/rfc6931#section-2.3.10 )
        
        if isinstance(TSL_Signer.public_key(), rsa.RSAPublicKey):
            TSL_Signer.public_key().verify( 
                    Signature_Value,
                    SignedInfo.encode("utf-8"),
                    padding.PSS(mgf=padding.MGF1(hashes.SHA256()), salt_length=32),
                    hashes.SHA256() 
            )
        else:
            print(SignedInfo)

            SignedInfo = SignedInfo.replace("<ds:", "<")
            SignedInfo = SignedInfo.replace("</ds:", "</")

            print(SignedInfo)

            TSL_Signer.public_key().verify( 
                    Signature_Value,
                    SignedInfo.encode("utf-8"),
                    ec.ECDSA(hashes.SHA256())
            )
    except InvalidSignature:
        sys.exit("FAIL: Die Signatur über den SignedInfo-Block stimmt nicht.")

m = re.findall(r'<ds:Reference.+URI="">.+?<ds:DigestValue>(.+?)</ds:DigestValue>', Signature_String, flags=re.M+re.S)
if len(m) != 1:
    sys.exit("FAIL: kein passender DigestValue im SignedInfo-Block -> keine valide TSL.") 

Bestaetigter_Hashwert_TSL_Daten = m[0].encode()

#
#
# Entfernen der XML-Signatur um die "reinen" Daten zu erhalten. Davon wird der Hashwert 
# errechnet, dieser findet sich dann wieder im ersten Referenz-Block (URL="") der XML-Signatur
# (SignedInfo).
# Der Hashwert über (in unserem Fall) alle Referenzblöcke wird durch die kryptographische
# Signatur am Ende bestätigt.
#
# Da die TSL-Daten schon bei bei der Erstellung mit der C14N-Methode "normalisiert" sind,
# kann ich mir das sparen.
#
#

data = re.sub("<ds:Signature.+</ds:Signature>", "", data, flags=re.M+re.S)
h = hashlib.sha256(data.encode("utf-8"))
b64_tsl_data = base64.b64encode(h.digest())

# SHA-256-Wert der (indirekt) über die Signatur zu bestätigenden Daten: b64_tsl_data

if b64_tsl_data != Bestaetigter_Hashwert_TSL_Daten:
    sys.exit("FAIL: der Hashwert der TSL-Daten stimmt nicht mit dem im SignedInfo-Block bestätigten Hashwert überein.")


print("kryptographische Verifikation der TSL-Daten: OK")


xml_doc_root = ElementTree.parse(args.TSL_Datei).getroot()
if xml_doc_root.tag != '{http://uri.etsi.org/02231/v2#}TrustServiceStatusList':
    sys.exit("keine gültige TI-TSL")

if 'Id' in xml_doc_root.attrib:
    print('TSL id {}'.format(xml_doc_root.attrib['Id']))
else:
    sys.exit("keine gültige TI-TSL")

ns = {'tsl': 'http://uri.etsi.org/02231/v2#'} # ElementTree.register_namespace('tsl', ns['tsl'])

c1 = xml_doc_root.findall(r'./tsl:SchemeInformation/tsl:ListIssueDateTime', ns)
print('TSL erzeugt am', c1[0].text)
if datetime.datetime.now() < datetime.datetime.strptime(c1[0].text, '%Y-%m-%dT%H:%M:%SZ'):
    logging.warning("WARNUNG: lokale Zeit ist falsch.")
c1 = xml_doc_root.findall(r'./*/tsl:NextUpdate/tsl:dateTime', ns)
print("nächstes Update", c1[0].text)
if datetime.datetime.strptime(c1[0].text, '%Y-%m-%dT%H:%M:%SZ') < datetime.datetime.now():
    logging.warning("WARNUNG: TSL-Gültigkeit abgelaufen oder lokale Zeit ist falsch.")


sys.exit(0)

