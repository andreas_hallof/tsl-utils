#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import datetime, jwt, glob, base64, cbor

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization, hashes

signer_cert = open("pki/signer_cert.der", "rb").read()
signing_key_pem = open("pki/signing_key.pem", "rb").read()

signing_key = serialization.load_pem_private_key(signing_key_pem, password=None,
              backend=default_backend())

## So ich habe es erst mit einer JSON Web Signature umgesetzt.
## Funktioniert -- aber so so richtig rockt das nicht.

#list_data = {
#            "version" : 1,
#            "datetime" : datetime.datetime.now().isoformat(),
#            "certs" : []
#            }
#
#for filename in glob.glob("certs/*.der"):
#    with open(filename, "rb") as f:
#        certdata = base64.b64encode(f.read()).decode()
#        list_data["certs"].append(certdata)
#
#token = jwt.encode( list_data , signing_key, 'ES256' )
#
#with open("t", "wb") as f:
#    f.write(token)
#

list_data = {"version" : 1,
             "datetime" : datetime.datetime.now().isoformat(),
             "certs" : []
            }

for filename in glob.glob("certs/*.der"):
    with open(filename, "rb") as f:
        list_data["certs"].append(f.read())

list_data_encoded = cbor.dumps(list_data)
signature = signing_key.sign(list_data_encoded, ec.ECDSA(hashes.SHA256()))
list_2 = [list_data_encoded, signer_cert, signature]
list_2_encoded = cbor.dumps(list_2)

with open("liste.cbor", "wb") as f:
    f.write(list_2_encoded)

