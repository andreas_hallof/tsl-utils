#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import cbor, os, sys, hashlib

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.exceptions import InvalidSignature

if not os.path.exists("liste.cbor"):
    sys.exit("Sie müssen erst mit gen-n-sign.py die Datei 'liste.cbor' erstellen")

list_2 = cbor.loads(open("liste.cbor", "rb").read())
assert len(list_2) == 3

if os.path.exists("pki/signer.cert"):
    # lokal integeres Signer-Zertifikat vorhanden
    assert list_2[1] == open("pki/signer.cert").read()
else:
    # Es gibt also lokal keinen Signer und es wird zum Test die
    # Signatur von https://tipkg.de/liste.cbor geprüft. Dafür gibt
    # es hier dessen Signer fixiert über dessen Hashwert
    assert hashlib.sha256(list_2[1]).hexdigest() == '4339f2e6eb5baad82a2870014841cecb16c6eb3e6227805067f34abc412d96e6'

cert = x509.load_der_x509_certificate(list_2[1], default_backend())
public_key = cert.public_key()

try:
    public_key.verify(list_2[-1], list_2[0], ec.ECDSA(hashes.SHA256()))
except InvalidSignature:
    sys.exit("Signatur ungültig!")

list_data = cbor.loads(list_2[0])
assert list_data["version"] == 1

print("Vertrauenslist am", list_data["datetime"], "erstellt und enthält",
      len(list_data["certs"]), "Zertifikate.")

for cert_der in list_data["certs"]:
    cert =  x509.load_der_x509_certificate(cert_der, default_backend())
    print("Zertifikat", hashlib.sha256(cert_der).hexdigest(), 
            ', '.join(x for x in [y.value for y in cert.subject]))

