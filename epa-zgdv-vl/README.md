# ePA ZGdV Vertrauensliste

Ab ePA 2.0 ist es notwendig, dass FdV auch AS ansprechen, die von
einem anderer Hersteller als das FdV sind. 

FdV verwenden in einem solchen Fall die Daten aus dieser Vertrauensliste für
die Prüfung von "Internet"-TLS-Zertifikaten fremder ZGdV. In der Liste sind
alle Internet-Zertifikate der AS aufgeführt.

Die Daten dieser Liste werden davon Aktensystem selbst in eine eigene Liste 
überführt und selbst signiert. Die FdV vertrauen nur diesem jeweiligen AS-Signer. 
Die Hersteller sind frei die AS-Liste und deren Signatur selbst zu gestalten.

## Testlauf

	$ ls certs/*.der
	certs/aok.pem.der         certs/bitmarck-2.pem.der  certs/bitmarck-4.pem.der  certs/ibm.pem.der
	certs/bitmarck-1.pem.der  certs/bitmarck-3.pem.der  certs/ibm-ecc.pem.der

	$ ./gen-n-sign-list.py

	$ ll liste.cbor 
	-rw-rw-r-- 1 a a 18779 Jun 16 21:10 liste.cbor

	$ ./verify_list.py
	Vertrauenslist am 2021-06-16T21:30:06.238023 erstellt und enthält 7 Zertifikate.
	Zertifikat d052c740e4c65601b963566dcaebe0bcb8d82343a857d77c2141a084084b8f86 Government Entity, DE, Government Entity, DE, BERLIN, AOK Bundesverband, epa.aokmeinleben.de
	Zertifikat c8bddd3adb4b83fdcfbcd164acb9892df3389d0c4bf037e316eea314657aeb91 Private Organization, HRB 98549, DE, Hamburg, Hamburg, DE, Hamburg, Hamburg, Hammerbrookstr. 38, BITMARCK Technik GmbH, akte-pu.deine-epa.de
	Zertifikat 71a358329d7ad714af26bfb63bc388d5f3dbcfa293f3f5fa9efb51213a2f30ca Private Organization, HRB 98549, DE, Hamburg, Hamburg, DE, Hamburg, Hamburg, Hammerbrookstr. 38, BITMARCK Technik GmbH, akte-pu.deine-epa.de
	Zertifikat ea8d53057c7a01c7786e739f6d83a569b27e00dc784e465124e8e554f1111db4 Private Organization, HRB 98549, DE, Hamburg, Hamburg, DE, Hamburg, Hamburg, Hammerbrookstr. 38, BITMARCK Technik GmbH, akte-pu.deine-epa.de
	Zertifikat 0a85229c41b9263e4002c1e41fcf1b6baab61153633498fed230acaba702de40 Private Organization, DE, Baden-Württemberg, Stuttgart, HRB 14562, DE, Baden-Württemberg, Ehningen, IBM Deutschland GmbH, ibmepa.de
	Zertifikat 5213e99191bd84f041c99dc7d236e2437c52d1fb18ff7da9edd878e62c2b1330 Private Organization, HRB 98549, DE, Hamburg, Hamburg, DE, Hamburg, Hamburg, Hammerbrookstr. 38, BITMARCK Technik GmbH, akte-pu.deine-epa.de
	Zertifikat 64a5eaf13ed8ef42404afca4ebe6f8a214a2ab0f02d35a5889ae593baf844432 Private Organization, DE, Baden-Württemberg, Stuttgart, HRB 14562, DE, Baden-Württemberg, Ehningen, IBM Deutschland GmbH, ibmepa.de

