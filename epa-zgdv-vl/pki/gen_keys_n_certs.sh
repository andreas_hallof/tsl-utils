#! /usr/bin/env bash

openssl ecparam -name brainpoolP256r1 -genkey -out signing_key.pem

openssl req -x509 -key signing_key.pem \
    -outform der \
    -out signer_cert.der -days 365 \
    -subj "/C=DE/ST=Berlin/L=Berlin/O=gematik/OU=gematik/CN=ePA-ZGdV-VL-Signer"

