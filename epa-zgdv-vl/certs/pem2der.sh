#! /bin/bash

for i in *.pem; do
    openssl x509 -inform pem -outform der -in $i -out $i.der
done

