# aktuell KOMP-CAs

    Script started on 2024-11-10 20:40:58+01:00 [TERM="tmux-256color" TTY="/dev/pty3" COLUMNS="158" LINES="47"]

    $ for i in *.der ; do echo $i; openssl x509 -text -noout -inform der -in $i; done
    1.der
    Certificate:
        Data:
            Version: 3 (0x2)
            Serial Number: 9 (0x9)
            Signature Algorithm: sha256WithRSAEncryption
            Issuer: C = DE, O = gematik GmbH, OU = Zentrale Root-CA der Telematikinfrastruktur, CN = GEM.RCA2
            Validity
                Not Before: Mar 16 12:23:07 2018 GMT
                Not After : Mar 14 12:23:06 2026 GMT
            Subject: C = DE, O = gematik GmbH, OU = Komponenten-CA der Telematikinfrastruktur, CN = GEM.KOMP-CA3
            Subject Public Key Info:
                Public Key Algorithm: rsaEncryption
                    Public-Key: (2048 bit)
                    Modulus:
                        00:a8:19:c3:d4:27:a9:cc:7a:1a:bd:a3:d9:5c:f9:
                        8e:33:64:49:cb:ef:84:3d:c6:c5:d2:33:23:38:87:
                        82:91:01:11:06:b6:9a:64:62:64:3e:eb:8c:77:7e:
                        2d:de:ed:24:ba:a4:f9:0c:29:8b:0d:64:6c:5e:2f:
                        a5:b0:92:66:15:b8:e7:49:09:c3:fa:c5:23:84:70:
                        84:d1:31:34:00:28:6e:09:d2:2f:1b:fa:a9:4c:18:
                        7d:f6:f7:0c:ed:a3:74:96:41:5c:8b:d0:37:f6:d9:
                        49:dd:a6:7a:97:9c:cd:9e:0a:7d:b7:de:76:89:c2:
                        93:cf:58:72:26:1d:ee:f7:c6:12:fd:01:66:f3:b7:
                        84:a2:a3:de:52:5f:8f:20:7f:d3:03:ae:5b:e7:69:
                        c2:bd:c1:f7:21:a1:60:0e:e3:63:46:17:f3:16:51:
                        da:eb:96:5a:1e:8c:0f:ff:8c:c7:1a:e1:ac:29:8e:
                        e6:60:b3:e7:6d:6f:15:e1:2c:3d:d4:e9:8a:9b:9b:
                        a9:3a:f1:b9:b8:ab:69:19:2e:5b:ed:45:61:93:b2:
                        ed:91:c1:04:81:fb:11:a1:fe:13:a0:f5:f5:01:8b:
                        b1:bd:6c:e9:91:64:a7:96:68:10:16:62:47:68:2b:
                        0c:52:98:7f:cc:98:03:c5:44:96:7a:f2:26:6e:c7:
                        61:a1
                    Exponent: 65537 (0x10001)
            X509v3 extensions:
                X509v3 Subject Key Identifier: 
                    FD:B5:B9:17:79:BD:B8:6B:1E:29:57:22:3D:C2:B9:B3:89:81:CF:4D
                X509v3 Authority Key Identifier: 
                    EC:5C:18:E0:13:B4:43:6C:09:8C:DF:FA:3C:3C:5B:7E:4B:70:84:46
                Authority Information Access: 
                    OCSP - URI:http://ocsp.root-ca.ti-dienste.de/ocsp
                X509v3 Basic Constraints: critical
                    CA:TRUE, pathlen:0
                X509v3 Key Usage: critical
                    Certificate Sign, CRL Sign
                X509v3 Certificate Policies: 
                    Policy: 1.2.276.0.76.4.163
        Signature Algorithm: sha256WithRSAEncryption
        Signature Value:
            47:98:b3:25:03:a5:19:31:fa:43:9d:2f:61:ea:a4:43:c8:62:
            6e:ac:e5:a8:ee:24:b6:91:29:7d:6c:c5:aa:72:c8:ef:c4:8d:
            1a:11:ee:dd:b2:b7:77:a0:90:48:a0:d8:7f:d8:98:15:44:c4:
            b4:bb:0c:b7:8f:5b:56:fd:64:2d:2e:77:5c:b5:53:cc:fd:50:
            6e:8e:6d:e3:ba:d3:23:a3:5d:88:ea:43:14:0c:9d:10:4f:1f:
            ca:ad:0c:61:f3:f1:e8:5a:e8:ca:13:57:45:9c:ab:17:db:ee:
            65:fc:d9:72:39:80:8e:8f:31:c3:d8:1a:5a:16:a9:a4:b2:9b:
            ba:2e:8a:67:5e:cb:ee:d1:ba:a9:a2:9e:d4:9a:d4:d8:3e:ec:
            05:3e:dc:cb:68:7c:a9:cb:b3:96:9c:af:1d:66:62:a3:33:84:
            02:07:87:55:99:da:8d:53:ad:8b:0a:a4:8f:36:b3:6a:ea:24:
            d1:12:0c:ed:07:71:e2:bf:d0:e6:72:a2:f1:52:f6:7a:3c:8f:
            45:ac:8e:34:6a:cd:ea:66:a4:a1:7d:74:5d:f7:8b:a6:d5:25:
            c8:42:75:ea:3f:94:8b:02:b7:26:d2:f9:55:55:f4:29:1f:5d:
            2f:d6:26:ea:99:ee:42:b0:00:75:a3:2a:c9:b3:63:2a:9c:cf:
            9b:93:4c:2e



    2.der
    Certificate:
        Data:
            Version: 3 (0x2)
            Serial Number: 22 (0x16)
            Signature Algorithm: sha256WithRSAEncryption
            Issuer: C = DE, O = gematik GmbH, OU = Zentrale Root-CA der Telematikinfrastruktur, CN = GEM.RCA2
            Validity
                Not Before: Mar  3 10:43:06 2021 GMT
                Not After : Dec  7 08:41:56 2026 GMT
            Subject: C = DE, O = gematik GmbH, OU = Komponenten-CA der Telematikinfrastruktur, CN = GEM.KOMP-CA5
            Subject Public Key Info:
                Public Key Algorithm: rsaEncryption
                    Public-Key: (2048 bit)
                    Modulus:
                        00:ae:9c:e8:98:2c:01:b6:a9:0e:b8:85:18:7f:e9:
                        cb:16:a7:89:75:5a:a7:c8:04:f9:7b:10:5e:76:bf:
                        9e:b4:45:76:01:fb:12:00:d6:26:8c:52:af:55:57:
                        ba:be:ed:b6:0f:d6:06:43:c2:bd:ef:67:25:fe:5c:
                        be:8a:e6:37:b5:88:be:12:97:51:af:5c:8d:3d:00:
                        6a:20:4a:22:9e:ca:6d:4d:99:70:b7:e4:03:36:73:
                        51:f4:3f:35:06:6a:d5:6a:88:01:7c:4a:71:1f:f0:
                        88:07:e4:f4:0f:fb:2b:9d:95:e0:f3:df:ee:56:a9:
                        8f:80:6a:b3:76:60:b1:b8:8a:71:a0:59:52:90:f2:
                        42:40:f8:bb:0b:de:88:6d:72:74:1b:90:90:2f:28:
                        a6:43:48:95:f1:ff:89:61:ff:fb:79:d1:35:bb:a4:
                        ba:1d:44:37:70:8d:dd:0d:ec:85:b3:e7:16:d9:38:
                        57:8f:bf:c2:21:c7:9c:45:a4:9e:20:5e:24:2a:ec:
                        ee:0b:04:8b:27:9a:27:9b:e5:11:a3:60:61:d8:72:
                        d2:e4:d9:dc:9f:12:b4:61:94:1e:3a:b5:3e:e9:eb:
                        9f:7b:a0:b7:f4:be:57:5d:c7:5f:be:08:42:17:59:
                        6b:9d:59:c6:28:fa:65:9b:26:53:eb:b8:6d:c6:70:
                        71:0f
                    Exponent: 65537 (0x10001)
            X509v3 extensions:
                X509v3 Subject Key Identifier: 
                    95:54:9B:3F:BD:C4:3C:B4:78:C8:91:8C:FD:68:57:8A:64:A4:30:97
                X509v3 Authority Key Identifier: 
                    EC:5C:18:E0:13:B4:43:6C:09:8C:DF:FA:3C:3C:5B:7E:4B:70:84:46
                Authority Information Access: 
                    OCSP - URI:http://ocsp.root-ca.ti-dienste.de/ocsp
                X509v3 Basic Constraints: critical
                    CA:TRUE, pathlen:0
                X509v3 Key Usage: critical
                    Certificate Sign, CRL Sign
                X509v3 Certificate Policies: 
                    Policy: 1.2.276.0.76.4.163
        Signature Algorithm: sha256WithRSAEncryption
        Signature Value:
            7b:e2:70:a6:2c:0e:fb:cb:41:c3:91:0c:12:f4:06:71:ea:9b:
            91:ef:6c:e4:7c:d2:49:32:69:5d:86:95:3b:61:92:22:91:6d:
            df:b9:3d:68:3a:32:bc:66:ed:b7:16:35:a5:dc:e7:ad:69:1f:
            c3:db:cf:29:a3:2e:63:43:50:55:82:7e:10:4d:84:b3:bd:ea:
            ed:9e:87:db:fa:63:36:47:5d:ec:3c:f4:4d:24:f0:d0:15:7e:
            94:77:f9:b4:7b:ad:4f:43:33:03:bd:8b:9c:9b:16:db:a9:22:
            fe:3f:ab:1e:ec:72:dd:71:f0:eb:0a:4a:0b:a7:10:b4:d4:ae:
            66:eb:26:bc:bc:8b:a3:55:d4:56:ec:f1:9f:25:f8:15:a4:55:
            ee:70:97:96:22:bb:ce:9f:30:bd:08:98:ef:27:20:d6:a6:29:
            6a:64:57:b1:5a:de:55:ea:9f:66:a0:a3:df:29:ee:30:01:5a:
            d0:19:0f:8b:f8:44:3d:31:c9:bc:9f:a0:dd:cf:5a:fc:55:f8:
            be:b2:7f:c3:24:40:8b:a2:94:fd:88:af:d1:15:51:10:5d:7a:
            db:21:a5:bb:82:79:03:a0:55:4c:97:ca:b2:22:9a:2d:e3:46:
            41:27:86:f5:b8:fb:00:5c:91:9e:c8:2e:4c:f1:bb:f4:6a:a3:
            8e:b1:55:05



    3.der
    Certificate:
        Data:
            Version: 3 (0x2)
            Serial Number: 4 (0x4)
            Signature Algorithm: ecdsa-with-SHA256
            Issuer: C = DE, O = gematik GmbH, OU = Zentrale Root-CA der Telematikinfrastruktur, CN = GEM.RCA3
            Validity
                Not Before: Sep 13 09:26:32 2018 GMT
                Not After : Sep 11 09:26:31 2026 GMT
            Subject: C = DE, O = gematik GmbH, OU = Komponenten-CA der Telematikinfrastruktur, CN = GEM.KOMP-CA4
            Subject Public Key Info:
                Public Key Algorithm: id-ecPublicKey
                    Public-Key: (256 bit)
                    pub:
                        04:30:7a:cd:92:bf:24:65:ce:c2:7a:43:d4:28:47:
                        c5:76:bf:28:94:c2:98:0a:37:ff:b3:06:33:9e:49:
                        e4:8b:31:74:58:8a:a4:fe:42:98:00:c4:ff:9d:71:
                        67:83:2f:af:6a:0f:a4:d6:e0:86:25:eb:70:0b:68:
                        ce:55:f2:d2:e4
                    ASN1 OID: brainpoolP256r1
            X509v3 extensions:
                X509v3 Subject Key Identifier: 
                    02:55:E2:DB:21:CF:B4:2B:1D:AD:AD:D8:1E:FC:44:61:FE:E3:B5:B0
                X509v3 Authority Key Identifier: 
                    DF:C3:BE:75:BE:25:EB:13:32:30:2E:A7:50:85:08:9F:37:10:D0:E8
                Authority Information Access: 
                    OCSP - URI:http://ocsp.root-ca.ti-dienste.de/ocsp
                X509v3 Basic Constraints: critical
                    CA:TRUE, pathlen:0
                X509v3 Key Usage: critical
                    Certificate Sign, CRL Sign
                X509v3 Certificate Policies: 
                    Policy: 1.2.276.0.76.4.163
        Signature Algorithm: ecdsa-with-SHA256
        Signature Value:
            30:44:02:20:3c:2c:5f:a3:35:17:11:05:82:ac:a1:69:3c:1f:
            72:e7:ba:51:6f:f3:72:1b:1f:a7:cf:27:de:64:32:9c:a6:96:
            02:20:5a:ec:f0:a0:aa:69:54:1a:4f:a4:90:2d:2a:88:e9:07:
            a9:33:40:86:39:08:e1:77:53:68:cb:dd:78:14:78:46



    4.der
    Certificate:
        Data:
            Version: 3 (0x2)
            Serial Number: 15 (0xf)
            Signature Algorithm: ecdsa-with-SHA256
            Issuer: C = DE, O = gematik GmbH, OU = Zentrale Root-CA der Telematikinfrastruktur, CN = GEM.RCA4
            Validity
                Not Before: Jul 22 11:02:54 2021 GMT
                Not After : Jul 20 11:02:53 2029 GMT
            Subject: C = DE, O = gematik GmbH, OU = Komponenten-CA der Telematikinfrastruktur, CN = GEM.KOMP-CA6
            Subject Public Key Info:
                Public Key Algorithm: id-ecPublicKey
                    Public-Key: (256 bit)
                    pub:
                        04:53:4b:2a:c1:e9:61:b7:6f:43:41:f5:0e:d2:35:
                        a4:9b:7d:f0:0b:f4:98:bb:a2:21:6e:68:45:53:dd:
                        a5:f8:a2:46:18:43:a6:7e:08:7e:d9:20:fb:15:78:
                        c2:ff:61:48:f2:3b:03:e1:00:95:f0:e2:20:f0:cc:
                        fc:0a:ad:33:79
                    ASN1 OID: brainpoolP256r1
            X509v3 extensions:
                X509v3 Subject Key Identifier: 
                    30:D2:E1:36:AF:6C:57:F2:B1:83:95:DA:B6:38:A6:7B:BA:31:74:CA
                X509v3 Authority Key Identifier: 
                    80:61:70:19:1C:38:ED:E2:6B:E4:A1:2E:AB:22:3E:75:C9:94:FE:7D
                Authority Information Access: 
                    OCSP - URI:http://ocsp.root-ca.ti-dienste.de/ocsp
                X509v3 Key Usage: critical
                    Certificate Sign, CRL Sign
                X509v3 Certificate Policies: 
                    Policy: 1.2.276.0.76.4.163
                      CPS: http://www.gematik.de/go/policies
                X509v3 Basic Constraints: critical
                    CA:TRUE, pathlen:0
        Signature Algorithm: ecdsa-with-SHA256
        Signature Value:
            30:46:02:21:00:a0:f3:12:29:a2:d2:ee:13:2d:f3:a6:46:b9:
            56:55:9b:87:8f:db:e5:b3:56:bf:c2:8b:b9:72:71:49:89:b5:
            42:02:21:00:9b:57:b1:a6:bd:8b:f5:ec:79:f5:4f:22:55:e0:
            dc:14:88:3c:84:9e:72:ed:e1:fe:86:26:0d:c6:7b:66:8b:ad



    5.der
    Certificate:
        Data:
            Version: 3 (0x2)
            Serial Number: 15 (0xf)
            Signature Algorithm: sha256WithRSAEncryption
            Issuer: C = DE, O = gematik GmbH, OU = Zentrale Root-CA der Telematikinfrastruktur, CN = GEM.RCA6
            Validity
                Not Before: Dec  2 08:35:25 2021 GMT
                Not After : Nov 30 08:35:24 2029 GMT
            Subject: C = DE, O = gematik GmbH, OU = Komponenten-CA der Telematikinfrastruktur, CN = GEM.KOMP-CA7
            Subject Public Key Info:
                Public Key Algorithm: rsaEncryption
                    Public-Key: (2048 bit)
                    Modulus:
                        00:fa:48:71:12:12:d3:ff:9d:b7:8a:8d:f6:55:e2:
                        9e:db:c4:96:dc:bc:1a:b1:c2:75:5a:fb:90:e4:9c:
                        72:98:02:e6:b2:85:a1:06:78:c3:37:b1:c4:4b:a4:
                        c2:e1:97:09:ca:0b:4c:7d:b8:a6:62:ba:e0:d0:c6:
                        b8:df:5a:e9:48:06:7e:3d:20:15:f6:2e:9e:2b:10:
                        5f:0b:ed:83:ec:84:a1:c0:72:c0:97:d0:21:89:30:
                        ed:c7:19:f3:40:1e:dd:3f:43:7d:77:20:d0:7c:17:
                        a9:ab:28:f0:f5:3a:b9:92:19:db:44:f7:a5:f3:74:
                        51:64:a7:fa:b2:77:6e:26:69:8b:fc:57:ae:e0:76:
                        f9:7b:ad:bd:7b:1c:3a:cc:87:5b:c7:66:1c:b3:4c:
                        b5:0b:9b:4d:73:ea:c5:e5:8b:c0:91:4b:d2:ce:4e:
                        06:39:45:84:b0:65:d1:d6:87:48:f2:32:33:91:19:
                        66:03:8c:f4:fb:01:00:7f:ca:74:bd:04:85:dc:18:
                        27:6e:5d:2d:cd:2c:23:c0:80:c5:14:1e:9a:e1:8a:
                        14:3c:20:99:5f:d2:ce:b8:18:25:7b:a2:d2:e0:81:
                        3d:27:bf:aa:a4:d7:2d:ef:e2:c7:81:9e:f2:9f:6c:
                        73:0b:62:7d:c2:48:30:06:f4:f1:71:42:05:ae:5d:
                        87:57
                    Exponent: 65537 (0x10001)
            X509v3 extensions:
                X509v3 Subject Key Identifier: 
                    04:B3:F4:16:30:45:3B:7B:91:4A:F3:7C:F0:89:E5:09:4E:92:51:94
                X509v3 Authority Key Identifier: 
                    18:44:C4:DA:66:93:3A:EF:4F:3B:2A:09:65:E4:FE:28:90:1E:55:11
                Authority Information Access: 
                    OCSP - URI:http://ocsp.root-ca.ti-dienste.de/ocsp
                X509v3 Key Usage: critical
                    Certificate Sign, CRL Sign
                X509v3 Certificate Policies: 
                    Policy: 1.2.276.0.76.4.163
                      CPS: http://www.gematik.de/go/policies
                X509v3 Basic Constraints: critical
                    CA:TRUE, pathlen:0
        Signature Algorithm: sha256WithRSAEncryption
        Signature Value:
            2d:89:a0:e4:ad:51:9c:78:c4:bb:02:63:94:87:fb:98:a3:36:
            77:59:ed:a8:a1:83:8c:f2:d3:0a:0c:58:5e:7c:79:68:f0:4a:
            0d:4f:8c:c6:c9:58:a9:1b:bb:26:25:8f:95:1a:0c:1e:08:7a:
            66:fe:29:a0:de:b8:f1:26:48:19:74:f9:64:95:1c:c9:73:b7:
            97:dc:d2:4c:fb:9c:91:5c:42:ef:5e:2a:e7:00:78:7b:e4:37:
            69:7f:a7:a4:11:70:e9:70:bc:12:31:96:14:91:7b:eb:a4:ce:
            62:b8:27:5f:24:5a:41:83:ea:70:d1:cc:de:73:ab:a4:9c:dd:
            09:ab:d3:55:09:04:ee:ac:eb:e1:8b:47:0f:63:d1:97:5d:38:
            8b:0b:79:1e:91:65:ff:1e:f7:88:e9:51:04:a1:61:2d:77:2d:
            21:38:0b:bd:ae:a3:ec:fc:56:ae:8b:37:b7:80:a0:c7:cc:c0:
            0f:1c:cd:0b:7f:7e:be:98:3a:14:2a:4d:27:af:d2:76:41:e8:
            b4:81:08:bc:a9:1a:6c:59:b7:65:a0:da:fc:55:1a:dd:e3:6f:
            8c:3b:67:c3:73:08:41:06:23:f4:c7:2a:a9:7e:e5:ae:00:58:
            c5:e5:08:e3:43:aa:b7:75:ad:f1:6e:ea:cd:3f:31:2e:08:7f:
            41:57:f6:58



    6.der
    Certificate:
        Data:
            Version: 3 (0x2)
            Serial Number: 5 (0x5)
            Signature Algorithm: ecdsa-with-SHA256
            Issuer: C = DE, O = gematik GmbH, OU = Zentrale Root-CA der Telematikinfrastruktur, CN = GEM.RCA7
            Validity
                Not Before: Jul 18 11:37:29 2023 GMT
                Not After : Jul 16 11:37:28 2031 GMT
            Subject: C = DE, O = gematik GmbH, OU = Komponenten-CA der Telematikinfrastruktur, CN = GEM.KOMP-CA8
            Subject Public Key Info:
                Public Key Algorithm: id-ecPublicKey
                    Public-Key: (256 bit)
                    pub:
                        04:3e:87:0d:ff:ea:43:0d:f2:b5:79:62:c2:6e:00:
                        cc:53:d2:73:d3:a5:e0:42:83:c1:42:e6:2e:6f:34:
                        64:77:25:23:64:f7:61:89:b8:42:4b:4d:c3:5f:95:
                        4d:48:f0:01:77:fe:25:84:f9:ae:87:69:8a:0a:d4:
                        48:4a:40:02:df
                    ASN1 OID: prime256v1
                    NIST CURVE: P-256
            X509v3 extensions:
                X509v3 Subject Key Identifier: 
                    F8:6E:E0:C9:82:7C:BA:84:77:0F:06:C3:5A:A1:FC:54:29:89:FF:40
                X509v3 Authority Key Identifier: 
                    13:10:78:1E:88:77:DB:8D:E5:AB:E8:2D:00:E0:32:F8:ED:F5:21:55
                Authority Information Access: 
                    OCSP - URI:http://ocsp.root-ca.ti-dienste.de/ocsp
                X509v3 Key Usage: critical
                    Certificate Sign, CRL Sign
                X509v3 Certificate Policies: 
                    Policy: 1.2.276.0.76.4.163
                      CPS: http://www.gematik.de/go/policies
                X509v3 Basic Constraints: critical
                    CA:TRUE, pathlen:0
        Signature Algorithm: ecdsa-with-SHA256
        Signature Value:
            30:44:02:20:21:c9:30:5b:03:e8:26:32:3a:2f:20:1c:f5:5b:
            30:ac:25:a0:e2:35:22:82:78:fe:b3:65:88:c6:3e:af:67:08:
            02:20:25:03:1e:b0:7f:b1:b5:52:b0:8b:07:ae:43:ae:23:79:
            08:0d:7f:16:b8:f8:36:83:c5:e8:58:35:b3:01:14:65



    7.der
    Certificate:
        Data:
            Version: 3 (0x2)
            Serial Number: 14 (0xe)
            Signature Algorithm: ecdsa-with-SHA256
            Issuer: C = DE, O = gematik GmbH, OU = Zentrale Root-CA der Telematikinfrastruktur, CN = GEM.RCA8
            Validity
                Not Before: Jul  3 06:47:25 2024 GMT
                Not After : Jul  1 06:47:24 2032 GMT
            Subject: C = DE, O = gematik GmbH, OU = Komponenten-CA der Telematikinfrastruktur, CN = GEM.KOMP-CA9
            Subject Public Key Info:
                Public Key Algorithm: id-ecPublicKey
                    Public-Key: (256 bit)
                    pub:
                        04:07:d9:ec:78:62:5c:a8:94:e5:eb:b8:62:14:2b:
                        30:78:66:0c:8d:e6:20:f1:c0:4a:84:79:b9:2f:81:
                        47:25:d0:97:2b:b3:b5:e3:a1:95:0f:2b:7b:db:7a:
                        3e:89:6a:ab:46:24:69:55:f5:4c:20:93:16:39:83:
                        c9:97:be:94:34
                    ASN1 OID: brainpoolP256r1
            X509v3 extensions:
                X509v3 Subject Key Identifier: 
                    89:00:07:E7:DF:25:01:1F:E7:F2:EC:A3:F7:5F:38:F9:B0:67:19:7A
                X509v3 Authority Key Identifier: 
                    8B:9C:0C:D0:7A:BE:00:68:7A:AD:32:8D:69:8F:99:80:72:C7:9B:2C
                Authority Information Access: 
                    OCSP - URI:http://ocsp.root-ca.ti-dienste.de/ocsp
                X509v3 Key Usage: critical
                    Certificate Sign, CRL Sign
                X509v3 Certificate Policies: 
                    Policy: 1.2.276.0.76.4.163
                      CPS: http://www.gematik.de/go/policies
                X509v3 Basic Constraints: critical
                    CA:TRUE, pathlen:0
        Signature Algorithm: ecdsa-with-SHA256
        Signature Value:
            30:44:02:20:76:29:c1:83:29:06:e3:44:a6:5e:56:08:16:ea:
            24:e9:5c:9c:84:2b:94:9e:f5:7f:db:b8:fe:b1:4f:ee:3a:86:
            02:20:38:bf:fc:84:cb:16:71:83:d2:95:eb:ab:9d:93:38:0b:
            23:a0:e6:22:f1:7e:27:37:18:f7:88:7f:d4:39:3f:7d

