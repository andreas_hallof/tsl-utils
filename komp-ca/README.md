# aktuell KOMP-CAs aus der TSL extrahieren

    $ date
    Sun Nov 10 20:35:46 CET 2024

    $ ./Extract-TSP.py ECC-RSA_TSL.xml oid_fd_tls_s
    TSL id ID31027920241028000006Z
    TSL erzeugt am 2024-10-28T00:00:06Z
    nächstes Update 2024-11-27T00:00:06Z
    In der TSL sind 9 TSPs mit zusammen 263 TSPServices enthalten. Es wurden 7 Suchziele gefunden und extrahiert.

    $ ls -l *.der
    -rw-r--r-- 1 Andreas.Hallof int 1054 Nov 10 20:18 1.der
    -rw-r--r-- 1 Andreas.Hallof int 1054 Nov 10 20:18 2.der
    -rw-r--r-- 1 Andreas.Hallof int  658 Nov 10 20:18 3.der
    -rw-r--r-- 1 Andreas.Hallof int  709 Nov 10 20:18 4.der
    -rw-r--r-- 1 Andreas.Hallof int 1103 Nov 10 20:18 5.der
    -rw-r--r-- 1 Andreas.Hallof int  706 Nov 10 20:18 6.der
    -rw-r--r-- 1 Andreas.Hallof int  707 Nov 10 20:18 7.der

