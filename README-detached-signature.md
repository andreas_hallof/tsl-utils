# TSL und die detached Signatur

Nach Absprache zwischen gematik und BSI soll die Prüfung der TSL bei 
Download der TSL aus dem Internet i. d. R. nicht über die XMLDSig-Signatur,
die sich innerhalb der XML-Daten der TSL befindet, erfolgen, sondern über die
[detached Signatur](https://bitbucket.org/andreas_hallof/tsl-utils/src/master/detached-signature/).


## TU

 [ECC-RSA\_TSL-test.xml](http://download-testref.crl.ti-dienste.de/TSL-ECC-test/ECC-RSA_TSL-test.xml)
und
 [ECC-RSA\_TSL-test.sig](http://download-testref.crl.ti-dienste.de/TSL-ECC-test/ECC-RSA_TSL-test.sig)


	$ ./Chk-Detached-Signature-TSL.py --TU ECC-RSA_TSL.xml
	Signierendes Zertifikat ("DE, gematik GmbH NOT-VALID, TSL Signing Unit 10 TEST-ONLY", SN: 2, gültig bis 2023-04-08T09:58) ist Teil der TI-PKI.
	kryptographische Verifikation der TSL-Daten: OK
	TSL id ID31033820211202175549Z
	TSL erzeugt am 2021-12-02T17:55:49Z
	nächstes Update 2022-01-01T17:55:49Z
	$ echo $?
	0

Man beachte `--TU`

## PU


	$ ./Download-TSL.sh
	2021-12-11 21:59:56 URL:http://download.crl.ti-dienste.de/TSL-ECC/ECC-RSA_TSL.xml [441696/441696] -> "ECC-RSA_TSL.xml" [1]
	2021-12-11 21:59:56 URL:http://download.crl.ti-dienste.de/TSL-ECC/ECC-RSA_TSL.sig [713/713] -> "ECC-RSA_TSL.sig" [1]

Ohne explizite Angaben von PU (`--PU`) oder TU (`--TU`), ist PU der default.

	$ ./Chk-Detached-Signature-TSL.py ECC-RSA_TSL.xml
	Signierendes Zertifikat ("DE, gematik GmbH, TSL Signing Unit 4", SN: 2, gültig bis 2025-05-26T08:08) ist Teil der TI-PKI.
	kryptographische Verifikation der TSL-Daten: OK
	TSL id ID31018820211202153655Z
	TSL erzeugt am 2021-12-02T15:36:55Z
	nächstes Update 2022-01-01T15:36:55Z

	$ ./Chk-Detached-Signature-TSL.py --PU ECC-RSA_TSL.xml
	Signierendes Zertifikat ("DE, gematik GmbH, TSL Signing Unit 4", SN: 2, gültig bis 2025-05-26T08:08) ist Teil der TI-PKI.
	kryptographische Verifikation der TSL-Daten: OK
	TSL id ID31018820211202153655Z
	TSL erzeugt am 2021-12-02T15:36:55Z
	nächstes Update 2022-01-01T15:36:55Z

	$ echo $?
	0

Hinweis: schlägt die Signaturprüfung fehl ist der Exit-Code des Programms != 0.

