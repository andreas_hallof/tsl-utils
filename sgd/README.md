# Tools für die SGDCertificateImportList-XML-Dateien

## SGDCertificateImportList-XML-Datei erstellen aus einer TSL der TI

Ich lade mir die die aktuelle TSL der TI (PU) vom Internet-Downloadpunkt:

    [a@h tsl-utils]$ ./Download-TSL.sh
    CA-Zertifikat »/etc/ssl/certs/ca-certificates.crt« wurde geladen
    2020-08-09 22:11:53 URL:https://download.tsl.ti-dienste.de/TSL.xml [233417/233417] -> "TSL.xml" [1]

    [a@h tsl-utils]$ sha256sum TSL.xml 
    95d3b452e619762ffbda39c80472b396a75f570b670c9610d45cf22f1eb1fe70  TSL.xml

Signatur prüfen:

    [a@h tsl-utils]$ ./Chk-Signature-TSL.py TSL.xml
    Signierendes Zertifikat ("DE, gematik GmbH, TSL Signing Unit 3", SN: 5, gültig bis 2023-02-17T10:18) ist Teil der TI-PKI.
    kryptographische Verifikation der TSL-Daten: OK
    TSL id ID335820200731120341Z
    TSL erzeugt am 2020-07-31T12:03:41Z
    nächstes Update 2020-08-30T12:03:41Z

Aus der TSL und den 4 Root-CA-Zertifikaten (PU) aus dem Verzeichnis SGD2
erzeuge ich eine SGDCertificateImportList-XML-Datei:

    [a@h tsl-utils]$ cd sgd/
    [a@h sgd]$ sha256sum GEM.RCA?.der
    c2eef6cb3a7829db5c595fef25470a5b5e142fe14827259cc201136f1c032bee  GEM.RCA1.der
    848fda162c607b492c62f625840e6451285c40c7334ec8dd659d093236ebc9ec  GEM.RCA2.der
    9a7e2f1ea6449cb39c241f063b5845d3e0789dc28e0286d3c6e64505f95c8457  GEM.RCA3.der
    a9a6d014b5adffaede619afd7c94f2ad4eaa403ddfc63d04907b80a51a4f8b39  GEM.RCA4.der
    [a@h sgd]$ ./gen_import_list.py ../TSL.xml >my_key_data.xml
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:135} INFO - TSL-id: ID335820200731120341Z
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:140} INFO - TSL erzeugt am 2020-07-31T12:03:41Z
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:144} INFO - nächstes Update 2020-08-30T12:03:41Z
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:156} INFO - ---------- medisign GmbH
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:156} INFO - ---------- arvato Systems GmbH
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:156} INFO - ---------- Atos Information Technology GmbH
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:156} INFO - ---------- T-Systems International GmbH
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:156} INFO - ---------- D-Trust GmbH
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:156} INFO - ---------- kubus IT GbR
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:156} INFO - ---------- BITMARCK
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:156} INFO - ---------- Bundesnetzagentur
    [2020-08-09T22:16:00+0200] {./gen_import_list.py:250} INFO - In der TSL sind 8 TSPs mit zusammen 83 TSPServices enthalten. Es wurden 30 Suchziele gefunden und extrahiert.
    [a@h sgd]$ ls -l my_key_data.xml 
    -rw-r--r-- 1 a users 193520  9. Aug 22:16 my_key_data.xml
    [a@h sgd]$ head my_key_data.xml 
    <?xml version = "1.0" encoding = "ISO-8859-1" ?>
    <!-- Date: 2020-08-09T22:16:00.357896 File: ../TSL.xml -->

    <SGDCertificateImportList>

    <SGDCertificate>
        <CertName>C=DE, O=gematik GmbH, OU=Zentrale Root-CA der Telematikinfrastruktur, CN=GEM.RCA4</CertName>
        <CertType>Root</CertType>
            <CertID>47454d2e52434134</CertID>
            <CertValue>308202683082020fa003020102020101300a06082a8648ce3d040302306d310b300906035504061302444531153013060355040a0c0c67656d6174696b20476d624831343032060355040b0c2b5a656e7472616c6520526f6f742d4341206465722054656c656d6174696b696e667261737472756b7475723111300f06035504030c0847454d2e52434134301e170d3139313030323036333134315a170d3239303932393036333134315a306d310b300906035504061302444531153013060355040a0c0c67656d6174696b20476d624831343032060355040b0c2b5a656e7472616c6520526f6f742d4341206465722054656c656d6174696b696e667261737472756b7475723111300f06035504030c0847454d2e52434134305a301406072a8648ce3d020106092b24030302080101070342000474b6837db20a84f780ca730991267bf7313e3202bcbf63ced15bbc2d639d10974d64bba6bbcbc85bd5c734a2df5e99b07a1e580ded3b6235c9237179965eaa75a3819e30819b301d0603551d0e04160414806170191c38ede26be4a12eab223e75c994fe7d304206082b0601050507010104363034303206082b060105050730018626687474703a2f2f6f6373702e726f6f742d63612e74692d6469656e7374652e64652f6f637370300f0603551d130101ff040530030101ff300e0603551d0f0101ff04040302010630150603551d20040e300c300a06082a8214004c048123300a06082a8648ce3d040302034700304402203054b607f7e14326a189674bb24611468316f1b877545de1707fb9a0f27a5740022061b8c4ea0ae8a48c3ef69ec49a995f9c67207163dd98f34e8c5f2e3eccbf2cd8</CertValue

## Daten aus einer SGDCertificateImportList-XML-Datei anzeigen

Daten aus einer SGDCertificateImportList-XML-Datei anzeigen:

    [a@h sgd]$ ./chk_import_list.py my_key_data.xml >report.txt
    [a@h sgd]$ head -20 report.txt 
    Root "GEM.RCA4" 47454d2e52434134 2029-09-29T06:31 a9a6d014b5adffaede619afd7c94f2ad4eaa403ddfc63d04907b80a51a4f8b39
    Root "GEM.RCA1" 47454d2e52434131 2025-01-05T09:47 c2eef6cb3a7829db5c595fef25470a5b5e142fe14827259cc201136f1c032bee
    Root "GEM.RCA3" 47454d2e52434133 2027-10-15T07:13 9a7e2f1ea6449cb39c241f063b5845d3e0789dc28e0286d3c6e64505f95c8457
    Root "GEM.RCA2" 47454d2e52434132 2026-12-07T08:41 848fda162c607b492c62f625840e6451285c40c7334ec8dd659d093236ebc9ec
    OCSPSigner "Root-CA OCSP-Signer3" 526f6f742d4341204f4353502d5369676e657233 2023-09-18T10:30 bfd7d770f2b55659d6c7b226bf0a318ad7ffe9a2042b293fb0584eb4a6de54e7
    OCSPRelation 526f6f742d4341204f4353502d5369676e657233 47454d2e52434134
    OCSPRelation 526f6f742d4341204f4353502d5369676e657233 47454d2e52434131
    OCSPRelation 526f6f742d4341204f4353502d5369676e657233 47454d2e52434133
    OCSPRelation 526f6f742d4341204f4353502d5369676e657233 47454d2e52434132
    TrustedCA "MESIG.SMCB-CA1" 4d455349472e534d43422d434131 2026-06-10T07:32 97bc6a008ba5264566ec560e2c76a54966839b70b2c8146fb97d6906e71523be
        # 4d455349472e534d43422d434131 TIsigned(ec5c18e013b4436c098cdffa3c3c5b7e4b708446)
    OCSPSigner "MESIG.HBA-OCSP1" 4d455349472e4842412d4f43535031 2025-06-03T13:19 43488bed8f220d6318b359266761bb660655889cd2fb88d3f89432ccdc5ff042
    OCSPRelation 4d455349472e4842412d4f43535031 4d455349472e534d43422d434131
    OCSPSigner "MESIG.SMCB-OCSP1" 4d455349472e534d43422d4f43535031 2023-06-18T14:35 a34ce3b7a8b5468a13832056ae2ad7d8916689dc7734544abfedabdd61d646fb
    OCSPRelation 4d455349472e534d43422d4f43535031 4d455349472e534d43422d434131
    TrustedCA "ATOS.EGK-CA201" 41544f532e45474b2d4341323031 2025-02-09T13:58 4f1b7d3a82a73361e920bedac3393450d17bd5fa58ec0d893fa42479acc46f7a
        # 41544f532e45474b2d4341323031 nonTIsigned
    TrustedCA "ATOS.EGK-CA9" 41544f532e45474b2d434139 2026-10-02T12:17 fb444c862c2f339044c1aaec02bb2380f69e8aea7cfe5ae9c526a0d28b3411c9
        # 41544f532e45474b2d434139 nonTIsigned
    TrustedCA "ATOS.SMCB-CA1" 41544f532e534d43422d434131 2026-12-06T08:59 f2fb86e5ca461b69e8218d48764ad0fb88ff440380019f9c0f3ef2ca70e327cd

## Zwei SGDCertificateImportList-XML-Datei auf semantische Äquivalenz untersuchen

Annahme: ich habe zwei SGDCertificateImportList-XML-Dateien die bspw. eine
unterschiedliche White-Space-Formatierung besitzen oder die
SGDCertifcate-Einträge sind unterschiedlich sortiert.

Die semantische Äquivalenz der beiden Listen (im folgenden Beispiel liste1.xml
und liste2.xml) kann man wie folgt prüfen

    ./chk_import_list.py liste1.xml | sort  >report1.txt
    ./chk_import_list.py liste2.xml | sort  >report2.txt
    diff report1.txt report2.txt

    if [ $? -eq 0 ]; then echo "JA liste1.xml und liste2.xml sind äquivalent"; fi

