#! /usr/bin/env python3

import sys, os, argparse, glob, logging, hashlib, json
from binascii import hexlify, unhexlify
from xml.etree import ElementTree
from collections import defaultdict

from cryptography import x509
from cryptography.hazmat.backends import default_backend

from lxml import etree

a_p = argparse.ArgumentParser(description="Aus einer angegeben SGDCertificateImportList-XML-Datei zeige ich die zu importierenden Schlüssel an und führe einige Sanity-Checks durch.")
a_p.add_argument('-v', '--verbose', dest='verbose', action='store_true', help='verbose mode')
a_p.add_argument(dest='ImportFile', metavar='SGDCertificateImportList-XML-Datei')
args = a_p.parse_args()

logging.basicConfig(level=logging.DEBUG,
                    format='[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
                    datefmt="%Y-%m-%dT%H:%M:%S%z")

if not (os.path.exists(args.ImportFile) and os.path.isfile(args.ImportFile)):
    sys.exit('Datei "{}" nicht gefunden'.format(args.ImportFile))

config_file="config.json"
if not (os.path.exists(config_file) and os.path.isfile(config_file)):
    sys.exit('Datei "{}" nicht gefunden'.format(config_file))
with open(config_file, "rt") as f:
    config = json.load(f)

# erst mal kurz eine Schemaprüfung
xml_file = etree.parse(args.ImportFile)
xml_validator = etree.XMLSchema(file="SGDCertificateImportList.xsd")
is_valid = xml_validator.validate(xml_file)
if not is_valid:
    sys.exit(args.ImportFile + " erscheint nicht Schema-konform zu sein.")

# weiter gehts mit den TI-X.509-Root-CA-Zerifiakten
root_cert_files = glob.glob("GEM.RCA*.der")
root_ids = defaultdict()
root_CertIDs = defaultdict()
root_der_data = []

def parse_cert(cert: x509.Certificate):
    """
    Ermittelt Informationen aus dem übergebenen Zertifikat und gibt diese
    als dictionary zurück.
    """

    #CertName = ', '.join(x for x in [y.value for y in cert.subject])
    CertName = ', '.join(x for x in [y.rfc4514_string() for y in cert.subject])
    CertID = ""
    for i in cert.subject:
        if i.rfc4514_string().startswith("CN="):
            cn = i.rfc4514_string()[3:]
            CertID = hexlify(cn.encode()).decode()

    for ext in cert.extensions:
        if ext.oid == x509.oid.ExtensionOID.SUBJECT_KEY_IDENTIFIER:
            #print(ext.oid)
            subj_k_i = hexlify(ext.value.digest).decode()
            #print(subj_k_i)
            if config["brainpoolOpenSSL"]:
                test_1 = x509.SubjectKeyIdentifier.from_public_key(cert.public_key())
                if ext.value.digest != test_1.digest:
                    logging.warning("Keyidentifyer nicht RFC konform?")

    assert subj_k_i
    assert CertID

    a_k_i = ""
    for ext in cert.extensions:
        if ext.oid == x509.oid.ExtensionOID.AUTHORITY_KEY_IDENTIFIER:
            a_k_i = hexlify(ext.value.key_identifier).decode()

    return { "subj_k_i" : subj_k_i, 
             "cn"       : cn,
             "CertName" : CertName,
             "CertID"   : CertID,
             "a_k_i"    : a_k_i,
             "NotValidAfter" : cert.not_valid_after.isoformat(timespec='minutes')
           }

for i in root_cert_files:
    with open(i, "rb") as f:
        cert_der = f.read()
        root_der_data.append(hexlify(cert_der).decode())
        cert = x509.load_der_x509_certificate(cert_der, default_backend())
        cert_info = parse_cert(cert)
        root_ids[cert_info["subj_k_i"]] = 1
        root_CertIDs[cert_info["CertID"]]=1

xml_doc_root = ElementTree.parse(args.ImportFile).getroot()
if not xml_doc_root.tag.endswith('SGDCertificateImportList'):
    sys.exit("keine gültige SGDCertificateImportList-Datei")

CertIDs = defaultdict()
CACertIDs = defaultdict() 

for sgd_cert in xml_doc_root:
    if not sgd_cert.tag == 'SGDCertificate':
        logging.critical("ups")

    Cert_Attribute = {}
    for cert_attr in sgd_cert:
        Cert_Attribute[cert_attr.tag] = cert_attr.text

    for i in ['CertName', 'CertType', 'CertID', 'CertValue']:
        if not i in Cert_Attribute:
            logging.critical("Attribut " + i + " fehlt.")
    if not Cert_Attribute["CertType"] in ["Root", "TrustedCA", "OCSPSigner"]:
        logging.critical("unbekannter CertType " + Cert_Attribute["CertType"])

    try:
        der_cert_data = unhexlify(Cert_Attribute["CertValue"])
        cert = x509.load_der_x509_certificate(der_cert_data, default_backend())
        cert_info = parse_cert(cert)
    except Exception as my_exception:
        logging.critical("Fehler bei der Zertifikatsdekodierung")
        print(my_exception)

    assert '"' not in cert_info["cn"]
    print(Cert_Attribute["CertType"], f'"{cert_info["cn"]}"',
          cert_info["CertID"], cert_info["NotValidAfter"],
          hashlib.sha256(der_cert_data).hexdigest())
    if cert_info["CertID"] in CertIDs:
        logging.critical("Doppeltvergabe der CertID: " + cert_info["CertID"] +
                         " " + cert_info["cn"])
    else:
        CertIDs[cert_info["CertID"]] = 1

    if Cert_Attribute["CertType"] == "TrustedCA":
        CACertIDs[cert_info["CertID"]] = 1
        if cert_info["a_k_i"] == cert_info["subj_k_i"]:
            sign_info = "selfsigned"
        elif cert_info["a_k_i"] in root_ids:
            sign_info = "TIsigned({})".format(cert_info["a_k_i"])
        else:
            sign_info = "nonTIsigned"

        print("    #", cert_info["CertID"], sign_info)

    if (Cert_Attribute["CertType"] == "Root") and (
            (not cert_info["subj_k_i"] in root_ids) or (
             not Cert_Attribute["CertValue"] in root_der_data)):
        logging.critical("ungültiges Root-Zertifikat")

    if Cert_Attribute["CertType"] == "OCSPSigner":
        zugeordnete_CAs = []
        for cert_attr in sgd_cert:
            if cert_attr.tag == "OCSPSigneeList":
                for ocsp_signee in cert_attr:
                    Found_CertName = False
                    for eintrag in ocsp_signee:
                        if eintrag.tag == "CertID":
                            if len(eintrag.text)<1:
                                logging.critical("leere CertID im OCSP-Signee-Eintrag")
                            zugeordnete_CAs.append(eintrag.text)
                        elif eintrag.tag == "CertName":
                            Found_CertName = True
                            if len(eintrag.text)<1:
                                logging.critical("leere CertName im OCSP-Signee-Eintrag")
                        else:
                            logging.critical("unbekanntes Attribut im OCSP-Signee-Eintrag")
                    if not Found_CertName:
                        logging.critical("Kein CertName im OCSP-Signee-Eintrag")

        if len(zugeordnete_CAs)==0:
            logging.critical("OCSP-Signee wird keiner CA zugeordnet")
        else:
            for ca_id in zugeordnete_CAs:
                print("OCSPRelation", cert_info["CertID"], ca_id)
                if (not ca_id in CACertIDs) and (not ca_id in root_CertIDs):
                    # xxx der test funktioniert nur wenn die CAs vor dem OCSP-Responder definiert werden
                    # bei Gelegenheit zu verbessern
                    logging.critical("OCSP-Signee zugeordnete CA-CertID nicht gefunden, Reihenfolgen Problem?")
                        
