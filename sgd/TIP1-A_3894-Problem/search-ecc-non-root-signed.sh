#! /bin/bash

for certfile in *.der; do

    openssl x509 -inform der -in $certfile -text -noout >tmp.$$

    if [ ! -z "$(grep 'Public Key Algorithm: id-ecPublicKey' tmp.$$)" ]; then
        rca3_ski="DF:C3:BE:75:BE:25:EB:13:32:30:2E:A7:50:85:08:9F:37:10:D0:E8"
        rca4_ski="80:61:70:19:1C:38:ED:E2:6B:E4:A1:2E:AB:22:3E:75:C9:94:FE:7D"

        aki=$(grep -A 1 'X509v3 Authority Key Identifier:' tmp.$$ |tail -1 | sed 's/^ *keyid://')

        if [ -z "$aki" ]; then
            echo TIP1-A_3894-Problemfall $certfile    
            grep 'Subject: ' tmp.$$ |sed s'/^Subject://'
        else
            if [ $aki = $rca3_ski -o $aki = $rca4_ski ]; then
                # alles ok
                :
            else 
                echo TIP1-A_3894-Problemfall $certfile    
                grep 'Subject: ' tmp.$$ |sed s'/^Subject://'
            fi
        fi
    fi

    rm tmp.$$
done

