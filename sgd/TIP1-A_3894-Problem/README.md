# TIP1-A\_3894-Problem

Entgegen der Annahme, dass TIP1-A\_3894 (siehe folgend) von allen TSPs-X.509-ECC
umgesetzt wurde, gibt es doch selbstsignierte ECC-CA-Zertifikate für die PU.

Wir sind in der Spezifikation fälschlicher Weise davon ausgegangen, dass
es aufgrund von TIP1-A\_3894 solche CAs nicht gibt, i. S. v. alle ECC-CAs über die
X.509-Root prüfbar sind.

    TIP1-A_3894 - Obligatorisch abzuleitende Sub-CAs unterhalb der gematikRoot-CA

    Der TSP-X.509 nonQES MUSS Sub-CA-Zertifikate zur Erstellung von
    X.509-Zertifikaten von der gematikRoot-CA ableiten. [<=]

=> Wir müssen in die SGD-HSM ebenfalls die selbsignierten (im Sprachgebrauch
der SGD-Spezifikation "alten") ECC-CAs initial in die SGD-HSM importieren.

## Import-Daten aus der aktuellen ECC-RSA-TSL erzeugen

Wir machen Nägel mit Köpfen und importieren einfach alle CA/OCSP-Schlüssel in
die SGD-HSM.

    $ sha256sum ECC-RSA_TSL.xml
    dc51a861d19e003e417604f4ed4c78ed41aa536391f7585557ef901afe507c51  ECC-RSA_TSL.xml

    $ perl -ne 'if (/Id="(ID\d+Z)"/) { print $1,"\n"; }' < ECC-RSA_TSL.xml 
    ID31003420201201142928Z

    $ cd ..
    
    $ [a@h sgd]$ ./gen_import_list.py TIP1-A_3894-Problem/ECC-RSA_TSL.xml >TIP1-A_3894-Problem/import-sgd-s2-2020-12-06.xml
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:135} INFO - TSL-id: ID31003420201201142928Z
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:140} INFO - TSL erzeugt am 2020-12-01T14:29:28Z
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:144} INFO - nächstes Update 2020-12-31T14:29:28Z
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:156} INFO - ---------- medisign GmbH
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:156} INFO - ---------- arvato Systems GmbH
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:156} INFO - ---------- Atos Information Technology GmbH
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:156} INFO - ---------- IBM Deutschland GmbH
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:156} INFO - ---------- ITSG GmbH
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:156} INFO - ---------- T-Systems International GmbH
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:156} INFO - ---------- D-Trust GmbH
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:156} INFO - ---------- kubus IT GbR
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:156} INFO - ---------- BITMARCK
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:178} INFO - Ziel gefunden
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:156} INFO - ---------- Bundesnetzagentur
    [2020-12-06T15:12:10+0100] {./gen_import_list.py:250} INFO - In der TSL sind 10 TSPs mit zusammen 117 TSPServices enthalten. Es wurden 44 Suchziele gefunden und extrahiert.

    $ ./chk_import_list.py TIP1-A_3894-Problem/import-sgd-s2-2020-12-06.xml|grep -v '^ *#' |wc -l
    655
    
    $ ./chk_import_list.py TIP1-A_3894-Problem/import-sgd-s2-2020-12-06.xml|grep -v '^ *#' | cut -d\  -f1| sort | uniq -c | sort -nr
    569 OCSPRelation
     44 TrustedCA
     38 OCSPSigner
      4 Root

## Genauere Analyse fehlende "alte" ECC-CA-Zertifikate

    $ pwd
    /home/a/git/tsl-utils/sgd/TIP1-A_3894-Problem

    $ ../../Extract-TSP.py ECC-RSA_TSL.xml oid_egk_aut oid_egk_aut_alt oid_smc_b_aut
    TSL id ID31003420201201142928Z
    TSL erzeugt am 2020-12-01T14:29:28Z
    nächstes Update 2020-12-31T14:29:28Z
    In der TSL sind 10 TSPs mit zusammen 117 TSPServices enthalten. Es wurden 44 Suchziele gefunden und extrahiert.

    $ ls -m *.der
    1.der, 10.der, 11.der, 12.der, 13.der, 14.der, 15.der, 16.der, 17.der, 18.der,
    19.der, 2.der, 20.der, 21.der, 22.der, 23.der, 24.der, 25.der, 26.der, 27.der,
    28.der, 29.der, 3.der, 30.der, 31.der, 32.der, 33.der, 34.der, 35.der, 36.der,
    37.der, 38.der, 39.der, 4.der, 40.der, 41.der, 42.der, 43.der, 44.der, 5.der,
    6.der, 7.der, 8.der, 9.der

    $ ./search-ecc-non-root-signed.sh
    TIP1-A_3894-Problemfall 13.der
        Subject: C = DE, O = Atos Information Technology GmbH, OU = Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, CN = ATOS.EGK-CA203
    TIP1-A_3894-Problemfall 19.der
        Subject: C = DE, O = Atos Information Technology GmbH, OU = Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, CN = ATOS.EGK-CA8
    TIP1-A_3894-Problemfall 20.der
        Subject: C = DE, O = Atos Information Technology GmbH, OU = Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, CN = ATOS.EGK-CA10
    TIP1-A_3894-Problemfall 33.der
        Subject: C = DE, O = T-Systems International GmbH, OU = Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, CN = TSYSI.EGK-CA8
    TIP1-A_3894-Problemfall 38.der
        Subject: C = DE, O = kubus IT GbR, OU = Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, CN = KUBUS.EGK-CA3
    TIP1-A_3894-Problemfall 43.der
        Subject: C = DE, O = BITMARCK, OU = Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, CN = BITMARCK.EGK-CA12
    TIP1-A_3894-Problemfall 6.der
        Subject: C = DE, O = Atos Information Technology GmbH, OU = Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, CN = ATOS.EGK-CA6
    TIP1-A_3894-Problemfall 7.der
        Subject: C = DE, O = Atos Information Technology GmbH, OU = Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, CN = ATOS.EGK-CA14

## Vergleich der neuen Import-Daten (s. o.) mit den initialen Importdaten-SGD2 (August 2020)

    $ ./chk_import_list.py TIP1-A_3894-Problem/import-sgd-s2-2020-12-06.xml |grep -v '^ *#' | grep -v OCSPRelation | sort >h1
    $ ./chk_import_list.py my_key_data.xml |grep -v '^ *#' | grep -v OCSPRelation | sort >h2
    $ diff h1 h2
    1,5d0
    < OCSPSigner "ATOS.EGK-ALVI-OCSP1" 41544f532e45474b2d414c56492d4f43535031 2025-11-08T12:44 61cca3b2bcfb56908bfa96b109452196a5910016e87bf0c4542d3399c129ba03
    < OCSPSigner "ATOS.EGK-ALVI-OCSP2" 41544f532e45474b2d414c56492d4f43535032 2025-11-08T12:45 d98d60ae4ce3db3f1bf9d4a5a9a95a7cd9b4ffd8765152a90cfe2ac7aba4c33b
    < OCSPSigner "ATOS.EGK-ALVI-OCSP3" 41544f532e45474b2d414c56492d4f43535033 2025-11-08T12:46 63a8e888256b4f2dd46dfb90ad4fbc2f2ca624bcfe5303821824ef5bf95612ef
    < OCSPSigner "ATOS.EGK-ALVI-OCSP4" 41544f532e45474b2d414c56492d4f43535034 2025-11-08T12:47 b4380ca5ba25148f8321265381993b3bebc67fcc06daf0f048541c4dd6f67a38
    < OCSPSigner "ATOS.EGK-OCSP10" 41544f532e45474b2d4f4353503130 2023-10-01T12:38 e2c7c838b03807b1ee4cb5f0494509716725c164640b30ea403f45fa118b8c92
    9d3
    < OCSPSigner "ATOS.EGK-OCSP14" 41544f532e45474b2d4f4353503134 2024-05-06T13:08 8160968c956f028322b29919bec45cb26097e246ace5f5d9ca349e817abf9289
    12d5
    < OCSPSigner "ATOS.EGK-OCSP203" 41544f532e45474b2d4f435350323033 2023-11-13T15:45 94530ba9c73ff963f71feac70d587e6c8b1e39f34b1d6755c26f6f29836f5142
    14d6
    < OCSPSigner "ATOS.EGK-OCSP6" 41544f532e45474b2d4f43535036 2023-06-19T13:23 a7d61f116e3b841021a5c7ec94417344061083a45338fa348de781619a01070c
    16d7
    < OCSPSigner "ATOS.EGK-OCSP8" 41544f532e45474b2d4f43535038 2023-08-29T12:51 e318ad5f33b39186aae502b2f71c0cad5d220892df3d83c63164e336fbd4430f
    20d10
    < OCSPSigner "ATOS.SMCB-OCSP2" 41544f532e534d43422d4f43535032 2025-01-13T09:09 b5944346458c8a4b0cba2bfd349e5ce8187183bb1f5eb3525ceb8d6bba40ddbe
    22,23c12
    < OCSPSigner "BITMARCK.EGK-OCSP_SIGNER_12" 4249544d4152434b2e45474b2d4f4353505f5349474e45525f3132 2023-10-25T15:26 d162e8c34b7b3a91b836cedb0a967190fd5d7a5e96d96691e79e3412c5be2d82
    < OCSPSigner "BITMARCK.EGK-OCSP_SIGNER_27" 4249544d4152434b2e45474b2d4f4353505f5349474e45525f3237 2025-11-18T17:16 a1d82438023e1a343ae4bb690dd205c092f400cf8506a5ff26fbb36733556876
    ---
    > OCSPSigner "D-Trust.HBA-CA1-OCSP-Signer" 442d54727573742e4842412d4341312d4f4353502d5369676e6572 2020-11-29T09:34 e81b30cad32d9954ce92d9b6e629c0d6ad12ef336cff3573212c52e403aaed1c
    24a14
    > OCSPSigner "D-Trust.SMCB-CA1-OCSP-Signer" 442d54727573742e534d43422d4341312d4f4353502d5369676e6572 2020-11-29T09:37 4cdcd7dc1b884f6384b2590b71821dc669216a9dbf20aea5e27815134f061216
    31d20
    < OCSPSigner "TSYSI.EGK-OCSP-Signer8" 54535953492e45474b2d4f4353502d5369676e657238 2024-07-23T23:59 9e92394eaf6f580d95fd9ea410de98e8e9619c26d923c2e7d608e903ff245efe
    36d24
    < OCSPSigner "kubus IT OCSP Signer 1 CA3-ECC" 6b75627573204954204f435350205369676e65722031204341332d454343 2023-11-25T16:04 64f83a5e276cfbe0d8590b7001f6b051ec64a9b274df04f7592a136680681053
    43,46d30
    < TrustedCA "ATOS.EGK-ALVI-CA1" 41544f532e45474b2d414c56492d434131 2028-10-12T09:57 2c59b3757482bf7116ce4db1513e488a6be7821b32fe76c8496732f7f9aa3a63
    < TrustedCA "ATOS.EGK-ALVI-CA2" 41544f532e45474b2d414c56492d434132 2028-10-12T09:57 c2f543c11ff0c7ece7e2872353ca0da580e4749f9aa9e65df88f388656d8992f
    < TrustedCA "ATOS.EGK-ALVI-CA3" 41544f532e45474b2d414c56492d434133 2028-10-12T09:57 2bc759c9797dd8ddc1685c02fc94788642948a3a4682e299e30a8f4acf925f54
    < TrustedCA "ATOS.EGK-ALVI-CA4" 41544f532e45474b2d414c56492d434134 2028-10-12T09:57 114fbab86f237c2d3af5820961ed10db3d5b3c38efbd919859a7017aee6d9ef9
    48d31
    < TrustedCA "ATOS.EGK-CA10" 41544f532e45474b2d43413130 2026-10-02T12:29 18d466086f4b0f62f2ac13ece9f9eec6e7342074d81a0b8f642faa8d7607fbb5
    52d34
    < TrustedCA "ATOS.EGK-CA14" 41544f532e45474b2d43413134 2027-05-08T12:55 b277c3a4d8f2c57cee4ac7ba1ce693bcdde31f661dcb5173bd67ae64ab5b8ba5
    56d37
    < TrustedCA "ATOS.EGK-CA203" 41544f532e45474b2d4341323033 2026-11-14T15:30 195df845fef9540730b2b3653d2ea392558a49629b6079ead339f635d41d3474
    60d40
    < TrustedCA "ATOS.EGK-CA6" 41544f532e45474b2d434136 2026-06-19T14:11 f78f126153cc251678913c31bc55b4249b407101ec77823a249810846eed9a9d
    62d41
    < TrustedCA "ATOS.EGK-CA8" 41544f532e45474b2d434138 2026-08-30T12:40 3a7b8aed923e4ebc9ddb4af32310b371e2e767d04f4ff510ccfe92335080589b
    65d43
    < TrustedCA "ATOS.SMCB-CA2" 41544f532e534d43422d434132 2028-01-07T09:02 90d569741cb59116fb87f09c27222685075b801f8d9a97e197399c6370d5ce5d
    67,68d44
    < TrustedCA "BITMARCK.EGK-ALVI-CA27" 4249544d4152434b2e45474b2d414c56492d43413237 2028-11-16T10:05 931426863238348774af0d4e0c6e94bfa59d5bb6d0f62a35f1e5ea5bace86d67
    < TrustedCA "BITMARCK.EGK-CA12" 4249544d4152434b2e45474b2d43413132 2026-10-23T09:35 158e649e74bac42dc25acb2b9c69f39b4b98d39219ecafd7902d8685d9a8a50d
    75d50
    < TrustedCA "KUBUS.EGK-CA3" 4b554255532e45474b2d434133 2026-11-24T13:18 d45bcf4969b264200168793443435731e4f094eeda40a7656d0cfaf3532cf78f
    83d57
    < TrustedCA "TSYSI.EGK-CA8" 54535953492e45474b2d434138 2027-07-21T23:59 f5ae56eb8d965a4fa3e68f166fd30bdc4f7fb1d3358f20f23d6b9eac0f983dcf

