# SDG2 Zertifikat erzeugt am 6.11.2024 bei D-Trust 

SDG2 Zertifikat erzeugt am 6.11.2024 bei D-Trust im 4-Augen-Prinzip mit der gematik.

    $ openssl x509 -noout -text -in SGD2-PU-2024.pem
    Certificate:
        Data:
            Version: 3 (0x2)
            Serial Number: 86772 (0x152f4)
            Signature Algorithm: ecdsa-with-SHA256
            Issuer: C = DE, O = gematik GmbH, OU = Komponenten-CA der Telematikinfrastruktur, CN = GEM.KOMP-CA9
            Validity
                Not Before: Nov  6 15:04:33 2024 GMT
                Not After : Nov  5 15:04:32 2029 GMT
            Subject: C = DE, O = D-Trust GmbH, CN = SGD2-PU-2024
            Subject Public Key Info:
                Public Key Algorithm: id-ecPublicKey
                    Public-Key: (256 bit)
                    pub:
                        04:8b:d3:27:b1:35:28:57:c3:12:b6:74:f2:61:cc:
                        76:08:f1:3e:0b:93:29:30:a0:7a:b0:1a:d1:10:49:
                        a4:be:6a:70:0f:35:dc:33:82:fe:37:92:26:5a:a5:
                        c5:c0:0c:28:eb:0a:d1:37:68:ca:b4:09:04:64:7f:
                        90:e5:6b:1a:f2
                    ASN1 OID: brainpoolP256r1
            X509v3 extensions:
                X509v3 Subject Key Identifier:
                    25:D9:64:63:8C:8A:9C:B6:BC:0F:9B:3D:28:4A:FF:1D:0C:F2:A3:86
                X509v3 Authority Key Identifier:
                    89:00:07:E7:DF:25:01:1F:E7:F2:EC:A3:F7:5F:38:F9:B0:67:19:7A
                Authority Information Access:
                    OCSP - URI:http://download.crl.ti-dienste.de/ocsp/ec
                X509v3 Key Usage: critical
                    Digital Signature
                X509v3 Certificate Policies:
                    Policy: 1.2.276.0.76.4.163
                    Policy: 1.2.276.0.76.4.214
                X509v3 Basic Constraints: critical
                    CA:FALSE
                Professional Information or basis for Admission:
                    Entry 1:
                      Profession Info Entry 1:
                        Info Entries:
                          HSM Schl..sselgenerierungsdienst 2
                        Profession OIDs:
                          undefined (1.2.276.0.76.4.220)

        Signature Algorithm: ecdsa-with-SHA256
        Signature Value:
            30:45:02:21:00:a0:61:b7:02:77:d7:7b:93:e2:76:9e:eb:2b:
            00:b6:5f:58:af:c0:cf:22:31:99:17:94:d9:b7:53:2e:aa:35:
            2e:02:20:11:2b:49:dc:a2:59:73:65:e7:08:9b:01:3f:b5:6b:
            c5:84:8f:c0:e2:35:c7:52:5b:8c:43:dd:a1:70:40:68:a0


    $ cat SGD2-PU-2024.pem
    -----BEGIN CERTIFICATE-----
    MIICsTCCAlegAwIBAgIDAVL0MAoGCCqGSM49BAMCMG8xCzAJBgNVBAYTAkRFMRUw
    EwYDVQQKDAxnZW1hdGlrIEdtYkgxMjAwBgNVBAsMKUtvbXBvbmVudGVuLUNBIGRl
    ciBUZWxlbWF0aWtpbmZyYXN0cnVrdHVyMRUwEwYDVQQDDAxHRU0uS09NUC1DQTkw
    HhcNMjQxMTA2MTUwNDMzWhcNMjkxMTA1MTUwNDMyWjA7MQswCQYDVQQGEwJERTEV
    MBMGA1UECgwMRC1UcnVzdCBHbWJIMRUwEwYDVQQDDAxTR0QyLVBVLTIwMjQwWjAU
    BgcqhkjOPQIBBgkrJAMDAggBAQcDQgAEi9MnsTUoV8MStnTyYcx2CPE+C5MpMKB6
    sBrREEmkvmpwDzXcM4L+N5ImWqXFwAwo6wrRN2jKtAkEZH+Q5Wsa8qOCARMwggEP
    MB0GA1UdDgQWBBQl2WRjjIqctrwPmz0oSv8dDPKjhjAfBgNVHSMEGDAWgBSJAAfn
    3yUBH+fy7KP3Xzj5sGcZejBFBggrBgEFBQcBAQQ5MDcwNQYIKwYBBQUHMAGGKWh0
    dHA6Ly9kb3dubG9hZC5jcmwudGktZGllbnN0ZS5kZS9vY3NwL2VjMA4GA1UdDwEB
    /wQEAwIHgDAhBgNVHSAEGjAYMAoGCCqCFABMBIEjMAoGCCqCFABMBIFWMAwGA1Ud
    EwEB/wQCMAAwRQYFKyQIAwMEPDA6MDgwNjA0MDIwJAwiSFNNIFNjaGzDvHNzZWxn
    ZW5lcmllcnVuZ3NkaWVuc3QgMjAKBggqghQATASBXDAKBggqhkjOPQQDAgNIADBF
    AiEAoGG3AnfXe5Pidp7rKwC2X1ivwM8iMZkXlNm3Uy6qNS4CIBErSdyiWXNl5wib
    AT+1a8WEj8DiNcdSW4xD3aFwQGig
    -----END CERTIFICATE-----


# KOMP-CAs aus TSL extrahieren

    $ ./Extract-TSP.py ECC-RSA_TSL.xml oid_fd_tls_s
    TSL id ID31027920241028000006Z
    TSL erzeugt am 2024-10-28T00:00:06Z
    nächstes Update 2024-11-27T00:00:06Z
    In der TSL sind 9 TSPs mit zusammen 263 TSPServices enthalten. Es wurden 7 Suchziele gefunden und extrahiert.

# SGD2-HSM-Zertifikat aus Spass mal prüfen

`7.pem` ist KOMP-CA9 automatisch extrahiert mittels `Extract-TSP.py`. 
Der folgende Befehl prüft ebenfalls die das KOMP-CA9-Zertifikat -- 
es wird als zunächst `untrusted` deklariert, damit openssl es auch prüft.


    $ openssl verify -CAfile ../GEM.RCA8.pem -untrusted ../../komp-ca/7.pem SGD2-PU-2024.pem
    SGD2-PU-2024.pem: OK


