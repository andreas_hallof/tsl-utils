#! /usr/bin/env python3

import sys, base64, os, argparse, json, hashlib
from datetime import datetime
from xml.etree import ElementTree
from binascii import hexlify, unhexlify

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.hazmat.primitives import hashes, serialization
from cryptography.exceptions import InvalidSignature


# Ich gehen davon aus, dass zuvor die Signatur der TSL mit Chk-Signature-TSL.py
# erfolreich und mit positiven Ergebnis geprüft worden ist.
# Die hier XML-Daten verarbeiteten Daten sind also nicht durch einen Angreifer
# manipuliert worden (unter der Annahme der TSL-Dienst ist nicht kompromittiert). 
#

a_p=argparse.ArgumentParser(description="Aus einer angegeben TSL-Datei suche ich die (TI-internen) URLs"+
 " und den Signaturschlüssel (Svctype/Certstatus/OCSP) des OCSP-Responders für jedes CA-Zertifikat (Svctype/CA/PKC)."+
 " Zertifikate aus dem eIDAS-Vertrauensraums werden ignoriert.")
a_p.add_argument('-v', '--verbose', dest='verbose', action='store_true', help='verbose mode')
a_p.add_argument(dest='TSL_Datei', metavar='TSL-Datei')
args=a_p.parse_args()

if not (os.path.exists(args.TSL_Datei) and os.path.isfile(args.TSL_Datei)):
    sys.exit('Datei "{}" nicht gefunden'.format(args.TSL_Datei))

ns={ 'tsl': 'http://uri.etsi.org/02231/v2#' } # ElementTree.register_namespace('tsl', ns['tsl'])

root = ElementTree.parse(args.TSL_Datei).getroot()
if root.tag != '{http://uri.etsi.org/02231/v2#}TrustServiceStatusList':
    sys.exit("keine gültige TI-TSL")

if 'Id' in root.attrib:
    print('TSL-id: {}'.format(root.attrib['Id']))
else:
    sys.exit("keine gültige TI-TSL")

c1=root.findall('./tsl:SchemeInformation/tsl:ListIssueDateTime', ns);
print('TSL erzeugt am', c1[0].text)
if datetime.now()<datetime.strptime(c1[0].text, '%Y-%m-%dT%H:%M:%SZ'):
    print("WARNUNG: lokale Zeit ist falsch.")
c1=root.findall('./*/tsl:NextUpdate/tsl:dateTime', ns);
print("nächstes Update", c1[0].text)
if datetime.strptime(c1[0].text, '%Y-%m-%dT%H:%M:%SZ')<datetime.now():
    print("WARNUNG: TSL ausgelaufen oder lokale Zeit ist falsch.")

TSP_Counter=0; TSPService_Counter=0; Zertifikate_Gefunden_Counter=0

RoutingTable=dict()

for a in root:
    if a.tag != '{http://uri.etsi.org/02231/v2#}TrustServiceProviderList':
        continue
    for b in a:
        if b.tag != '{http://uri.etsi.org/02231/v2#}TrustServiceProvider':
            continue
        x=b.find('./tsl:TSPInformation/tsl:TSPName/tsl:Name', ns)
        if args.verbose:
            print('TSP "{}"'.format(x.text))
        for c in b:
            if c.tag != '{http://uri.etsi.org/02231/v2#}TSPServices':
                continue
            for d in c:
                #print(d.tag)
                sti=d.find('.//tsl:ServiceTypeIdentifier', ns)
                if not sti.text.endswith('/PKC'):
                    continue
                if args.verbose:
                    print(sti.text)
                sn=d.find('.//tsl:ServiceName/tsl:Name', ns);
                cert_b64=d.find('.//tsl:X509Certificate', ns);
                ssp=d.find('.//tsl:ServiceSupplyPoint', ns); 
                cert=x509.load_der_x509_certificate(base64.b64decode(cert_b64.text), default_backend())
                if args.verbose:
                    print(sn.text)
                    print(cert_b64.text[0:60])
                    print(ssp.text)
                    print('Zertifikat ("{}", SN: {}, gültig bis {}).'.format(
                      ', '.join(x for x in [ y.value for y in cert.subject ]),
                      cert.serial_number, cert.not_valid_after.isoformat(timespec='minutes')
                      )
                    )
                public_key=cert.public_key(); #print(public_key.key_size)
                pb=public_key.public_bytes(serialization.Encoding.DER,
                                           serialization.PublicFormat.PKCS1)
                #print(hexlify(pb))
                h_sha1=hashlib.sha1(pb).hexdigest();
                h_sha256=hashlib.sha256(pb).hexdigest();
                if args.verbose:
                    print('SHA-1:', h_sha1)
                    print('SHA-256:', h_sha256)
                RoutingTable[h_sha1]=RoutingTable[h_sha256]=ssp.text

print("\nRouting-Tabelle im JSON-Format:\n{}\n".format(
        json.dumps(RoutingTable, sort_keys=True, indent=4)
        )
     )

