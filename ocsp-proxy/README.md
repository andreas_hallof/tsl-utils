# Routing-Tabelle erzeugen

## Fachlicher Hintergrund

In der Spezifikation des Zugangsgateway des Versicherten (ZGdV)
([gemSpec\_Zugangsgateway\_Vers](https://fachportal.gematik.de/spezifikationen/online-produktivbetrieb/))
wird gefordern, dass das ZGdV OCSP-Anfragen von einem anfragenden ePA Frontend des Versicherten 
an die OCSP-Responder in der TI weiterleitet:

>   **A\_15869 - Zugangsgateway des Versicherten, Bereitstellung OCSP-Forwarder**
>
>   Ein Zugangsgateway des Versicherten MUSS folgende Vorgaben umsetzen:
>   
>   * Es MUSS unter dem in [gemSpec\_Aktensystem] Tabelle: Tab\_ePA\_FQDN
>   angegeben Pfadnamen für den key "ocspf" einen Proxy zur Statusabfrage über
>   sein vom ePA-Frontend des Versicherten schon genutztes HTTPS-Interface zur
>   Verfügung stellen (HTTP-POST, vgl. auch [RFC-6960, Appendix CgemSpec\_PKI]).
>   * Es MUSS über die Schnittstelle aus Spiegelstrich 1 OCSP-Requests [RFC-6960]
>   entgegen nehmen.
>   Aus einem solchen OCSP-Request MUSS es aus dem issuerKeyHash [RFC-
>   6960] die URL des entsprechenden OCSP-Responders in der TI ermitteln
>   (Datengrundlage ist die TSL der TI) und den OCSP-Request an diese ermittelte
>   URL weiterleiten.
>   * Es MUSS die erhaltenen OCSP-Response an das die OCSP-Anfrage
>   stellende ePA-Frontend des Versicherten unverändert weiterreichen.
>
>   [<=]
>
>   Auf Anfrage stellt die gematik eine Beispielimplementierung für A\_15869 Spiegelstrich 3
>   bereit.

D. h. beim ZGdV wird auf einer speziellen URL (bspw. "/ocsp", vgl. diesbez.
[gemSpec\_Aktensystem] Tabelle: Tab\_ePA\_FQDN beim Eintrag "ocspf") ein
HTTP-POST-Request ankommen, mit Content-Type "application/ocsp-request" (vgl.
[RFC-6960, Appendix C]). 

Ein solcher OCSP-Request hat eine genau definierte und sehr einfach zu erkennden
(i. S. v. wenig variable) Struktur:

      0  66: SEQUENCE {
      2  64:   SEQUENCE {
      4  62:     SEQUENCE {
      6  60:       SEQUENCE {
      8  58:         SEQUENCE {
     10   9:           SEQUENCE {
     12   5:             OBJECT IDENTIFIER sha1 (1 3 14 3 2 26)
     19   0:             NULL
           :             }
     21  20:           OCTET STRING
           :             FD 40 A1 51 E2 BC 48 25 61 53 3E 75 A3 C6 64 C5
           :             74 49 D1 8F
     43  20:           OCTET STRING
           :             EC 5C 18 E0 13 B4 43 6C 09 8C DF FA 3C 3C 5B 7E
           :             4B 70 84 46
     65   1:           INTEGER 2
           :           }
           :         }
           :       }
           :     }
           :   }

oder 

      0  94: SEQUENCE {
      2  92:   SEQUENCE {
      4  90:     SEQUENCE {
      6  88:       SEQUENCE {
      8  86:         SEQUENCE {
     10  13:           SEQUENCE {
     12   9:             OBJECT IDENTIFIER sha-256 (2 16 840 1 101 3 4 2 1)
     23   0:             NULL
           :             }
     25  32:           OCTET STRING
           :             60 CA 33 ED A0 83 73 91 B8 02 9F 01 DD C9 C0 62
           :             22 66 F5 08 1D B2 C3 B2 E2 9F AE 8E 30 E7 E6 B2
     59  32:           OCTET STRING
           :             90 9F B9 58 BA 12 49 B5 D2 F2 53 B0 F3 5A F2 52
           :             0A 1B FA C0 FF E1 A2 E0 BF 60 C7 9C 0D 45 67 92
     93   1:           INTEGER 2
           :           }
           :         }
           :       }
           :     }
           :   }

D. h. 

1. Das ZGdV erkennt anhand des "Content-Type"s, dass es sich um einen OCSP-Request handelt.
2. Es erkennt durch Bitvergleich der ersten 21 Bytes, ob es sich entweder 
   um einen "SHA-256"-OCSP-Request oder um einen "SHA-1"-OCSP-Request handelt.
3. Damit weiss das ZGdV bei welchem offset der issuerKeyHash (vgl. A\_15869, Punkt 3)
   beginnt und wie lang dieser ist.
4. Das ZGdV prüft die Länge des Requests und per Bitvergleich, ob die zu erwartenden 
   (i. S. v. konstanten) ASN.1-Struktur-Bytes sind (es ist hier keine aufwändige ASN.1-Dekodierung
   erforderlich).
5. Das ZGdV hat eine Tabelle mit der Zuordnung issuerKeyHash <-> URL (siehe unten GenRoutingTable.py).
6. Das ZGdV sendet den unveränderten OCSP-Request per HTTP-POST an die URL aus der Tabelle.
7. Was immer es als Antwort vom OCSP-Responder (vgl. URL aus der Tabelle) in
   der TI bekommt, sendet es als HTTP-Response an das anfragende FdV weiter.

Das ZGdV muss den OCSP-Request oder die OCSP-Repsonse nicht verstehen oder
irgendwie andersweitig parsen.

## Beispiel PU 
Das Programm GenRoutingTable.py erzeugt aus einer gültigen TSL (vgl. Chk-Signature-TSL.py) 

1. eine Abbildung issuerKeyHash (SHA-1) <-> URL, und
2. eine Abbildung issuerKeyHash (SHA-256) <-> URL.

Beispiel-Ausführung:

    $ ./GenRoutingTable.py ../TSL.xml 
    TSL-id: ID327020180527230000Z
    TSL erzeugt am 2018-05-27T23:00:00Z
    nächstes Update 2018-06-26T23:00:00Z
    WARNUNG: TSL ausgelaufen oder lokale Zeit ist falsch.
 
    Routing-Tabelle im JSON-Format:
    {
        "07eb35f9ed657093125a08b6b6b7251b00e6d7290d37f76ca6ab5dfc8abec61b": "http://ocsp1.atos.tsp-egk.telematik",
        "097dd2ecafc610289cf8a79daa403f1ac41fb547": "http://ocsp.kubusit.tsp-egk.telematik:8080/ocsp",
        "0a1f3f265f8f2d71c7965324d2f6457344be07d9c3f7e538202f0c444654a5a9": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eA.medisign.de%3A8080%2Focsp",
        "0fb5fd3c20a426033540121bf3c57cffff49cef08d25ed0e9602ced7b00152d7": "http://ocsp-egk.tsi.tsp-egk.telematik/ocspr",
        "1414b6b08db8e317b17b1acc81e29108fa9fcd4b": "http://ocsp1.atos.tsp-egk.telematik",
        "159bd00d6c784d6ac8eab446e64611f939aad9ff": "http://ocsp1.atos.tsp-egk.telematik",
        "1a2efd3464f1b95c878eeec6ecbb70dbf29d1e4a3644e5f133cc7d47f5a3b8b5": "http://ocsp1.atos.tsp-egk.telematik",
        "1aed9befc190da65223f7431a0f3ebb7685e38ba": "http://ocsp-hba.tsi.tsp-hba.telematik/ocspr",
        "216c0537f4d017a4cf4389315f79589ce2303d2a52d1005b5adb91d54463ce67": "http://ocsp-hba.tsi.tsp-hba.telematik/ocspr",
        "28171dbbcad34bf78e15819d600b3427edb07b50": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-ePTA.medisign.de%3A8080%2Focsp",
        "2af62411667b55f3f49cf351432836fe0e93837f": "http://ocsp-smcb.tsi.tsp-smc-b.telematik/ocspr",
        "2b2710c917992dda6a05dd4469a8166bb0cf07c1": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eA.medisign.de%3A8080%2Focsp",
        "2d7d47a533ad249b23754601f22e9082567434d130c533cb7f64a52ecffee9a3": "http://ocsp.tsl.telematik/ocsp",
        "2e2de6dd2055b7b74d186788badf17bb614dd292e5159d2c42b633b27f600860": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eZAA.medisign.de%3A8080%2Focsp",
        "32c06d315a816b3317e99156fb0ebf67f695d8ec4f8fd8b80ec856de848ba5a8": "http://ocsp1.atos.tsp-egk.telematik",
        "32c9d99143d30edfcdba2e86674ef59d65a9758c07d99aa6da206b65ad943f64": "http://ocsp-smcb.tsi.tsp-smc-b.telematik/ocspr",
        "35c9ea6220bfa8f3dfd05a401a3c4818cae1f91b": "http://ocsp-egk.bitmarck.tsp-egk.telematik:8180/ocsp-egk",
        "38e00cdd21f740ae30e7709173c0ace227f4e79c": "http://ocsp.tsl.telematik/ocsp",
        "3ca6ee2110a062181795f5d7bd3362a83d32c008d2a531a47c961fd8937576f4": "http://ocsp.kubusit.tsp-egk.telematik:8080/ocsp",
        "42b899c13a588208599c18cf3947669de133fae6": "http://ocsp.komp-ca.telematik/ocsp",
        "43614a6b9565f8143f990018c78567e1dabcc9d10350c1c143463bea174ee053": "http://ocsp.bdr.tsp-hba.telematik",
        "4a3a8c58e3588ad290d517dfe80bddff90f19007179a6daf7ee8cc891ebaa40f": "http://ocsp-egk.tsi.tsp-egk.telematik/ocspr",
        "4b1a00655abd865e048d69dd2f7ee6c2705a93ea": "http://ocsp-egk.tsi.tsp-egk.telematik/ocspr",
        "4d67b9877812eb8e2bcdae03899b525ed0b993aca5e6f08ee368c27e0e53ab03": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp",
        "4def0393a8e6f35ecc9c8e85d5bcdd4a326317ac": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp",
        "4fef05190a566ed4f8dba5fa72176711033f5ed884b546af2a724c2e250fbe33": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp",
        "521e10ac6b279232f4600f2456578cc1be4e72fc6d61756a9fa413410deae116": "http://ocsp-egk.bitmarck.tsp-egk.telematik:8180/ocsp-egk",
        "599962cd2b8439064ce546a2f95d2fd40b680e27": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eA.medisign.de%3A8080%2Focsp",
        "5f5114bb98201927908bd571e23efaf19e10bd40": "http://ocsp1.atos.tsp-egk.telematik",
        "657cbd744e7b3525bbb85f88884e4a3279bf5aa8": "http://ocsp-egk.tsi.tsp-egk.telematik/ocspr",
        "690aa71f7c224bd4f681cf5f73b6c62646ad966c": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp",
        "6abdc0383272c291672da1a141490813e7e7fe77": "http://download.crl.ti-dienste.de/crl/vpnk-ca1.crl",
        "6ed982b14798626e266b6f74cdb3435cb79cc007": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eZAA.medisign.de%3A8080%2Focsp",
        "6f91661f5727edfe3425f9cb24d327945d788f975b9bda1c25e82c49906b486e": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-ePTA.medisign.de%3A8080%2Focsp",
        "71beccac2f46f7af7503ce983b86e2f2bc56732e": "http://ocsp.kubusit.tsp-egk.telematik:8080/ocsp",
        "76d94708965eb780fb1378387279c1f9f03f664c5b8142b25521ddf46c5af141": "http://ocsp.komp-ca.telematik/ocsp",
        "7720fe98dbf9853cb489ce95b776650786a38725": "http://ocsp1.atos.tsp-egk.telematik",
        "794be575dc1eb7497a59451fb9c54d73d52ab6c9b8b23a199a8e108186511db3": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eZAA.medisign.de%3A8080%2Focsp",
        "7cd46422fc3386a96678788ea0089bcd241c1b2a2b7e944b1c6e349a5ab3ea0f": "http://ocsp1.atos.tsp-egk.telematik",
        "83d7323a5540402e6e563a0170ec2271952718bc": "http://ocsp.kubusit.tsp-egk.telematik:8080/ocsp",
        "8e1fda28ca6fcca6943f858fabe51ce3e3b1735a": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp",
        "90a763f542945f70d0b277b410a8f39ea1fb32fe285271de0929297255171e4d": "http://ocsp1.atos.tsp-egk.telematik",
        "9150167a63834ab9c5b106c71e65065e68c7ef37c16fafaf9c8e3927cad858be": "http://ocsp1.atos.tsp-egk.telematik",
        "9d121b291082a794b6146e7c4b2d7d162023874c31970e0dbb23a4274296ba21": "http://ocsp-egk.tsi.tsp-egk.telematik/ocspr",
        "a79c1941949bd11de5b8833ab20bb3d6560bf63386817cc03ddef58102588430": "http://ocsp-egk.bitmarck.tsp-egk.telematik:8180/ocsp-egk",
        "a7e7605a2a91651142ba8469fd364c8d2b2cfe0ddd04ede291319498c23b62cb": "http://ocsp.komp-ca.telematik/ocsp",
        "a7ea85d6dff6ce26a887bda1e04e9cd27886a3be89e5bf897f8a6c229f2635be": "http://download.crl.ti-dienste.de/crl/vpnk-ca1.crl",
        "ad1199685faaa2e4107822804f7dae9f57f0d27404a2cd48aabca7815f2f48d9": "http://ocsp-egk.tsi.tsp-egk.telematik/ocspr",
        "ad1f31bb64352f9df766d1021aa9133b3097602e4818606fba97f633ee7154cf": "http://download.crl.ti-dienste.de/crl/vpnk-ca1.crl",
        "aea3bdb4bacc1736c80f8dd211f08beeed2eec9b9fbe1a41679cec069a1700c0": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp",
        "af30a956287ab6615c8a33d501b6d8585463314d": "http://ocsp1.atos.tsp-egk.telematik",
        "be2001f194a5841fb9b782ef9e201dcd188713c09f829b91f2098bd10c684e0c": "http://ocsp.kubusit.tsp-egk.telematik:8080/ocsp",
        "c1db45fdcd755ea321e6c7a29b192e45752dcda9a8eb14e0b64e0ae209e7807c": "http://ocsp1.atos.tsp-egk.telematik",
        "c730a2a3a8f94834beb0096ac147f23f5f972802": "http://ocsp1.atos.tsp-egk.telematik",
        "cc685c7d3ba3a3a4f2af149032d3d6e3d7fc63be": "http://ocsp-egk.bitmarck.tsp-egk.telematik:8180/ocsp-egk",
        "d004db051c6e70249bc65b352b28c64b6715008434bf30bad8bcb0c69bcc9ecf": "http://ocsp.kubusit.tsp-egk.telematik:8080/ocsp",
        "d4fffdef4bc1b19506bbf514bf90c3706134020ae4f0d6ca8dabf0f55bfaeb19": "http://ocsp1.atos.tsp-egk.telematik",
        "dac6a3d34ad9fb3ddf740e2a790dcfc481fc9430": "http://ocsp1.atos.tsp-egk.telematik",
        "dde4ee0a453a7fe87edce3a82af83c978ac9a6ee03f2cc9bd8972a9f6d4276f3": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eA.medisign.de%3A8080%2Focsp",
        "e159185581e551c3a4e9e5e692d2949ee32e11a7": "http://ocsp-egk.tsi.tsp-egk.telematik/ocspr",
        "e1cd791e91b46f0f51158caee5b2d231446c94a45a45b3b5c7681cf856817480": "http://ocsp1.atos.tsp-egk.telematik",
        "e3b27ab778008c794fda8e5fa6b5069a0d06af62": "http://ocsp1.atos.tsp-egk.telematik",
        "ec120e565a88256fbb0741f2e614ba409bbee07e": "http://ocsp.bdr.tsp-smc-b.telematik",
        "f090ae4954fa9edb5272e81e3a62d355b9becb18d783bece9c19f5b7f3f21e32": "http://ocsp.bdr.tsp-smc-b.telematik",
        "f2243b3b22776658e6d431c37f1bc4c1ae758aab": "http://ocsp.bdr.tsp-hba.telematik",
        "f4e23b61186783c002aae87c64272a9276daedc3": "http://ocsp-egk.tsi.tsp-egk.telematik/ocspr",
        "f514d213ba2f31713239e61f3d9004389ba076d8": "http://ocsp1.atos.tsp-egk.telematik",
        "f6f9f0076c768d6d3762bce44515804c94237810": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eZAA.medisign.de%3A8080%2Focsp",
        "f7b2031c7499ad6421090e05d8be0056fc7a96a4": "http://download.crl.ti-dienste.de/crl/vpnk-ca1.crl",
        "f989ad8197ae5a1f2cff6b77b2433121d1bc5d11": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp",
        "fdb5b91779bdb86b1e2957223dc2b9b38981cf4d": "http://ocsp.komp-ca.telematik/ocsp",
        "ff7c3be789ea8eb3cd3da9d69b5dcc3838c7ede66bab4d49205a348cb54de7b2": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp"
    }
 
    $ ./GenRoutingTable.py ../TSL.xml -v
    TSL-id: ID327020180527230000Z
    TSL erzeugt am 2018-05-27T23:00:00Z
    nächstes Update 2018-06-26T23:00:00Z
    WARNUNG: TSL ausgelaufen oder lokale Zeit ist falsch.
    TSP "medisign GmbH"
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=DEMDS CA für Zahnärzte 2:PN,O=medisign GmbH,C=DE
    MIIFLjCCBBagAwIBAgIBDjANBgkqhkiG9w0BAQsFADBfMQswCQYDVQQGEwJE
    http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eZAA.medisign.de%3A8080%2Focsp
    Zertifikat ("DE, medisign GmbH, DEMDS CA für Zahnärzte 2:PN", SN: 14, gültig bis 2019-12-09T15:59).
    SHA-1: 6ed982b14798626e266b6f74cdb3435cb79cc007
    SHA-256: 794be575dc1eb7497a59451fb9c54d73d52ab6c9b8b23a199a8e108186511db3
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=DEMDS CA für Ärzte 5:PN,O=medisign GmbH,C=DE
    MIIFMDCCBBigAwIBAgIBLTANBgkqhkiG9w0BAQsFADBEMQswCQYDVQQGEwJE
    http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eA.medisign.de%3A8080%2Focsp
    Zertifikat ("DE, medisign GmbH, DEMDS CA für Ärzte 5:PN", SN: 45, gültig bis 2020-02-14T09:41).
    SHA-1: 599962cd2b8439064ce546a2f95d2fd40b680e27
    SHA-256: 0a1f3f265f8f2d71c7965324d2f6457344be07d9c3f7e538202f0c444654a5a9
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=DEMDS CA für Psychotherapeuten 3:PN,O=medisign GmbH,C=DE
    MIIFfzCCBGegAwIBAgIBHDANBgkqhkiG9w0BAQsFADBPMQswCQYDVQQGEwJE
    http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-ePTA.medisign.de%3A8080%2Focsp
    Zertifikat ("DE, medisign GmbH, DEMDS CA für Psychotherapeuten 3:PN", SN: 28, gültig bis 2020-02-14T09:36).
    SHA-1: 28171dbbcad34bf78e15819d600b3427edb07b50
    SHA-256: 6f91661f5727edfe3425f9cb24d327945d788f975b9bda1c25e82c49906b486e
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=DEMDS CA für Zahnärzte 1:PN,O=medisign GmbH,C=DE
    MIIFLjCCBBagAwIBAgIBDTANBgkqhkiG9w0BAQsFADBfMQswCQYDVQQGEwJE
    http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eZAA.medisign.de%3A8080%2Focsp
    Zertifikat ("DE, medisign GmbH, DEMDS CA für Zahnärzte 1:PN", SN: 13, gültig bis 2019-12-09T15:59).
    SHA-1: f6f9f0076c768d6d3762bce44515804c94237810
    SHA-256: 2e2de6dd2055b7b74d186788badf17bb614dd292e5159d2c42b633b27f600860
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=MED-ZOD2-CA-2:PN,O=medisign GmbH,C=DE
    MIIFCTCCA/GgAwIBAgIBQDANBgkqhkiG9w0BAQsFADA8MQswCQYDVQQGEwJE
    http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp
    Zertifikat ("DE, medisign GmbH, MED-ZOD2-CA-2:PN", SN: 64, gültig bis 2018-06-07T08:55).
    SHA-1: 8e1fda28ca6fcca6943f858fabe51ce3e3b1735a
    SHA-256: 4d67b9877812eb8e2bcdae03899b525ed0b993aca5e6f08ee368c27e0e53ab03
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=DEMDS CA für Ärzte 6:PN,O=medisign GmbH,C=DE
    MIIFMDCCBBigAwIBAgIBLjANBgkqhkiG9w0BAQsFADBEMQswCQYDVQQGEwJE
    http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eA.medisign.de%3A8080%2Focsp
    Zertifikat ("DE, medisign GmbH, DEMDS CA für Ärzte 6:PN", SN: 46, gültig bis 2020-02-14T09:43).
    SHA-1: 2b2710c917992dda6a05dd4469a8166bb0cf07c1
    SHA-256: dde4ee0a453a7fe87edce3a82af83c978ac9a6ee03f2cc9bd8972a9f6d4276f3
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=MED-ZOD2-CA-1:PN,O=medisign GmbH,C=DE
    MIIFCTCCA/GgAwIBAgIBPzANBgkqhkiG9w0BAQsFADA8MQswCQYDVQQGEwJE
    http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp
    Zertifikat ("DE, medisign GmbH, MED-ZOD2-CA-1:PN", SN: 63, gültig bis 2018-06-07T08:45).
    SHA-1: f989ad8197ae5a1f2cff6b77b2433121d1bc5d11
    SHA-256: ff7c3be789ea8eb3cd3da9d69b5dcc3838c7ede66bab4d49205a348cb54de7b2
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=MED-ZOD2-CA-4:PN,O=medisign GmbH,C=DE
    MIIFCTCCA/GgAwIBAgIBTjANBgkqhkiG9w0BAQsFADA8MQswCQYDVQQGEwJE
    http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp
    Zertifikat ("DE, medisign GmbH, MED-ZOD2-CA-4:PN", SN: 78, gültig bis 2020-01-27T10:50).
    SHA-1: 690aa71f7c224bd4f681cf5f73b6c62646ad966c
    SHA-256: 4fef05190a566ed4f8dba5fa72176711033f5ed884b546af2a724c2e250fbe33
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=MED-ZOD2-CA-3:PN,O=medisign GmbH,C=DE
    MIIFCTCCA/GgAwIBAgIBTTANBgkqhkiG9w0BAQsFADA8MQswCQYDVQQGEwJE
    http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp
    Zertifikat ("DE, medisign GmbH, MED-ZOD2-CA-3:PN", SN: 77, gültig bis 2020-01-27T10:50).
    SHA-1: 4def0393a8e6f35ecc9c8e85d5bcdd4a326317ac
    SHA-256: aea3bdb4bacc1736c80f8dd211f08beeed2eec9b9fbe1a41679cec069a1700c0
    TSP "arvato Systems GmbH"
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=GEM.KOMP-CA3,OU=Komponenten-CA der Telematikinfrastruktur,O=gematik GmbH,C=DE
    MIIEGjCCAwKgAwIBAgIBCTANBgkqhkiG9w0BAQsFADBtMQswCQYDVQQGEwJE
    http://ocsp.komp-ca.telematik/ocsp
    Zertifikat ("DE, gematik GmbH, Komponenten-CA der Telematikinfrastruktur, GEM.KOMP-CA3", SN: 9, gültig bis 2026-03-14T12:23).
    SHA-1: fdb5b91779bdb86b1e2957223dc2b9b38981cf4d
    SHA-256: a7e7605a2a91651142ba8469fd364c8d2b2cfe0ddd04ede291319498c23b62cb
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=GEM.VPNK-CA1,OU=VPN-Zugangsdienst-CA der Telematikinfrastruktur,O=gematik GmbH,C=DE
    MIIEfzCCA2egAwIBAgIBBjANBgkqhkiG9w0BAQsFADBtMQswCQYDVQQGEwJE
    http://download.crl.ti-dienste.de/crl/vpnk-ca1.crl
    Zertifikat ("DE, gematik GmbH, VPN-Zugangsdienst-CA der Telematikinfrastruktur, GEM.VPNK-CA1", SN: 6, gültig bis 2023-03-17T14:03).
    SHA-1: 6abdc0383272c291672da1a141490813e7e7fe77
    SHA-256: ad1f31bb64352f9df766d1021aa9133b3097602e4818606fba97f633ee7154cf
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=GEM.TSL-CA1,OU=TSL-Signer-CA der Telematikinfrastruktur,O=gematik GmbH,C=DE
    MIIEGDCCAwCgAwIBAgIBAjANBgkqhkiG9w0BAQsFADBtMQswCQYDVQQGEwJE
    http://ocsp.tsl.telematik/ocsp
    Zertifikat ("DE, gematik GmbH, TSL-Signer-CA der Telematikinfrastruktur, GEM.TSL-CA1", SN: 2, gültig bis 2023-02-17T10:18).
    SHA-1: 38e00cdd21f740ae30e7709173c0ace227f4e79c
    SHA-256: 2d7d47a533ad249b23754601f22e9082567434d130c533cb7f64a52ecffee9a3
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=GEM.KOMP-CA1,OU=Komponenten-CA der Telematikinfrastruktur,O=gematik GmbH,C=DE
    MIIEeTCCA2GgAwIBAgIBAzANBgkqhkiG9w0BAQsFADBtMQswCQYDVQQGEwJE
    http://ocsp.komp-ca.telematik/ocsp
    Zertifikat ("DE, gematik GmbH, Komponenten-CA der Telematikinfrastruktur, GEM.KOMP-CA1", SN: 3, gültig bis 2023-03-17T14:02).
    SHA-1: 42b899c13a588208599c18cf3947669de133fae6
    SHA-256: 76d94708965eb780fb1378387279c1f9f03f664c5b8142b25521ddf46c5af141
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=GEM.VPNK-CA3,OU=VPN-Zugangsdienst-CA der Telematikinfrastruktur,O=gematik GmbH,C=DE
    MIIEIDCCAwigAwIBAgIBCzANBgkqhkiG9w0BAQsFADBtMQswCQYDVQQGEwJE
    http://download.crl.ti-dienste.de/crl/vpnk-ca1.crl
    Zertifikat ("DE, gematik GmbH, VPN-Zugangsdienst-CA der Telematikinfrastruktur, GEM.VPNK-CA3", SN: 11, gültig bis 2026-03-14T12:24).
    SHA-1: f7b2031c7499ad6421090e05d8be0056fc7a96a4
    SHA-256: a7ea85d6dff6ce26a887bda1e04e9cd27886a3be89e5bf897f8a6c229f2635be
    TSP "Atos Information Technology GmbH"
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=ATOS.EGK-CA201,OU=Elektronische Gesundheitskarte-CA der Telematikinfrastruktur,O=Atos Information Technology GmbH,C=DE
    MIIEZjCCA06gAwIBAgIDD0JAMA0GCSqGSIb3DQEBCwUAMIGYMQswCQYDVQQG
    http://ocsp1.atos.tsp-egk.telematik
    Zertifikat ("DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA201", SN: 1000000, gültig bis 2025-02-09T13:58).
    SHA-1: c730a2a3a8f94834beb0096ac147f23f5f972802
    SHA-256: c1db45fdcd755ea321e6c7a29b192e45752dcda9a8eb14e0b64e0ae209e7807c
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=Atos eGK-CA 8,OU=Trustcenter,O=Atos Information Technology GmbH,C=DE
    MIIEAjCCAuqgAwIBAgIDD0JAMA0GCSqGSIb3DQEBCwUAMGYxCzAJBgNVBAYT
    http://ocsp1.atos.tsp-egk.telematik
    Zertifikat ("DE, Atos Information Technology GmbH, Trustcenter, Atos eGK-CA 8", SN: 1000000, gültig bis 2023-06-09T10:42).
    SHA-1: e3b27ab778008c794fda8e5fa6b5069a0d06af62
    SHA-256: 1a2efd3464f1b95c878eeec6ecbb70dbf29d1e4a3644e5f133cc7d47f5a3b8b5
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=Atos Origin eGK-CA 7,OU=Trustcenter,O=Atos Origin GmbH,C=DE
    MIID8DCCAtigAwIBAgIDD0JAMA0GCSqGSIb3DQEBCwUAMF0xCzAJBgNVBAYT
    http://ocsp1.atos.tsp-egk.telematik
    Zertifikat ("DE, Atos Origin GmbH, Trustcenter, Atos Origin eGK-CA 7", SN: 1000000, gültig bis 2019-09-15T13:01).
    SHA-1: 5f5114bb98201927908bd571e23efaf19e10bd40
    SHA-256: 07eb35f9ed657093125a08b6b6b7251b00e6d7290d37f76ca6ab5dfc8abec61b
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=Atos Origin eGK-CA 5,OU=Trustcenter,O=Atos Origin GmbH,C=DE
    MIID8DCCAtigAwIBAgIDD0JAMA0GCSqGSIb3DQEBCwUAMF0xCzAJBgNVBAYT
    http://ocsp1.atos.tsp-egk.telematik
    Zertifikat ("DE, Atos Origin GmbH, Trustcenter, Atos Origin eGK-CA 5", SN: 1000000, gültig bis 2019-09-14T15:04).
    SHA-1: f514d213ba2f31713239e61f3d9004389ba076d8
    SHA-256: 9150167a63834ab9c5b106c71e65065e68c7ef37c16fafaf9c8e3927cad858be
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=Atos Origin eGK-CA 4,OU=Trustcenter,O=Atos Origin GmbH,C=DE
    MIID8DCCAtigAwIBAgIDD0JAMA0GCSqGSIb3DQEBCwUAMF0xCzAJBgNVBAYT
    http://ocsp1.atos.tsp-egk.telematik
    Zertifikat ("DE, Atos Origin GmbH, Trustcenter, Atos Origin eGK-CA 4", SN: 1000000, gültig bis 2019-09-14T15:03).
    SHA-1: dac6a3d34ad9fb3ddf740e2a790dcfc481fc9430
    SHA-256: e1cd791e91b46f0f51158caee5b2d231446c94a45a45b3b5c7681cf856817480
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=ATOS.EGK-CA1,SERIALNUMBER=00000-0001000000,OU=Elektronische Gesundheitskarte-CA der Telematikinfrastruktur,O=Atos Information Technology GmbH,C=DE
    MIIEmDCCA4CgAwIBAgIDD0JAMA0GCSqGSIb3DQEBCwUAMIGxMQswCQYDVQQG
    http://ocsp1.atos.tsp-egk.telematik
    Zertifikat ("DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, 00000-0001000000, ATOS.EGK-CA1", SN: 1000000, gültig bis 2023-11-03T15:36).
    SHA-1: 7720fe98dbf9853cb489ce95b776650786a38725
    SHA-256: 32c06d315a816b3317e99156fb0ebf67f695d8ec4f8fd8b80ec856de848ba5a8
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=ATOS.EGK-CA2,OU=Elektronische Gesundheitskarte-CA der Telematikinfrastruktur,O=Atos Information Technology GmbH,C=DE
    MIIEYjCCA0qgAwIBAgIDD0JAMA0GCSqGSIb3DQEBCwUAMIGWMQswCQYDVQQG
    http://ocsp1.atos.tsp-egk.telematik
    Zertifikat ("DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA2", SN: 1000000, gültig bis 2024-03-17T15:00).
    SHA-1: 1414b6b08db8e317b17b1acc81e29108fa9fcd4b
    SHA-256: d4fffdef4bc1b19506bbf514bf90c3706134020ae4f0d6ca8dabf0f55bfaeb19
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=ATOS.EGK-CA3,OU=Elektronische Gesundheitskarte-CA der Telematikinfrastruktur,O=Atos Information Technology GmbH,C=DE
    MIIEYjCCA0qgAwIBAgIDD0JAMA0GCSqGSIb3DQEBCwUAMIGWMQswCQYDVQQG
    http://ocsp1.atos.tsp-egk.telematik
    Zertifikat ("DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA3", SN: 1000000, gültig bis 2024-03-17T15:11).
    SHA-1: af30a956287ab6615c8a33d501b6d8585463314d
    SHA-256: 90a763f542945f70d0b277b410a8f39ea1fb32fe285271de0929297255171e4d
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=ATOS.EGK-CA4,OU=Elektronische Gesundheitskarte-CA der Telematikinfrastruktur,O=Atos Information Technology GmbH,C=DE
    MIIEYjCCA0qgAwIBAgIDD0JAMA0GCSqGSIb3DQEBCwUAMIGWMQswCQYDVQQG
    http://ocsp1.atos.tsp-egk.telematik
    Zertifikat ("DE, Atos Information Technology GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, ATOS.EGK-CA4", SN: 1000000, gültig bis 2024-03-17T15:11).
    SHA-1: 159bd00d6c784d6ac8eab446e64611f939aad9ff
    SHA-256: 7cd46422fc3386a96678788ea0089bcd241c1b2a2b7e944b1c6e349a5ab3ea0f
    TSP "T-Systems International GmbH"
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=TSYSI.SMCB-CA1,OU=Institution des Gesundheitswesens-CA der Telematikinfrastruktur,O=T-Systems International GmbH,C=DE
    MIIEQzCCAyugAwIBAgIBCTANBgkqhkiG9w0BAQsFADBtMQswCQYDVQQGEwJE
    http://ocsp-smcb.tsi.tsp-smc-b.telematik/ocspr
    Zertifikat ("DE, T-Systems International GmbH, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, TSYSI.SMCB-CA1", SN: 9, gültig bis 2023-12-07T09:45).
    SHA-1: 2af62411667b55f3f49cf351432836fe0e93837f
    SHA-256: 32c9d99143d30edfcdba2e86674ef59d65a9758c07d99aa6da206b65ad943f64
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=T-Systems eGK CA 3,OU=Trust Center Deutsche Telekom,O=T-Systems International GmbH,C=DE
    MIIEOjCCAyKgAwIBAgIBATANBgkqhkiG9w0BAQsFADB5MQswCQYDVQQGEwJE
    http://ocsp-egk.tsi.tsp-egk.telematik/ocspr
    Zertifikat ("DE, T-Systems International GmbH, Trust Center Deutsche Telekom, T-Systems eGK CA 3", SN: 1, gültig bis 2021-08-25T23:59).
    SHA-1: 657cbd744e7b3525bbb85f88884e4a3279bf5aa8
    SHA-256: 4a3a8c58e3588ad290d517dfe80bddff90f19007179a6daf7ee8cc891ebaa40f
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=TSYSI.HBA-CA1,OU=Heilberufsausweis-CA der Telematikinfrastruktur,O=T-Systems International GmbH,C=DE
    MIIEMjCCAxqgAwIBAgIBCjANBgkqhkiG9w0BAQsFADBtMQswCQYDVQQGEwJE
    http://ocsp-hba.tsi.tsp-hba.telematik/ocspr
    Zertifikat ("DE, T-Systems International GmbH, Heilberufsausweis-CA der Telematikinfrastruktur, TSYSI.HBA-CA1", SN: 10, gültig bis 2024-03-15T12:47).
    SHA-1: 1aed9befc190da65223f7431a0f3ebb7685e38ba
    SHA-256: 216c0537f4d017a4cf4389315f79589ce2303d2a52d1005b5adb91d54463ce67
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=T-Systems eGK CA 4,OU=Trust Center Deutsche Telekom,O=T-Systems International GmbH,C=DE
    MIIEBjCCAu6gAwIBAgIBATANBgkqhkiG9w0BAQsFADB5MQswCQYDVQQGEwJE
    http://ocsp-egk.tsi.tsp-egk.telematik/ocspr
    Zertifikat ("DE, T-Systems International GmbH, Trust Center Deutsche Telekom, T-Systems eGK CA 4", SN: 1, gültig bis 2022-02-18T23:59).
    SHA-1: f4e23b61186783c002aae87c64272a9276daedc3
    SHA-256: 9d121b291082a794b6146e7c4b2d7d162023874c31970e0dbb23a4274296ba21
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=TSYSI.EGK-CA5,OU=Elektronische Gesundheitskarte-CA der Telematikinfrastruktur,O=T-Systems International GmbH,C=DE
    MIIEPzCCAyegAwIBAgIBATANBgkqhkiG9w0BAQsFADCBkzELMAkGA1UEBhMC
    http://ocsp-egk.tsi.tsp-egk.telematik/ocspr
    Zertifikat ("DE, T-Systems International GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, TSYSI.EGK-CA5", SN: 1, gültig bis 2024-05-29T23:59).
    SHA-1: 4b1a00655abd865e048d69dd2f7ee6c2705a93ea
    SHA-256: ad1199685faaa2e4107822804f7dae9f57f0d27404a2cd48aabca7815f2f48d9
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=TSYSI.EGK-CA6,OU=Elektronische Gesundheitskarte-CA der Telematikinfrastruktur,O=T-Systems International GmbH,C=DE
    MIIEPzCCAyegAwIBAgIBATANBgkqhkiG9w0BAQsFADCBkzELMAkGA1UEBhMC
    http://ocsp-egk.tsi.tsp-egk.telematik/ocspr
    Zertifikat ("DE, T-Systems International GmbH, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, TSYSI.EGK-CA6", SN: 1, gültig bis 2024-05-29T23:59).
    SHA-1: e159185581e551c3a4e9e5e692d2949ee32e11a7
    SHA-256: 0fb5fd3c20a426033540121bf3c57cffff49cef08d25ed0e9602ced7b00152d7
    TSP "D-Trust GmbH"
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=D-Trust.HBA-CA1,OU=Heilberufsausweis-CA der Telematikinfrastruktur,O=DTRUS,C=DE
    MIIEHDCCAwSgAwIBAgIBBzANBgkqhkiG9w0BAQsFADBtMQswCQYDVQQGEwJE
    http://ocsp.bdr.tsp-hba.telematik
    Zertifikat ("DE, DTRUS, Heilberufsausweis-CA der Telematikinfrastruktur, D-Trust.HBA-CA1", SN: 7, gültig bis 2023-12-06T11:56).
    SHA-1: f2243b3b22776658e6d431c37f1bc4c1ae758aab
    SHA-256: 43614a6b9565f8143f990018c78567e1dabcc9d10350c1c143463bea174ee053
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=D-Trust.SMCB-CA1,OU=Institution des Gesundheitswesens-CA der Telematikinfrastruktur,O=DTRUS,C=DE
    MIIELjCCAxagAwIBAgIBCDANBgkqhkiG9w0BAQsFADBtMQswCQYDVQQGEwJE
    http://ocsp.bdr.tsp-smc-b.telematik
    Zertifikat ("DE, DTRUS, Institution des Gesundheitswesens-CA der Telematikinfrastruktur, D-Trust.SMCB-CA1", SN: 8, gültig bis 2023-12-06T11:57).
    SHA-1: ec120e565a88256fbb0741f2e614ba409bbee07e
    SHA-256: f090ae4954fa9edb5272e81e3a62d355b9becb18d783bece9c19f5b7f3f21e32
    TSP "kubus IT GbR"
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    C=DE,O=kubus IT,OU=eGK PKI,CN=kubus IT eGK CA 1
    MIIEGjCCAwKgAwIBAgIIQnajwS2PXb0wDQYJKoZIhvcNAQELBQAwTjEaMBgG
    http://ocsp.kubusit.tsp-egk.telematik:8080/ocsp
    Zertifikat ("kubus IT eGK CA 1, eGK PKI, kubus IT, DE", SN: 4789195303843487165, gültig bis 2021-07-25T14:30).
    SHA-1: 71beccac2f46f7af7503ce983b86e2f2bc56732e
    SHA-256: 3ca6ee2110a062181795f5d7bd3362a83d32c008d2a531a47c961fd8937576f4
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    C=DE,O=kubus IT,OU=eGK,CN=kubus IT eGK CA
    MIIEDzCCAvegAwIBAgIISZnor9MFDHAwDQYJKoZIhvcNAQELBQAwSDEYMBYG
    http://ocsp.kubusit.tsp-egk.telematik:8080/ocsp
    Zertifikat ("kubus IT eGK CA, eGK, kubus IT, DE", SN: 5303525878062713968, gültig bis 2019-10-26T08:51).
    SHA-1: 83d7323a5540402e6e563a0170ec2271952718bc
    SHA-256: d004db051c6e70249bc65b352b28c64b6715008434bf30bad8bcb0c69bcc9ecf
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=KUBUS.EGK-CA1,OU=Elektronische Gesundheitskarte-CA der Telematikinfrastruktur,O=kubus IT GbR,C=DE
    MIIEKDCCAxCgAwIBAgIBATANBgkqhkiG9w0BAQsFADCBgzELMAkGA1UEBhMC
    http://ocsp.kubusit.tsp-egk.telematik:8080/ocsp
    Zertifikat ("DE, kubus IT GbR, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, KUBUS.EGK-CA1", SN: 1, gültig bis 2024-04-09T14:20).
    SHA-1: 097dd2ecafc610289cf8a79daa403f1ac41fb547
    SHA-256: be2001f194a5841fb9b782ef9e201dcd188713c09f829b91f2098bd10c684e0c
    TSP "BITMARCK"
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    CN=BITTE.EGK-CA7,OU=Elektronische Gesundheitskarte-CA der Telematikinfrastruktur,O=BITMARCK,C=DE
    MIIEFDCCAvygAwIBAgIBATANBgkqhkiG9w0BAQsFADB/MQswCQYDVQQGEwJE
    http://ocsp-egk.bitmarck.tsp-egk.telematik:8180/ocsp-egk
    Zertifikat ("DE, BITMARCK, Elektronische Gesundheitskarte-CA der Telematikinfrastruktur, BITTE.EGK-CA7", SN: 1, gültig bis 2024-02-14T11:56).
    SHA-1: 35c9ea6220bfa8f3dfd05a401a3c4818cae1f91b
    SHA-256: a79c1941949bd11de5b8833ab20bb3d6560bf63386817cc03ddef58102588430
    http://uri.etsi.org/TrstSvc/Svctype/CA/PKC
    C=DE,O=BITMARCK,OU=eGK PKI,CN=BITMARCK CA 1
    MIIEFjCCAv6gAwIBAgIIcGIzwGLaucswDQYJKoZIhvcNAQELBQAwSjEWMBQG
    http://ocsp-egk.bitmarck.tsp-egk.telematik:8180/ocsp-egk
    Zertifikat ("BITMARCK CA 1, eGK PKI, BITMARCK, DE", SN: 8098091981350812107, gültig bis 2019-09-28T18:42).
    SHA-1: cc685c7d3ba3a3a4f2af149032d3d6e3d7fc63be
    SHA-256: 521e10ac6b279232f4600f2456578cc1be4e72fc6d61756a9fa413410deae116
    TSP "Bundesnetzagentur"
 
    Routing-Tabelle im JSON-Format:
    {
        "07eb35f9ed657093125a08b6b6b7251b00e6d7290d37f76ca6ab5dfc8abec61b": "http://ocsp1.atos.tsp-egk.telematik",
        "097dd2ecafc610289cf8a79daa403f1ac41fb547": "http://ocsp.kubusit.tsp-egk.telematik:8080/ocsp",
        "0a1f3f265f8f2d71c7965324d2f6457344be07d9c3f7e538202f0c444654a5a9": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eA.medisign.de%3A8080%2Focsp",
        "0fb5fd3c20a426033540121bf3c57cffff49cef08d25ed0e9602ced7b00152d7": "http://ocsp-egk.tsi.tsp-egk.telematik/ocspr",
        "1414b6b08db8e317b17b1acc81e29108fa9fcd4b": "http://ocsp1.atos.tsp-egk.telematik",
        "159bd00d6c784d6ac8eab446e64611f939aad9ff": "http://ocsp1.atos.tsp-egk.telematik",
        "1a2efd3464f1b95c878eeec6ecbb70dbf29d1e4a3644e5f133cc7d47f5a3b8b5": "http://ocsp1.atos.tsp-egk.telematik",
        "1aed9befc190da65223f7431a0f3ebb7685e38ba": "http://ocsp-hba.tsi.tsp-hba.telematik/ocspr",
        "216c0537f4d017a4cf4389315f79589ce2303d2a52d1005b5adb91d54463ce67": "http://ocsp-hba.tsi.tsp-hba.telematik/ocspr",
        "28171dbbcad34bf78e15819d600b3427edb07b50": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-ePTA.medisign.de%3A8080%2Focsp",
        "2af62411667b55f3f49cf351432836fe0e93837f": "http://ocsp-smcb.tsi.tsp-smc-b.telematik/ocspr",
        "2b2710c917992dda6a05dd4469a8166bb0cf07c1": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eA.medisign.de%3A8080%2Focsp",
        "2d7d47a533ad249b23754601f22e9082567434d130c533cb7f64a52ecffee9a3": "http://ocsp.tsl.telematik/ocsp",
        "2e2de6dd2055b7b74d186788badf17bb614dd292e5159d2c42b633b27f600860": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eZAA.medisign.de%3A8080%2Focsp",
        "32c06d315a816b3317e99156fb0ebf67f695d8ec4f8fd8b80ec856de848ba5a8": "http://ocsp1.atos.tsp-egk.telematik",
        "32c9d99143d30edfcdba2e86674ef59d65a9758c07d99aa6da206b65ad943f64": "http://ocsp-smcb.tsi.tsp-smc-b.telematik/ocspr",
        "35c9ea6220bfa8f3dfd05a401a3c4818cae1f91b": "http://ocsp-egk.bitmarck.tsp-egk.telematik:8180/ocsp-egk",
        "38e00cdd21f740ae30e7709173c0ace227f4e79c": "http://ocsp.tsl.telematik/ocsp",
        "3ca6ee2110a062181795f5d7bd3362a83d32c008d2a531a47c961fd8937576f4": "http://ocsp.kubusit.tsp-egk.telematik:8080/ocsp",
        "42b899c13a588208599c18cf3947669de133fae6": "http://ocsp.komp-ca.telematik/ocsp",
        "43614a6b9565f8143f990018c78567e1dabcc9d10350c1c143463bea174ee053": "http://ocsp.bdr.tsp-hba.telematik",
        "4a3a8c58e3588ad290d517dfe80bddff90f19007179a6daf7ee8cc891ebaa40f": "http://ocsp-egk.tsi.tsp-egk.telematik/ocspr",
        "4b1a00655abd865e048d69dd2f7ee6c2705a93ea": "http://ocsp-egk.tsi.tsp-egk.telematik/ocspr",
        "4d67b9877812eb8e2bcdae03899b525ed0b993aca5e6f08ee368c27e0e53ab03": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp",
        "4def0393a8e6f35ecc9c8e85d5bcdd4a326317ac": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp",
        "4fef05190a566ed4f8dba5fa72176711033f5ed884b546af2a724c2e250fbe33": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp",
        "521e10ac6b279232f4600f2456578cc1be4e72fc6d61756a9fa413410deae116": "http://ocsp-egk.bitmarck.tsp-egk.telematik:8180/ocsp-egk",
        "599962cd2b8439064ce546a2f95d2fd40b680e27": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eA.medisign.de%3A8080%2Focsp",
        "5f5114bb98201927908bd571e23efaf19e10bd40": "http://ocsp1.atos.tsp-egk.telematik",
        "657cbd744e7b3525bbb85f88884e4a3279bf5aa8": "http://ocsp-egk.tsi.tsp-egk.telematik/ocspr",
        "690aa71f7c224bd4f681cf5f73b6c62646ad966c": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp",
        "6abdc0383272c291672da1a141490813e7e7fe77": "http://download.crl.ti-dienste.de/crl/vpnk-ca1.crl",
        "6ed982b14798626e266b6f74cdb3435cb79cc007": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eZAA.medisign.de%3A8080%2Focsp",
        "6f91661f5727edfe3425f9cb24d327945d788f975b9bda1c25e82c49906b486e": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-ePTA.medisign.de%3A8080%2Focsp",
        "71beccac2f46f7af7503ce983b86e2f2bc56732e": "http://ocsp.kubusit.tsp-egk.telematik:8080/ocsp",
        "76d94708965eb780fb1378387279c1f9f03f664c5b8142b25521ddf46c5af141": "http://ocsp.komp-ca.telematik/ocsp",
        "7720fe98dbf9853cb489ce95b776650786a38725": "http://ocsp1.atos.tsp-egk.telematik",
        "794be575dc1eb7497a59451fb9c54d73d52ab6c9b8b23a199a8e108186511db3": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eZAA.medisign.de%3A8080%2Focsp",
        "7cd46422fc3386a96678788ea0089bcd241c1b2a2b7e944b1c6e349a5ab3ea0f": "http://ocsp1.atos.tsp-egk.telematik",
        "83d7323a5540402e6e563a0170ec2271952718bc": "http://ocsp.kubusit.tsp-egk.telematik:8080/ocsp",
        "8e1fda28ca6fcca6943f858fabe51ce3e3b1735a": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp",
        "90a763f542945f70d0b277b410a8f39ea1fb32fe285271de0929297255171e4d": "http://ocsp1.atos.tsp-egk.telematik",
        "9150167a63834ab9c5b106c71e65065e68c7ef37c16fafaf9c8e3927cad858be": "http://ocsp1.atos.tsp-egk.telematik",
        "9d121b291082a794b6146e7c4b2d7d162023874c31970e0dbb23a4274296ba21": "http://ocsp-egk.tsi.tsp-egk.telematik/ocspr",
        "a79c1941949bd11de5b8833ab20bb3d6560bf63386817cc03ddef58102588430": "http://ocsp-egk.bitmarck.tsp-egk.telematik:8180/ocsp-egk",
        "a7e7605a2a91651142ba8469fd364c8d2b2cfe0ddd04ede291319498c23b62cb": "http://ocsp.komp-ca.telematik/ocsp",
        "a7ea85d6dff6ce26a887bda1e04e9cd27886a3be89e5bf897f8a6c229f2635be": "http://download.crl.ti-dienste.de/crl/vpnk-ca1.crl",
        "ad1199685faaa2e4107822804f7dae9f57f0d27404a2cd48aabca7815f2f48d9": "http://ocsp-egk.tsi.tsp-egk.telematik/ocspr",
        "ad1f31bb64352f9df766d1021aa9133b3097602e4818606fba97f633ee7154cf": "http://download.crl.ti-dienste.de/crl/vpnk-ca1.crl",
        "aea3bdb4bacc1736c80f8dd211f08beeed2eec9b9fbe1a41679cec069a1700c0": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp",
        "af30a956287ab6615c8a33d501b6d8585463314d": "http://ocsp1.atos.tsp-egk.telematik",
        "be2001f194a5841fb9b782ef9e201dcd188713c09f829b91f2098bd10c684e0c": "http://ocsp.kubusit.tsp-egk.telematik:8080/ocsp",
        "c1db45fdcd755ea321e6c7a29b192e45752dcda9a8eb14e0b64e0ae209e7807c": "http://ocsp1.atos.tsp-egk.telematik",
        "c730a2a3a8f94834beb0096ac147f23f5f972802": "http://ocsp1.atos.tsp-egk.telematik",
        "cc685c7d3ba3a3a4f2af149032d3d6e3d7fc63be": "http://ocsp-egk.bitmarck.tsp-egk.telematik:8180/ocsp-egk",
        "d004db051c6e70249bc65b352b28c64b6715008434bf30bad8bcb0c69bcc9ecf": "http://ocsp.kubusit.tsp-egk.telematik:8080/ocsp",
        "d4fffdef4bc1b19506bbf514bf90c3706134020ae4f0d6ca8dabf0f55bfaeb19": "http://ocsp1.atos.tsp-egk.telematik",
        "dac6a3d34ad9fb3ddf740e2a790dcfc481fc9430": "http://ocsp1.atos.tsp-egk.telematik",
        "dde4ee0a453a7fe87edce3a82af83c978ac9a6ee03f2cc9bd8972a9f6d4276f3": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eA.medisign.de%3A8080%2Focsp",
        "e159185581e551c3a4e9e5e692d2949ee32e11a7": "http://ocsp-egk.tsi.tsp-egk.telematik/ocspr",
        "e1cd791e91b46f0f51158caee5b2d231446c94a45a45b3b5c7681cf856817480": "http://ocsp1.atos.tsp-egk.telematik",
        "e3b27ab778008c794fda8e5fa6b5069a0d06af62": "http://ocsp1.atos.tsp-egk.telematik",
        "ec120e565a88256fbb0741f2e614ba409bbee07e": "http://ocsp.bdr.tsp-smc-b.telematik",
        "f090ae4954fa9edb5272e81e3a62d355b9becb18d783bece9c19f5b7f3f21e32": "http://ocsp.bdr.tsp-smc-b.telematik",
        "f2243b3b22776658e6d431c37f1bc4c1ae758aab": "http://ocsp.bdr.tsp-hba.telematik",
        "f4e23b61186783c002aae87c64272a9276daedc3": "http://ocsp-egk.tsi.tsp-egk.telematik/ocspr",
        "f514d213ba2f31713239e61f3d9004389ba076d8": "http://ocsp1.atos.tsp-egk.telematik",
        "f6f9f0076c768d6d3762bce44515804c94237810": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-eZAA.medisign.de%3A8080%2Focsp",
        "f7b2031c7499ad6421090e05d8be0056fc7a96a4": "http://download.crl.ti-dienste.de/crl/vpnk-ca1.crl",
        "f989ad8197ae5a1f2cff6b77b2433121d1bc5d11": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp",
        "fdb5b91779bdb86b1e2957223dc2b9b38981cf4d": "http://ocsp.komp-ca.telematik/ocsp",
        "ff7c3be789ea8eb3cd3da9d69b5dcc3838c7ede66bab4d49205a348cb54de7b2": "http://ocsp.ocsp-proxy.telematik/ocsp/http%3A%2F%2Ffocsp-zod2.medisign.de%3A8080%2Focsp"
    }

## Beispiel TU

    $ ./GenRoutingTable.py ../TSL-testref.xml 
    TSL-id: ID371820180928085849Z
    TSL erzeugt am 2018-09-28T08:58:49Z
    nächstes Update 2018-10-28T08:58:49Z
    WARNUNG: TSL ausgelaufen oder lokale Zeit ist falsch.

    Routing-Tabelle im JSON-Format:
    {
        "00aeff7ccebc545bea7ec50979afb601ce5e3fe8f1dbbb523530136b40836046": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "05a482e42a037858deeae0e21fe1498f09de5502": "http://10.25.5.5/KOMP/",
        "08aac5a4e95fbad102664f248d1f8bfc4e5d1ba4fb82d6dca18c60e7c9b30290": "http://tirutu-ocsp-smcb.medisign.tsp-smc-b.telematik-test:8080/ocsp",
        "0a1f3f265f8f2d71c7965324d2f6457344be07d9c3f7e538202f0c444654a5a9": "http://ocsp-testref.ocsp-proxy.telematik-test/ocsp/http%3A%2F%2Focsp.medisign.de%3A8080%2Focsp-ocspresponder",
        "0b4c046e0e76d443044dff3b4605f71372216ff9": "http://ocsp.achelos.de:8080/ocsp/komp",
        "0c249d09e96886442a130f00700eb3bee6df81c8": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "0d3e7d172ad44910568de831b5ea0bb07b4df39eac70be361366610844fa6f3e": "http://ehca-testref.sig-test.telematik-test:8080/status/ocsp",
        "142b3cc1219dc85e684596131c1e6f05c2f89524": "http://ocsp-egk-testref.bitmarck.tsp-egk.telematik-test:8180/ocsp-egk",
        "16aaa72f8168fdea9d11358c7e1c5cf4e52540f0": "http://ocsp.bdr.tsp-smc-b.telematik-test",
        "179bc6f69451dbda0a0feda24ad28203479a4643a63fea5660347cee7c7dcf91": "http://ocsp.achelos.de:8080/ocsp/komp",
        "17b828bd22cf161430a17c3ade501423c506f1b6": "http://download-testref.crl.ti-dienste.de/crl/vpnk-ca1.crl",
        "1a65dd6a66d930279d97c3cec9deda7c690a684733900a2c872fba9abd4f1aac": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "25306d18d53cb141690a977a4375e9951601a6d2": "http://ehca-testref.sig-test.telematik-test:8080/status/ocsp",
        "25f7229b0cbe0ea615515121eb6bb873d279862b": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "275f759b9f09249928714c63e3f8d1876e172b1a722982f7ffd5b66e904f8c11": "http://ocsp-testref.ocsp-proxy.telematik-test/ocsp/http%3A%2F%2Focsp.achelos.de%3A8080%2Focsp%2Fsmcb",
        "28171dbbcad34bf78e15819d600b3427edb07b50": "http://ocsp-testref.ocsp-proxy.telematik-test/ocsp/http%3A%2F%2Ffocsp-ePTA.medisign.de%3A8080%2Focsp",
        "29918e024314bc16951ac4324af1069823625bd64189344813e6eafe35b156ee": "http://ocsp-egk-testref.bitmarck.tsp-egk.telematik-test:8180/ocsp-egk",
        "2b2710c917992dda6a05dd4469a8166bb0cf07c1": "http://ocsp-testref.ocsp-proxy.telematik-test/ocsp/http%3A%2F%2Ffocsp-eA.medisign.de%3A8080%2Focsp",
        "2bb88f9cdf12d4d2d5bfe90d10c99adc67013fc8": "http://ocsp.bdr.tsp-hba.telematik-test",
        "387be33d1af4b7c6962fe5d778a07902d63955e1b2a078818bfcd93fc8b602d8": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "393084915e58760bcd860f4515667675e26e6013b6544aab665f1c589ea591d5": "http://10.25.5.5/KOMP/",
        "3a9360fe6fdd05faf31773d6ac9da2436ea3417a": "http://ocsp.bdr.tsp-hba.telematik-test",
        "3b06966360c731cf925c85e410255c8e5a77905d": "http://ocsp-egk-testref.tsi.tsp-egk.telematik-test/ocspr",
        "3f5bd8210b2db2233195e4be14e7a42b291adcf3cfc2800546d3089ce49dafef": "http://ehca-testref.sig-test.telematik-test:8080/status/ocsp",
        "3fb682e2695979cc39d2d551278ee0a6590a31af": "http://ehca-testref.sig-test.telematik-test:8080/status/ocsp",
        "420f0353f865f7a47348aa344a2a5c4850e19abd": "http://ocsp-egk-testref.bitmarck.tsp-egk.telematik-test:8180/ocsp-egk",
        "42fd78ae3c944ffb6341826dbe65e89dd3735f94": "http://ocsp.bdr.tsp-smc-b.telematik-test",
        "4408d1ebbfe43e8c6bfd4639db4c6e5d5ff0afe2": "http://ocsp.bdr.tsp-hba.telematik-test",
        "4419958ddf3e4319bdebd9ca4695049f2aef7f39968958c5e8d7baefe11b9e47": "http://download-testref.crl.ti-dienste.de/crl/vpnk-ca1.crl",
        "44b1c6aca0284ee50f0857abbecd7d59ba871013": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "47d399d5ac8cd9644fbc03cc5fb223c04e62da02": "http://ocsp-testref.ocsp-proxy.telematik-test/ocsp/http%3A%2F%2Focsp.achelos.de%3A8080%2Focsp%2Fegk",
        "5712f7bb4704bfd140ecbeebfdba9678813aafdeb71aef761437fe2678466875": "http://ocsp-egk-testref.bitmarck.tsp-egk.telematik-test:8180/ocsp-egk",
        "594afecae0d4e3167e1724dc8b3e858ed939c6681904da1569db9b9683937e0c": "http://ehca-testref.sig-test.telematik-test:8080/status/ocsp",
        "599962cd2b8439064ce546a2f95d2fd40b680e27": "http://ocsp-testref.ocsp-proxy.telematik-test/ocsp/http%3A%2F%2Focsp.medisign.de%3A8080%2Focsp-ocspresponder",
        "5a10c1962dff37de9c1cf00b7c7b6e0e8a4fe55d": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "5a504869cfa76cc5e3d958396cb60c34b79ee7a8": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "5bb43ca39d43f566602b3907ad6fa16d817daafd": "http://download-testref.crl.ti-dienste.de/crl/vpnk-ca1.crl",
        "5bb7298691701a7a5ac4e36e380d64c3f8cec7960ec711ba22d672cd4c4022c8": "http://ocsp-testref.tsl.telematik-test/ocsp",
        "5e192b13f15cb5004c7fad2f94537c559c2ec0e6": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "5ff992c25da0f029b290fd1e9a1d0fb04a8a9032": "http://ocsp-testref.tsl.telematik-test/ocsp",
        "665dcd9f3e41733b8288f068a69033145fdb3a25ac808190add2769a69adfaba": "http://ocsp-testref.komp-ca.telematik-test/ocsp",
        "685a5a2452bc722d74734186c7b1b6126a623e8f3a7c7e919da87a521f6f6eb4": "http://ocsp.bdr.tsp-hba.telematik-test",
        "6c41b54c86477433a48edce4a9b5feb3f9eb55846ca0ffa92dcb5a48b7b01a45": "http://ocsp-testref.kubusit.tsp-egk.telematik-test:8080/ocsp",
        "6e74237772c9750d4a18f023f88d139ded1c78ad": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "6eaa587e81211e5a758fc46e6c841451b27b4c1a68217197741c034822837555": "http://ehca-testref.sig-test.telematik-test:8080/status/ocsp",
        "6ed982b14798626e266b6f74cdb3435cb79cc007": "http://ocsp-testref.ocsp-proxy.telematik-test/ocsp/http%3A%2F%2Ffocsp-eZAA.medisign.de%3A8080%2Focsp",
        "6f91661f5727edfe3425f9cb24d327945d788f975b9bda1c25e82c49906b486e": "http://ocsp-testref.ocsp-proxy.telematik-test/ocsp/http%3A%2F%2Ffocsp-ePTA.medisign.de%3A8080%2Focsp",
        "71dc9e6789ece8c33bed44a78ab1d098c5245a16e2f8c3fd8110d4b192481f26": "http://ocsp-smcb-testref.tsi.tsp-smc-b.telematik-test/ocspr",
        "7211741c9ac8db08b829532e5e5013aa0ce55054": "http://ocsp-testref.komp-ca.telematik-test/ocsp",
        "72af9be2c30e11f3f45021249b3bc9a08a25e171": "http://ocsp-testref.kubusit.tsp-egk.telematik-test:8080/ocsp",
        "76965b5815d799d65a474f662974d1708fb548dfe3b72b3958cb2e9b858b7c64": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "792188a12907738455366a2241b279e926f170d219260ba517ce872aac577ab0": "http://ocsp-egk-testref.tsi.tsp-egk.telematik-test/ocspr",
        "794be575dc1eb7497a59451fb9c54d73d52ab6c9b8b23a199a8e108186511db3": "http://ocsp-testref.ocsp-proxy.telematik-test/ocsp/http%3A%2F%2Ffocsp-eZAA.medisign.de%3A8080%2Focsp",
        "7a0176b369876ccfe3d19a5f1bea1b6710bda20a": "http://ocsp-testref.ocsp-proxy.telematik-test/ocsp/http%3A%2F%2Focsp.achelos.de%3A8080%2Focsp%2Fsmcb",
        "7ae9e16fea14591605ee03e9d3fd21abdee9d99e": "http://ehca-testref.sig-test.telematik-test:8080/status/ocsp",
        "7d6d6443c589f004a762d9006aeb64cc5eed7774": "http://ocsp-testref.komp-ca.telematik-test/ocsp",
        "7d957a2e17ff9a324ab82c968a2ab863420564ab091356c6aa961a734bafa684": "http://ocsp-egk-testref.tsi.tsp-smc-b.telematik-test/ocspr",
        "7e0f0224ad730b37f24ce5cb59b51cdcdf611f37282c20eef62c4fa3430f3dfb": "http://ocsp.bdr.tsp-smc-b.telematik-test",
        "809cfe19975f708431e97d3a248afaa89df6748b": "http://ocsp-smcb-testref.tsi.tsp-smc-b.telematik-test/ocspr",
        "8118143b204af341871b609d8ba397ddde54c0a82717cbcb01b7dc6c0f29a41b": "http://ocsp.bdr.tsp-hba.telematik-test",
        "82731948c44a78c9f63a9bf0812ce15a19049c59": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "86781dbcfc6e224d1f39963e52e0a276d1f3803dffcee77a01b886e328b26e67": "http://ocsp-egk-testref.bitmarck.tsp-egk.telematik-test:8180/ocsp-egk",
        "8bb9a80fd7301550d3ef36831e7e0a1a14402f6d6c47d740ab8c01827dadf640": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "90ed91cc0d5a32016ec7c74e987f05a7a92c678016dee7f8b860eddcc6b546e4": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "9297139827d9c4a35c2f73b3f002ddb91dd192d958e0422c1befa4d1bf58ee0e": "http://ocsp-egk-testref.tsi.tsp-egk.telematik-test/ocspr",
        "95e013bdd440754eeed85e3c83b67382de0428d3": "http://ocsp-testref.ocsp-proxy.telematik-test/ocsp/http%3A%2F%2Focsp.testumgebung.dgnservice.de%3A8080%2Focsp-ocspresponder",
        "966e8f4549af867e76d312c625be2640ba4acfb1": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "96a270a8aa8653742fbdf8708d6226fdc550a0a25ec6748244eea4a0c65ab1ad": "http://ocsp-testref.ocsp-proxy.telematik-test/ocsp/http%3A%2F%2Focsp.achelos.de%3A8080%2Focsp%2Fegk",
        "96e00182709a391bb7bfc192676f202cb4e27c5f864ea689f78689ffc2e7e91a": "http://download-testref.crl.ti-dienste.de/crl/vpnk-ca1.crl",
        "988cbe40f7ce1135b397a5c6e2b74ac2d171ddffbee71a9604bb732d36a7c648": "http://ehca-testref.sig-test.telematik-test:8080/status/ocsp",
        "9b13bd5b074e73b8141a7ea32f6cd27ffdae06f9": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "9b344ab4a9d289c52db9695d14750ee28eceee19fa12b9b2c3a2551af3b2fae9": "http://ocsp-testref.ocsp-proxy.telematik-test/ocsp/http%3A%2F%2Focsp.achelos.de%3A8080%2Focsp%2Fhba",
        "9dafccc6b63df83a4ed65f99273264f3a3f3aa579537af1eeeeb3507a77219ad": "http://ehca-testref.sig-test.telematik-test:8080/status/ocsp",
        "a267edc3f1ecb0b400da7c804720b60f908e74cc31b2bdc33cae06d97391bde5": "http://ocsp-testref.kubusit.tsp-egk.telematik-test:8080/ocsp",
        "a26aebc339a3661576f7b8eb4e6b0e94af136f1a": "http://ocsp-egk-testref.tsi.tsp-egk.telematik-test/ocspr",
        "a31274068c98a11d5b8807297da97513fc86c536": "http://ehca-testref.sig-test.telematik-test:8080/status/ocsp",
        "a3e2fd33788de22e81b3969f12b0fb30e80e492b": "http://ehca-testref.sig-test.telematik-test:8080/status/ocsp",
        "a50ecc97f96b98a0a30cfaa0f71059bc937a6ffd604eec86d96450e243fe5673": "http://ocsp.bdr.tsp-hba.telematik-test",
        "a5bc535208bac5baa0ebdc185e2dc37588748de0": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "abd73891b73b66fe09897086feec042235d68560df7738a90789929e35b90f73": "http://ehca-testref.sig-test.telematik-test:8080/status/ocsp",
        "b0a29dda3b0b1c6cf3f19eb075368071eba5b0a1": "http://ehca-testref.sig-test.telematik-test:8080/status/ocsp",
        "b0f3861155c28654c267181c0f83c7a9eafb8f5f694de7793dd972d7a88aaa8a": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "b5311f1f4a632c5277d5eee5e61d68b7455791f254518f7458cf00184ea3f9d7": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "b7c2b6c68f30ba8079d16b8d6d13faee43e0c293b9f4f175cbee9bf19dc74577": "http://ocsp-testref.komp-ca.telematik-test/ocsp",
        "bbef26289082760a3d10d419e4b049c6104879cf6e6b3469b813bf485ee96e17": "http://ocsp-egk-testref.tsi.tsp-egk.telematik-test/ocspr",
        "bfaaaa2d189c1792244c97c430f53f69cbe8b886": "http://ocsp-egk-testref.tsi.tsp-egk.telematik-test/ocspr",
        "c0bf8878b7f541e6fd27f1eef924ae2692fe1399e5c1811a9d7cb9e31f6ae8bb": "http://ocsp.bdr.tsp-hba.telematik-test",
        "ca3516e263d825ff88a98a00c698cf011b658de7": "http://ocsp-testref.kubusit.tsp-egk.telematik-test:8080/ocsp",
        "ce0ff124b10646dfdd6a930574a4912e682f14f5dec9e6cf9edec988a4f77cbd": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "cfae354e44a14b019b4d65609149cbab3593fbe4": "http://ehca-testref.sig-test.telematik-test:8080/status/ocsp",
        "cffb2f5b11bfeb4071fd7c7a5e732ad4c42ae103": "http://tirutu-ocsp-smcb.medisign.tsp-smc-b.telematik-test:8080/ocsp",
        "d69f58fc76e361fe74872a5bf9acb28da2a6b7eb": "http://ocsp-testref.kubusit.tsp-egk.telematik-test:8080/ocsp",
        "db934c476bc18db7fafa1be826b2b22355d33db20c083e958aa8adee77374f0c": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "dc8c90fa837ae40c8122818ff260fc6162bb137019fa83a52105d52724622126": "http://ocsp-testref.ocsp-proxy.telematik-test/ocsp/http%3A%2F%2Focsp.testumgebung.dgnservice.de%3A8080%2Focsp-ocspresponder",
        "dd7064ecbc26bb3e2a994cc6b000dd0219acad84": "http://ocsp-egk-testref.bitmarck.tsp-egk.telematik-test:8180/ocsp-egk",
        "dde4ee0a453a7fe87edce3a82af83c978ac9a6ee03f2cc9bd8972a9f6d4276f3": "http://ocsp-testref.ocsp-proxy.telematik-test/ocsp/http%3A%2F%2Ffocsp-eA.medisign.de%3A8080%2Focsp",
        "e04750432368a553de71962381bffa48fc837f55e2fb7512066817f713b17a05": "http://ocsp.bdr.tsp-smc-b.telematik-test",
        "e2469ce0c3a16acc4cfb153b5da880fdaa11842b": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "e588d6137c747d6ae44c6d98fcd5d95096f68621": "http://ocsp-egk-testref.tsi.tsp-smc-b.telematik-test/ocspr",
        "e661b75086014f733e2aa7b5ffab3bcbd8dd240b": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "e6e6fae69fbe30a6bd7afc8fd09953c337779298": "http://ocsp.bdr.tsp-hba.telematik-test",
        "e7e5cb6473a1106098eb5559c3bc200320dcc0ec": "http://ocsp-hba-testref.tsi.tsp-hba.telematik-test/ocspr",
        "f04188b1639baea5c3b305435ba59655167e4577ee7763c342b994daa0690844": "http://ocsp-testref.kubusit.tsp-egk.telematik-test:8080/ocsp",
        "f0a3f40fa6f521cc092bcc00d2814dcd8fbe8cbe47d0663a6f582dd6106aad15": "http://ehca-testref.sig-test.telematik-test:8080/status/ocsp",
        "f20f54bfee5617c2b9a2dad021c68dd52233404df09ebb7c109ee1f4681c2f18": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "f310f187abdfb82575d36a0ffd35a1cba08905d0": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "f784f1a480e5404436262fe52976d44de84f87447421345393004e435ec64e4f": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "f786e78a231de01be2aa3292146218e60dd7376b63222a9d769b930b36b7f482": "http://ocsp-hba-testref.tsi.tsp-hba.telematik-test/ocspr",
        "f81280c2438bea2318ef6fe12f08b8e7094135b0": "http://ocsp-testref.ocsp-proxy.telematik-test/ocsp/http%3A%2F%2Focsp.achelos.de%3A8080%2Focsp%2Fhba",
        "fc20a0f4085ccb5baa25c22419a5a754f048b4c262a0ba2ead04d62d643c93e0": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "fc89872b47cee36a76994eb082b08b212ba69a818a51382dec873526de277785": "http://ocsp1-testref.atos.tsp-egk.telematik-test",
        "fef142029a454e75eb69cf6713ee560665ce1e16": "http://ehca-testref.sig-test.telematik-test:8080/status/ocsp"
    }

