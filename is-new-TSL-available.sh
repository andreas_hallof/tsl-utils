#! /usr/bin/env bash

# Hilfetext ausgeben, falls ich mit --help o. ä. aufgerufen werde
if [ "$1" != "" ];
then
	echo \
"Auf dem Download-Punkt des TSL-Dienstes liegt der SHA-256-Hashwert der aktuellen TSL,
unter https://download.tsl.ti-dienste.de/TSL.sha2 .
Diesen Hashwert lade ich herunter und vergleiche ihn mit dem Hashwert der lokal 
vorhandene Datei TSL.xml"
	exit 0
fi

if [ ! -f TSL.xml ]; then
	echo >&2 "keine lokale Datei TSL.xml gefunden. ABBRUCH."
	exit 1
fi

hash wget 2>/dev/null || { echo >&2 "Ich benötige wget und kann es aber nicht finden."; exit 1; }
hash cut 2>/dev/null || { echo >&2 "Ich benötige cut und kann es aber nicht finden."; exit 1; }

if [ -f TSL.sha2 ]; then rm TSL.sha2; fi

wget -nv https://download.tsl.ti-dienste.de/TSL.sha2
if [ $? != 0 ]; then
	echo >&2 "Fehler beim Download von TSL.sha2"
	exit 1
fi

if hash sha256sum 2>/dev/null; then
	lokale_tsl=$(sha256sum TSL.xml | cut -f1 -d\ )
else
	if hash openssl 2>/dev/null; then
		lokale_tsl=$(openssl dgst -sha256 TSL.xml | cut -f2 -d\ )
	else
		echo >&2 "Ich habe weder sha256sum noch openssl gefunden, muss aber einen Hashwert berechnen. ABBRUCH."
		exit 1
	fi
fi

remote_tsl=$(cut -f1 -d\  TSL.sha2)

if [ $lokale_tsl != $remote_tsl ]
then
	echo "Eine neuere TSL.xml ist auf dem Download-Punkt des TSL-Dienstes verfügbar."
        exit 1
else
	echo "Die lokale TSL.xml ist aktuell."
fi


