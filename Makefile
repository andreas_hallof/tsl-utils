.PHONY: clean test test1 test2 test3

clean: 
	rm -f *.der

test:
	./Extract-TSP.py TSL-tidy.xml oid_egk_aut
	ls *.der        

test1:
	./Extract-TSP.py TSL-tidy.xml oid_fd_tls_s oid_zd_tls_s
	ls *.der        

test2:
	./Extract-TSP.py TSL-tidy.xml oid_cv_rootcert
	ls *.der        

test3:
	./Extract-TSP.py TSL-tidy.xml oid_cv_cert
	ls *.der        

