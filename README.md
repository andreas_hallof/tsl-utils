tsl-utils
=========

Some utilities for working with the "Trust-service Status List" (TSL) (ETSI TS 102 231) 
of the "Telematikinfrastruktur"

cf. https://download.tsl.ti-dienste.de or https://download-testref.tsl.ti-dienste.de/TSL-testref.xml


Dependencies
------------
python3, the python cryptography modul, and openssl as its backend

If you don not have the 'cryptography'-modul installed on your system, use

	pip3 install cryptography


Example 1:
----------
Use wget to download TSL.xml from https://download.tsl.ti-dienste.de/TSL.xml

        $ ./Download-TSL.sh 
        CA-Zertifikat »/etc/ssl/certs/ca-certificates.crt« wurde geladen
        2018-06-15 15:00:03 URL:https://download.tsl.ti-dienste.de/TSL.xml [184963/184963] -> "TSL.xml" [1]
        $ echo $?
        0
        $ ls -l TSL.xml
        -rw-r--r-- 1 a users 184963 14. Jun 14:11 TSL.xml

If you already have a local TSL.xml, you can use is-new-TSL-available.sh to check if it is
up to date. The script compares the SHA-256 value of the local TSL with the SHA-256 value of the
remote TSL.

If the local TSL ist up to date

        $ ./is-new-TSL-available.sh 
        CA-Zertifikat »/etc/ssl/certs/ca-certificates.crt« wurde geladen
        2018-06-15 14:41:28 URL:https://download.tsl.ti-dienste.de/TSL.sha2 [74/74] -> "TSL.sha2" [1]
        Die lokale TSL.xml ist aktuell.
        $ echo $?
        0

if not

        $ ./is-new-TSL-available.sh
        CA-Zertifikat »/etc/ssl/certs/ca-certificates.crt« wurde geladen
        2018-06-15 14:39:01 URL:https://download.tsl.ti-dienste.de/TSL.sha2 [74/74] -> "TSL.sha2" [1]
        Eine neuere TSL.xml ist auf dem Download-Punkt des TSL-Dienstes verfügbar.
        $ echo $?
        1

Verify the signature of a TSL. The trust-anchor (from GEM.TSL-CA1.der) is
hard-coded in the script.  (This has various implications ... .)

        $ ./Chk-Signature-TSL.py TSL.xml
        Signierendes Zertifikat ("DE, arvato Systems GmbH, TSL Signing Unit 1", SN: 1, gültig bis 2020-03-21T11:16) ist Teil der TI-PKI.
        kryptographische Verifikation der TSL-Daten: OK
        $ echo $?
        0

Lets simulate a malicious modification of the tsl: we modify the first x509
certificate of the first TrustServiceProvider in the tsl.

        $ # black magic comes here
        $ perl -pe 'BEGIN { $line=0; } s#(<X509Certificate>)(.*)(</X509Certificate>)#$1 Modification-Test-Test $3# if $line==1; $line++;' TSL.xml >TSL-modified.xml
        $ ./Chk-Signature-TSL.py TSL-modified.xml 
        Signierendes Zertifikat ("DE, arvato Systems GmbH, TSL Signing Unit 1", SN: 1, gültig bis 2020-03-21T11:16) ist Teil der TI-PKI.
        FAIL: der Hashwert der TSL-Daten stimmt nicht mit dem im SignedInfo-Block bestätigten Hashwert überein.
        $ echo $?
        1

Extract all TSP-X.509-certificate that create certificats with oid
'oid_fd_tls_s' or 'oid_zd_tls_s' and write them to 1.der, ..., n.der

        $ ./Extract-TSP.py TSL.xml oid_fd_tls_s oid_zd_tls_s
        TSL id ID325520171128000000Z
        TSL erzeugt am 2017-11-28T00:00:00Z
        nächstes Update 2017-12-28T00:00:00Z
        In der TSL sind 8 TSPs mit zusammen 63 TSPServices enthalten. Es wurde 1 Suchziel gefunden und extrahiert.

        $ ls *.der
        1.der


Example 2:
----------
Extract all CV-root-certificates (generation 2) and write them to 1.der, ..., n.der

        $ ./Extract-TSP.py TSL.xml oid_cv_rootcert
        TSL id ID325520171128000000Z
        TSL erzeugt am 2017-11-28T00:00:00Z
        nächstes Update 2017-12-28T00:00:00Z
        In der TSL sind 8 TSPs mit zusammen 63 TSPServices enthalten. Es wurden 2 Suchziele gefunden und extrahiert.

        $ ls *.der
        1.der 2.der

Optionally you can also use "-v" or "--verbose" for more information.
Extract all CV-Cross-Certificates:

        $ ./Extract-TSP.py -v TSL.xml oid_cv_cert
        TSL id ID325520171128000000Z
        TSL erzeugt am 2017-11-28T00:00:00Z
        nächstes Update 2017-12-28T00:00:00Z
        ---------- medisign GmbH
        CN=MED-ZOD2-qCA-4 1:PN,O=medisign GmbH,C=DE
        CN=DEMDS CA für Zahnärzte 2:PN,O=medisign GmbH,C=DE
        CN=DEMDS qCA für Psychotherapeuten 4 1:PN,O=medisign GmbH,C=DE
        CN=DEMDS qCA für Ärzte 6 1:PN,O=medisign GmbH,C=DE
        CN=DEMDS qCA für Psychotherapeuten 3 1:PN,O=medisign GmbH,C=DE
        CN=DEMDS CA für Ärzte 5:PN,O=medisign GmbH,C=DE
        CN=DEMDS qCA für Zahnärzte 2 1:PN,O=medisign GmbH,C=DE
        CN=DEMDS qCA für Ärzte 5 1:PN,O=medisign GmbH,C=DE
        CN=DEMDS CA für Psychotherapeuten 3:PN,O=medisign GmbH,C=DE
        CN=DEMDS CA für Zahnärzte 1:PN,O=medisign GmbH,C=DE
        CN=MED-ZOD2-CA-2:PN,O=medisign GmbH,C=DE
        CN=DEMDS qCA für Zahnärzte 1 1:PN,O=medisign GmbH,C=DE
        CN=DEMDS CA für Ärzte 6:PN,O=medisign GmbH,C=DE
        CN=MED-ZOD2-qCA-3 1:PN,O=medisign GmbH,C=DE
        CN=MED-ZOD2-CA-1:PN,O=medisign GmbH,C=DE
        CN=MED-ZOD2-CA-4:PN,O=medisign GmbH,C=DE
        CN=MED-ZOD2-CA-3:PN,O=medisign GmbH,C=DE
        ---------- arvato Systems GmbH
        CN=DNSSEC-Trustanchor, DC=telematik
        CN=GEM.VPNK-CA1,OU=VPN-Zugangsdienst-CA der Telematikinfrastruktur,O=gematik GmbH,C=DE
        CN=GEM.TSL-CA1,OU=TSL-Signer-CA der Telematikinfrastruktur,O=gematik GmbH,C=DE
        CN=GEM.KOMP-CA1,OU=Komponenten-CA der Telematikinfrastruktur,O=gematik GmbH,C=DE
        CN=VPNK CRL-Signer2,O=arvato Systems GmbH,C=DE
        CN=Komp-PKI OCSP-Signer2,O=arvato Systems GmbH,C=DE
        CN=TSL-CA OCSP-Signer2,O=arvato Systems GmbH,C=DE
        ---------- Atos Information Technology GmbH
        CN=ATOS.EGK-CA201,OU=Elektronische Gesundheitskarte-CA der Telematikinfrastruktur,O=Atos Information Technology GmbH,C=DE
        CHR=DEZGW810214, CAR=DEZGW810214
        CN=Atos eGK-CA 8,OU=Trustcenter,O=Atos Information Technology GmbH,C=DE
        CN=Atos Origin eGK-CA 7,OU=Trustcenter,O=Atos Origin GmbH,C=DE
        CN=Atos Origin eGK-CA 5,OU=Trustcenter,O=Atos Origin GmbH,C=DE
        CN=Atos Origin eGK-CA 4,OU=Trustcenter,O=Atos Origin GmbH,C=DE
        CN=ATOS.EGK-CA1,SERIALNUMBER=00000-0001000000,OU=Elektronische Gesundheitskarte-CA der Telematikinfrastruktur,O=Atos Information Technology GmbH,C=DE
        CN=ATOS.EGK-CA2,OU=Elektronische Gesundheitskarte-CA der Telematikinfrastruktur,O=Atos Information Technology GmbH,C=DE
        CN=ATOS.EGK-CA3,OU=Elektronische Gesundheitskarte-CA der Telematikinfrastruktur,O=Atos Information Technology GmbH,C=DE
        CN=ATOS.EGK-CA4,OU=Elektronische Gesundheitskarte-CA der Telematikinfrastruktur,O=Atos Information Technology GmbH,C=DE
        CN=ATOS.EGK-OCSP2,O=Atos Information Technology GmbH,C=DE
        CHR=DEZGW820216, CAR=DEZGW820216
        CHR=DEZGW820216, CAR=DEZGW810214
        CHR=DEZGW810214, CAR=DEZGW820216
        ---------- T-Systems International GmbH
        CN=TSYSI.SMCB-CA1,OU=Institution des Gesundheitswesens-CA der Telematikinfrastruktur,O=T-Systems International GmbH,C=DE
        CN=T-Systems eGK CA 3,OU=Trust Center Deutsche Telekom,O=T-Systems International GmbH,C=DE
        CN=TSYSI.nonQES-OCSP-Signer1,O=T-Systems International GmbH,C=DE
        CN=TSYSI.nonQES-OCSP-Signer2,O=T-Systems International GmbH,C=DE
        CN=TSYSI.HBA-CA1,OU=Heilberufsausweis-CA der Telematikinfrastruktur,O=T-Systems International GmbH,C=DE
        CN=T-Systems eGK CA 4,OU=Trust Center Deutsche Telekom,O=T-Systems International GmbH,C=DE
        CN=TSYSI.EGK-CA5,OU=Elektronische Gesundheitskarte-CA der Telematikinfrastruktur,O=T-Systems International GmbH,C=DE
        CN=TSYSI.EGK-CA6,OU=Elektronische Gesundheitskarte-CA der Telematikinfrastruktur,O=T-Systems International GmbH,C=DE
        ---------- D-Trust GmbH
        CN=D-Trust.HBA-CA1-OCSP-Signer,O=DTRUS,C=DE
        CN=D-Trust.HBA-CA1,OU=Heilberufsausweis-CA der Telematikinfrastruktur,O=DTRUS,C=DE
        CN=D-Trust.SMCB-CA1-OCSP-Signer,O=DTRUS,C=DE
        CN=D-Trust.SMCB-CA1,OU=Institution des Gesundheitswesens-CA der Telematikinfrastruktur,O=DTRUS,C=DE
        ---------- kubus IT GbR
        C=DE,O=kubus IT,OU=eGK PKI,CN=kubus IT eGK CA 1
        C=DE,O=kubus IT,OU=eGK,CN=kubus IT eGK CA
        CN=OCSP-Responder X509CA 1,O=kubus IT GbR,C=DE
        CN=KUBUS.EGK-CA1,OU=Elektronische Gesundheitskarte-CA der Telematikinfrastruktur,O=kubus IT GbR,C=DE
        ---------- BITMARCK
        CN=BITTE.EGK-CA7,OU=Elektronische Gesundheitskarte-CA der Telematikinfrastruktur,O=BITMARCK,C=DE
        C=DE,O=BITMARCK,OU=eGK PKI,CN=BITMARCK CA 1
        CN=BITMARCK OCSP SIGNER 2,O=BITMARCK,C=DE
        ---------- Bundesnetzagentur
        CN=14R-CA 1:PN,O=Bundesnetzagentur,C=DE
        CN=15R-CA 1:PN,O=Bundesnetzagentur,C=DE
        CN=13R-CA 1:PN,O=Bundesnetzagentur,C=DE
        CN=12R-CA 1:PN,O=Bundesnetzagentur,C=DE
        CN=11R-CA 1:PN,O=Bundesnetzagentur,C=DE
        CN=10R-CA 1:PN,O=Bundesnetzagentur,C=DE
        In der TSL sind 8 TSPs mit zusammen 63 TSPServices enthalten. Es wurden 2 Suchziele gefunden und extrahiert.
        
        $ ls *.der
        1.der 2.der


License
-------

All programs of tsl-utils are licensed under the GPL v3 license (cf. attached file "LICENSE-GPL_V3"). 


Author
------

Andreas Hallof


