#! /usr/bin/env python3
# -*- coding: UTF-8 -*-

import argparse, logging, sys, os, hashlib, base64, re, datetime

from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import ec, padding
from cryptography.hazmat.primitives import hashes
from cryptography.exceptions import InvalidSignature

from xml.etree import ElementTree

# Vertrauensanker aus GEM.TLS-CA2.der
ECC_Public_Key_TSL_Signer_CA_PU = ec.EllipticCurvePublicNumbers(
            x=0x33f30e5392e9f79a01fe9053d17c87d9a08d37ec6244917403319f557d66c7cf,
            y=0x081b37ceba680bdd4f074c96a0521228c093ddff32befe180338d920d8c82c65,
            curve=ec.BrainpoolP256R1()
)

# Public-Key von GEM.TSL-CA28 TEST-ONLY:
ECC_Public_Key_TSL_Signer_CA_TU = ec.EllipticCurvePublicNumbers(
            x=0x5c83d1c561ecfcc14fdb305da1fa3f01bd2fdb9326dad8d62cc9a1e076ddcf1e,
            y=0x2bd0e7c8c5e429180f8f7e65cd6ca74877b48cf50c557bd37f41a409b85fb98a,
            curve=ec.BrainpoolP256R1()
)


if __name__ == '__main__':

    ScriptAge = datetime.datetime.fromtimestamp(os.path.getmtime(sys.argv[0]))
    if ScriptAge + datetime.timedelta(days=365) < datetime.datetime.now():
        sys.exit("FAIL: bitte tsl-utils aktualisieren (erscheint älter als ein Jahr zu sein).")

    a_p = argparse.ArgumentParser(description="Signaturprüfung einer ECC+RSA-TSL-XML-Datei / Detachted-Signatur")
    a_p.add_argument('-v', '--verbose', dest='verbose', action='store_true', help='verbose mode')
    a_p.add_argument('--TU', dest='TU', action='store_true', help='Testumgebung')
    a_p.add_argument('--PU', dest='PU', action='store_true', help='Produktivumgebung (default)')
    a_p.add_argument(dest='TSL_Datei', metavar='TSL-XML-Datei')
    args = a_p.parse_args()

    logging.basicConfig(level=logging.DEBUG,
                        format='[%(asctime)s] {%(pathname)s:%(lineno)d} %(levelname)s - %(message)s',
                        datefmt="%Y-%m-%dT%H:%M:%S%z")

    if not (os.path.exists(args.TSL_Datei) and os.path.isfile(args.TSL_Datei)):
        sys.exit('Datei "{}" nicht gefunden'.format(args.TSL_Datei))

    if not args.TSL_Datei.endswith("xml"):
        sys.exit("Nameskonvention (.xml) nicht eingehalten")

    signatur_datei = args.TSL_Datei[:-3] + "sig"
    if not (os.path.exists(signatur_datei) and os.path.isfile(signatur_datei)):
        sys.exit('Datei "{}" nicht gefunden'.format(signatur_datei))

    if not sys.platform in ['cygwin', 'linux']:
        logging.warning("Skript auf Plattform '{}' noch nicht getestet." % sys.platform)

    if args.TU:
        ECC_Public_Key_TSL_Signer_CA = ECC_Public_Key_TSL_Signer_CA_TU
    else:
        ECC_Public_Key_TSL_Signer_CA = ECC_Public_Key_TSL_Signer_CA_PU
        
    # So jetzt geht es erst richtig los
    with open(signatur_datei, "rb") as sig_file:
        gesamte_signatur = sig_file.read()

    assert len(gesamte_signatur)>700
    # Sanity-Check für den Beginn der ASN.1/DER-Kodierung
    assert gesamte_signatur[:2] == b'\x30\x82'
    l_1 = int.from_bytes(gesamte_signatur[2:4], byteorder='big', signed=False)
    # Sanity-Check Gesamtlänge der Signatur
    assert l_1 + 4 == len(gesamte_signatur)
    # OID für ECDSA testen
    assert gesamte_signatur[4:14] == b'\x30\x0a\x06\x08\x2a\x86\x48\xce\x3d\x04'
    # eigentliche ECDSA extrahieren
    assert gesamte_signatur[16:17] == b'\x03'
    signatur_länge = int.from_bytes(gesamte_signatur[17:18], byteorder='big', signed=False)
    offset_cert = 16 + signatur_länge + 2
    bitfeld_ecdsa_signatur = gesamte_signatur[16:offset_cert]
    ecdsa_signatur = bitfeld_ecdsa_signatur[3:]
    # und der Rest kann nach Spec nur noch das Signer-Zertifikat sein
    cert_der = gesamte_signatur[offset_cert:]

    cert = x509.load_der_x509_certificate(cert_der, default_backend())

    try:
        checked = False
        ECC_Public_Key_TSL_Signer_CA.public_key(default_backend()).verify( 
                cert.signature,
                cert.tbs_certificate_bytes, 
                ec.ECDSA(hashes.SHA256())
        )
        checked = True

    except InvalidSignature:
        sys.exit("FAIL: das signierende Zertifikat ist nicht Teil der TI-PKI.")
    else:
        if not checked:
            sys.exit("FAIL: das signierende Zertifikat ist nicht Teil der TI-PKI.")

    TSL_Signer = x509.load_der_x509_certificate(cert_der, default_backend())
    print('Signierendes Zertifikat ("{}", SN: {}, gültig bis {}) ist Teil der TI-PKI.'.format(
              ', '.join(x for x in [ y.value for y in TSL_Signer.subject ]),
              TSL_Signer.serial_number,
              TSL_Signer.not_valid_after.isoformat(timespec='minutes')
           )
         )

    if TSL_Signer.not_valid_after < datetime.datetime.now():
        sys.exit("FAIL: das signierende Zertifikat ist zeitlich nicht mehr gültig.")

    # gesamte TSL einlesen
    with open(args.TSL_Datei, 'rb') as tsl_file_binary:
        tsl_data = tsl_file_binary.read()

    TSL_Signer.public_key().verify( 
            ecdsa_signatur,
            tsl_data,
            ec.ECDSA(hashes.SHA256())
    )

    try:
            TSL_Signer.public_key().verify( 
                    ecdsa_signatur,
                    tsl_data,
                    ec.ECDSA(hashes.SHA256())
            )
    except InvalidSignature:
        sys.exit("FAIL: Die Signatur passt nicht zu den TSL-Daten.")

    print("kryptographische Verifikation der TSL-Daten: OK")


    xml_doc_root = ElementTree.parse(args.TSL_Datei).getroot()
    if xml_doc_root.tag != '{http://uri.etsi.org/02231/v2#}TrustServiceStatusList':
        sys.exit("keine gültige TI-TSL")

    if 'Id' in xml_doc_root.attrib:
        print('TSL id {}'.format(xml_doc_root.attrib['Id']))
    else:
        sys.exit("keine gültige TI-TSL")

    ns = {'tsl': 'http://uri.etsi.org/02231/v2#'} # ElementTree.register_namespace('tsl', ns['tsl'])

    c1 = xml_doc_root.findall(r'./tsl:SchemeInformation/tsl:ListIssueDateTime', ns)
    print('TSL erzeugt am', c1[0].text)
    if datetime.datetime.now() < datetime.datetime.strptime(c1[0].text, '%Y-%m-%dT%H:%M:%SZ'):
        logging.warning("WARNUNG: lokale Zeit ist falsch.")
    c1 = xml_doc_root.findall(r'./*/tsl:NextUpdate/tsl:dateTime', ns)
    print("nächstes Update", c1[0].text)
    if datetime.datetime.strptime(c1[0].text, '%Y-%m-%dT%H:%M:%SZ') < datetime.datetime.now():
        logging.warning("WARNUNG: TSL-Gültigkeit abgelaufen oder lokale Zeit ist falsch.")


    sys.exit(0)

